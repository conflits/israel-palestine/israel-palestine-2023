

.. _links_conflit_israel_2023:

==========================================================
Liens conflit
==========================================================


breakingthesilenceisrael
==========================

- https://www.instagram.com/breakingthesilenceisrael/?hl=en

israelismfilm
================

- https://video.liberta.vip/w/ectt2muHKDRHnb1cQ11V8r
- https://www.israelismfilm.com/
- https://www.instagram.com/israelismfilm/

rachelcorriefoundation
=========================


- https://www.instagram.com/rachelcorriefoundation/?hl=en

Jcall
=========

- https://fr.jcall.eu/qui-sommes-nous

JCall rassemble des citoyens juifs européens et des amis d’Israël qui sont
à la fois profondément attachés à l’existence et à la sécurité de
l’État d’Israël et très inquiets pour son avenir. Sans sous-estimer les
menaces extérieures existantes sur Israël, JCall voit dans l’occupation
et la poursuite ininterrompue des implantations en Cisjordanie et dans
les quartiers arabes de Jérusalem Est un danger pour l’identité de cet
État. 

Cette politique contribue, en outre, à affaiblir et à isoler de
plus en plus Israël sur la scène internationale en détériorant son image
auprès des opinions publiques mondiales. 

Nous affirmons que seules la fin de l’occupation et la création d’un 
État palestinien viable à ses côtés pourront garantir à Israël sa pérennité 
en tant qu’État démocratique à majorité juive, et lui rendre la place 
qui lui est due au sein des nations.

JCall est une initiative autonome de Juifs européens qui veulent
faire entendre leur voix. Elle n’est liée à aucun mouvement ou parti
israélien. Les initiateurs et signataires de JCall ne contestent en aucune
façon la légitimité et la représentativité des organisations juives
institutionnelles; ils souhaitent seulement se démarquer de leur alignement
trop souvent systématique sur la politique israélienne. 

Ils revendiquent le droit d’exprimer leur point de vue lorsque la politique d’Israël leur
paraît dangereuse pour les intérêts mêmes de l’État. 

Dans le même temps, ils s’élèvent avec force contre les campagnes de 
délégitimation d’Israël en tant qu’État qui se développent aujourd’hui 
au sein de nombreux pays européens.
