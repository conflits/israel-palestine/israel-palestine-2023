.. index::
   pair: rabba ; Valérie Stessin
   ! Valérie Stessin

.. _valerie_stessin_2023_10_11:

============================================================================
2023-10-11 **Rencontre avec la Rabba Valérie Stessin – vidéo et liens**
============================================================================

- https://rabbinchinsky.fr/2023/10/11/rabba-valerie-stessin/


Bonjour à toutes et à tous, merci pour votre large participation hier soir.

Comment prendre soin de soi et des autres lorsqu’on est touché.es de
près ou de loin par une catastrophe?

Il n’existe pas forcément de recettes, mais la question mérite d’être posée.

Que faites-vous pour vous-mêmes et pour les autres?

Ce type de partage nous pousse à la solidarité et nous éloigne du sentiment
d’impuissance.
La parole talmudique dit « les actes d’entraide (tsedaka) sauvent de la mort ».
Elle veut dire exactement cela: l’action nous permet d’échapper au vide
et à la terreur.

Partagez svp, pour que cela renforce notre pouvoir de vie, de quelle façon
vous prenez soin de vous-mêmes et des autres.

Hier, nous avons entendu la Rabba Valérie Stessin.

Ce soir, je vous propose un zoom court de suivi pour partager ce que sont
nos difficultés personnelles autour de la catastrophe israélienne et
ses retentissements pour nous.

Liens cités hier soir:

- https://www.mdais.org/en,
- https://womenofthewall.org.il/,
- https://www.kashouvot.org/,
- https://en.eran.org.il/,
- https://www.theparentscircle.org/en/about_eng-2/
