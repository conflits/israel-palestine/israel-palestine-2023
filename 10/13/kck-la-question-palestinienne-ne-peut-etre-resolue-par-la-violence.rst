.. index::
   pair: Rojava ; la question palestinienne ne peut être résolue par la violence

.. _rojava_2023_10_13:

===================================================================================
2023-10-13 **KCK : la question palestinienne ne peut être résolue par la violence**
===================================================================================

- https://rojinfo.com/kck-la-question-palestinienne-ne-peut-etre-resolue-par-la-violence/

Nous présentons nos condoléances aux populations palestiniennes et israéliennes
===================================================================================

La KCK appelle le Hamas et le gouvernement israélien à mettre fin aux
attaques qui coûtent la vie à d’innombrables civils de part et d’autre.

Face à l’escalade du conflit israélo-palestinien après les attaques
meurtrières du Hamas contre Israël et les attaques massives d’Israël
contre la bande de Gaza, qui ont fait également d’innombrables morts
parmi les civils, la coprésidence du Conseil exécutif de l’Union des
Communautés du Kurdistan (KCK, organisation faîtière du mouvement kurde)
appelle à l’arrêt des affrontements. Le conflit montre l’importance de
la solution du confédéralisme démocratique proposée par Abdullah Öcalan,
selon la KCK.

“Les attaques du Hamas contre Israël le 6 octobre 2023 et celles qui
ont suivi de la part de l’État israélien contre la population palestinienne,
notamment dans la bande de Gaza, ont fait des milliers de morts.

Ces attaques réciproques ont conduit à un véritable massacre.

**Cette situation nous afflige et nous blesse profondément.**

**En tant que mouvement de libération kurde, nous présentons nos condoléances
aux populations palestiniennes et israéliennes**.

Nous exprimons notre profonde tristesse, notre préoccupation et notre
inquiétude face à cette situation.

Nous ne sommes pas seulement préoccupés par ce qui s’est passé jusqu’à
présent, mais aussi par le scénario qui se dessine pour l’avenir.
Il est basé sur une approche totalement erronée, qui aggrave les problèmes
et conduit à un massacre des peuples.

Il est nécessaire d’abandonner cette attitude au plus vite et de mettre
fin aux attaques.
Si l’action du Hamas est inacceptable, l’attitude de l’État israélien
l’est tout autant.

Le blocus et les attaques contre Gaza doivent cesser immédiatement.

Dépasser l’État-nation #NonViolence
=======================================

La question palestinienne peut être résolue par la démocratie et la
reconnaissance des droits du peuple palestinien, et non par des méthodes
violentes.

La violence ne fera qu’aggraver les problèmes.

Les scènes terribles de ces derniers jours sont le résultat de l’impasse
dans laquelle la question palestinienne a été poussée.
La cause en est le problème lui-même.

Si l’on est vraiment préoccupé par cette situation, il faut se concentrer
sur la résolution de la question palestinienne.
Chaque mesure prise, chaque position adoptée sans tenir compte de la
résolution de la question palestinienne et des droits du peuple palestinien,
conduit à une aggravation des problèmes. La question palestinienne, qui
dure depuis près d’un siècle, l’a prouvé à maintes reprises.

Les événements en Palestine et en Israël ont une fois de plus montré
l’importance du concept de nation démocratique développé par
Rêber Apo [Abdullah Öcalan] pour la résolution des problèmes au Proche-Orient.

**La mentalité étatiste est à l’origine des problèmes des sociétés et de
l’humanité**.
Ces problèmes n’ont cessé de croître avec la pénétration de la pensée
étatiste dans les sociétés tout au long de l’histoire de l’humanité
jusqu’à aujourd’hui.

**Cette mentalité étatiste est à la racine du conflit historique arabo-juif**.

Avec la transposition au Moyen-Orient du système d’État-nation développé
par la modernité capitaliste, les problèmes n’ont fait que s’aggraver.

Tous les problèmes actuels au Moyen-Orient, y compris la question kurde,
sont dus à la mentalité de l’État-nation.
Le conflit israélo-palestinien trouve lui aussi ses racines dans la mentalité
de l’État-nation.
Les problèmes du Moyen-Orient, en particulier le problème kurde et le
problème palestinien, ne peuvent être résolus qu’en dépassant la mentalité
de l’État-nation.

Les problèmes ne peuvent pas être résolus, comme on le prétend souvent,
par la création d’autres États, mais, au contraire, par la construction
d’une nation démocratique basée sur une autogestion libre, égalitaire
et démocratique.

Les problèmes tant du peuple juif que du peuple palestinien peuvent être
résolus de cette manière.
Les modèles d’États-nations conduisent inévitablement à des conflits, à
la guerre et à la destruction mutuelle.

La réalité arabo-juive illustre le mieux cette situation.
La seule méthode pour briser ce cercle vicieux destructeur est l’approche
de la nation démocratique.

“Nous défendons les droits du peuple palestinien”
=====================================================

La cause du peuple palestinien est légitime.

Le mouvement de libération kurde est aux côtés de la juste cause du
peuple palestinien.

La résolution de la question palestinienne, tout comme celle de la question
kurde, est cruciale pour la résolution des problèmes au Moyen-Orient et
pour une démocratisation de la région.

L’État d’Israël devrait reconnaître cette réalité et, surtout, l’existence
et la volonté démocratique du peuple palestinien.

La résolution de la question palestinienne est la condition fondamentale
pour que tous les peuples du Proche-Orient, et en particulier le peuple juif,
puissent vivre en liberté, en sécurité et en paix.

“Le peuple juif a le droit de vivre dans sa région d’origine”.
=================================================================

**D’autre part, une approche correcte et respectueuse des atrocités
historiques et des génocides vécus par le peuple juif est absolument
nécessaire**.

**Sans une solution à la question palestinienne, le peuple juif ne pourra
pas non plus trouver la paix**.

**Nous pensons que le peuple juif dispose de cette conscience, de cette
sagesse et de cette volonté**.

Le peuple juif, l’un des plus anciens peuples du Moyen-Orient, a occupé
une place très importante dans la formation de la culture et de la société
moyen-orientales.
Tout comme les peuples kurde, arabe, perse, turc, araméen et d’autres
peuples du Moyen-Orient, le peuple juif a le droit de vivre au Moyen-Orient
dans sa région d’origine.


“Erdoğan verse des larmes de crocodile”
============================================

**L’État turc et le régime AKP/MHP [coalition islamo-nationaliste au pouvoir en Turquie]
n’ont pas une approche honnête et sincère de la question palestinienne**.

**Ils la considèrent exclusivement comme un sujet d’instrumentalisation**.

Le chef d’État fasciste Tayyip Erdoğan considère la question palestinienne
et le peuple palestinien sous cet angle et tente de s’en servir comme
moyen de pression pour imposer sa politique génocidaire au peuple kurde.
C’est la seule raison pour laquelle il s’intéresse à la question palestinienne.

Si l’Etat turc et Tayyip Erdoğan étaient sincères, premièrement, ils ne
traiteraient pas le peuple kurde de cette manière et, deuxièmement, ils
résoudraient la question kurde.

**La question kurde a des traits similaires à la question palestinienne**.

Tout comme on ne peut pas être droit et démocratique en Israël sans
percevoir et reconnaître les droits du peuple palestinien, on ne peut
pas être droit et démocratique en Turquie sans percevoir la réalité kurde,
sans reconnaître les droits du peuple kurde et sans soutenir la lutte
du peuple kurde.

Ceux qui méprisent et renient les Kurdes ne peuvent être aux côtés
d’autres peuples.
**Tout en dénonçant les attaques d’Israël contre les Palestiniens,
Tayyip Erdoğan parle d’attaquer plus durement les Kurdes**.

Qu’est-ce que c’est sinon de l’hypocrisie et de la tromperie ?

Ce sont clairement des larmes de crocodile qu’il verse sur la Palestine.

**Comment peut-il parler de ce qui se passe dans la bande de Gaza et de la
souffrance du peuple palestinien alors que tout le monde voit ce qu’il
fait subir au Rojava ?**

Lors des attaques aériennes de l’État turc sur le Rojava, toutes les
infrastructures de la région ont été ciblées et touchées.
Des barrages, des centrales électriques, des puits de pétrole, des dépôts
d’approvisionnement et de nombreuses autres installations ont été touchés
par les bombes.

Des dizaines de personnes été tuées.

Voilà l’attitude de l’État turc envers le peuple kurde.

Ceux qui font cela aux Kurdes ne peuvent pas se rapprocher sincèrement
des Palestiniens.”
