.. index::
   pair: pogrom ; 2023-10-15

.. _pogrom_2023_10_15:

===================================================================
2023-10-15 **Depuis le pogrom** par Bruno Karsenti & Danny Trom
===================================================================

- https://k-larevue.com/depuis-le-pogrom/

Les coordonnées du monde juif, après ce qui s’est produit en Israël les
7 et 8 octobre 2023, ne sont plus les mêmes.

Elles bougent, se recomposent et s’agencent autrement, si bien que parmi
tous les sentiments qui assaillent les juifs aujourd’hui figure la
désorientation provoquée par ce bouleversement.

Il n’est pas aisé, tandis qu’on est saisi par l’effroi et plongé dans le
deuil, d’en dégager la logique.
Discerner la situation nouvelle n’est possible qu’à nous forcer à ouvrir
les yeux – quand bien même nous voudrions les refermer pour ne plus
regarder qu’en nous-mêmes.

Mais il le faut bien.

Il le faut pour que les actions qui vont suivre se placent sous la gouverne
de notre réflexion, du moins de celle dont on reste capable.

Et puisque la réflexion, épurée autant que possible d’affects et conduite
jusqu’au bout, revêt un caractère public et ne distingue pas entre juifs
et non-juifs, parlons donc d’une façon que tout le monde puisse entendre.

L’État d’Israël, jusqu’à ces jours où l’action criminelle des islamistes
palestiniens s’est déchaînée dans le sud du pays, a été un centre juif
d’exception : le seul, dans la constellation des points du monde où les
juifs sont disséminés depuis la fin du royaume, non pas à dépasser la
condition d’exil du peuple (la Galout), mais à lui conférer une modalité
nouvelle au regard de sa conformation traditionnelle : celle d’un lieu
pour tous les juifs du monde qui, sans être leur ancien royaume restauré,
leur assurerait **la sécurité**.

Le mot, pour les juifs, rend un son étrange.

Il n’a pas la signification qu’on lui attribue d’ordinaire, lorsqu’on
pense notamment, comme il est juste par ailleurs de le faire, à son
occurrence (sous le nom de « sûreté ») parmi les droits de l’homme
référés à l’individu générique dans la déclaration de 1789 comme dans
celle de 1948.

Voilà ce qu’on omet en général de noter : si « sécurité » veut bien dire
pour un individu la préservation de sa vie et de son intégrité physique
face aux agressions venant d’autres individus, groupes ou pouvoirs en
place (étatiques ou non), le même mot revêt forcément une signification
plus spécifique quand il renvoie à des collectifs définis.

Car il se colore alors forcément de leurs expériences historiques
particulières accumulées, réfractées en chaque destin individuel des
membres du collectif concerné.

**Qu’en est-il pour les juifs ? Pour eux, la sécurité recouvre la
neutralisation du pogrom**.
C’est de cette forme très particulière de violence collective, à laquelle
il revient au centre juif de Russie de la fin du 19e siècle d’avoir donné
tardivement son nom propre, créant une catégorie applicable à rebours
et permettant de mieux lire l’histoire juive dans tout son déroulement,
que la sécurité acquise représente la neutralisation.

Par ce désignatif du pogrom, un type d’épreuve que les juifs vécurent
de façon récurrente depuis le 1er siècle jusqu’à l’époque moderne et
contemporaine – selon des modalités et avec des intensités et des
fréquences diverses – se trouvait adéquatement saisi.

Pour tout juif, elle a une résonance qu’un freudien appellerait à la fois
onto- et phylogénique.
Histoire collective du peuple et perception de soi des individus s’y mêlent
et s’y condensent.
Pour tous et pour chacun, se sentir en sécurité signifie ne pas redouter
l’émeute antijuive, encadrée ou non, émanant de groupes organisés ou de
foules inorganisées, avec sa cohorte de meurtres et d’exactions de toutes
sortes, et en tant qu’elle voue à la torture, à la mutilation et à la
mort tous les individus du peuple indistinctement, dans les lieux où on
les traque et les trouve, sans égard au sexe ou à l’âge, qu’ils soient
femmes ou hommes, nouveau-nés ou vieillards.

En ce sens, il importe de le noter, le pogrom comporte en lui-même une
passion exterminatrice du côté de ceux qui les commettent, comme il
comporte une dimension de menace existentielle du côté du groupe visé,
répercutée dans la conscience de chacun de ses membres.

Pris en ce sens – et en prenant soin de souligner que la catégorie est
évidemment applicable à d’autres peuples dès lors qu’ils se voient placés
dans des situations analogues – il est le nom propre de la persécution
et de la souffrance juives. Par où l’on voit aussi qu’il s’agit de la
violence corrélative de la Galout : celle que redoutent les collectifs
structurellement minoritaires en lesquels se distribue géographiquement
le peuple en exil.

Quant à la « sécurité », pour les juifs, elle n’est rien d’autre que la
condition où cette violence tout à fait spécifique se voit neutralisée.

C’est ce à quoi la création de l’État d’Israël, après la Shoah – où la
violence a franchi un nouveau seuil, puisque la persécution par le
pogrom, depuis le centre allemand, s’exhaussa en politique d’extermination
résolue et rationnellement mise en œuvre à l’échelle d’un continent – a
censément mis fin, en un seul et unique lieu du monde, exception territoriale
à ce titre.

Le paradoxe vaut d’être souligné : ce pays procure de la sécurité
collective juive, quand bien même il fait baisser le niveau de sécurité
individuelle objective – puisqu’il s’agit en l’espèce d’un État situé
dans un environnement hostile, fait de puissances qui veulent sa destruction
ou au mieux se résignent bon gré mal gré à sa factualité, et que les
attentats et les bombardements sont la trame continue de l’existence
de tous, les accalmies succédant irrégulièrement aux pics, et inversement.
