.. index::
   pair: pogrom ; 2023-10-15

.. _massacre_2023_10_15:

===================================================================
2023-10-15 **La ville du massacre** par Haïm Nahman Bialik
===================================================================

- https://k-larevue.com/la-ville-du-massacre/

En 1903, au lendemain du pogrom de Kichinev, Haïm Nahman Bialik quitte
Odessa et se rend sur les lieux du massacre pour recueillir les
témoignages des survivants.

Il écrit un poème qui, exprimant avec puissance son effroi et l’angoisse
devant la situation des juifs de l’est à ce moment de l’histoire de l’Europe,
trouva aussitôt un écho considérable dans le monde juif.
