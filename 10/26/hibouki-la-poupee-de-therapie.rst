.. index::
   pair: Hibouki ; la poupée de thérapie
   pair: Shy Bengal ; psychlogue

.. _hibouki_2023_10_26:

=====================================================================
2023-10-26 🧡 **Hibouki, la poupée de thérapie** (Hibouk = câlin)
=====================================================================

- https://invidious.fdn.fr/watch?v=tTCCNhQJ3f4

De quoi parle la télévision israélienne en ce moment ?

De beaucoup de choses très difficiles bien sûr, mais également des
initiatives qui permettent de prendre soin de soi et des autres.

Ce reportage israélien présente la poupée-calin du Dr. Shai Ben Gal.

Reportage diffusé le 18/10/2023 sur la chaine kahn 11

- https://www.kan.org.il/content/kan/kan-actual/p-11894/news-item/572944/


La vidéo à laquelle je veux vous donner accès aujourd'hui est une vidéo
disponible sur la télé israélienne à `cette URL https://www.kan.org.il/content/kan/kan-actual/p-11894/news-item/572944 <https://www.kan.org.il/content/kan/kan-actual/p-11894/news-item/572944/>`_
qui est en commentaire elle parle de cet homme qui est un psychologue clinicien et
de la poupée qui l'a créé pour les enfants qui ont subi un traumatisme

je veux vous faire rencontrer un bon ami à moi qui s'appelle Hibouki ça
veut dire mon câlin en hébreu Hibou c'est un câlin prêt à
dans un hôtel et donc on leur demande comment ça va et ils disent ça va
ou peut jouer avec des amis ou on joue principalement à cache-cache
c'est ce que dit cet enfant que vous voyez ici

quoi encore pourquoi est-ce que vous
pensez que Hibouki est triste ? en posant cette question il leur permet de
projeter sur le la poupée quelles sont les raisons
de sa tristesse et donc d'exprimer eux-mêmes leur propres difficultés
de façon médiatisé médiatisé dans le sens il y a un média il y
a un intermédiaire et donc le **psychologue en question s'appelle
Shy Bengal** vous le voyez avec la poupée les enfants jouer et embrasser la
poupée de diverses façons et il dit c'est pas seulement une poupée il
y a différentes parties de son corps qui permettent justement de faire
des câlins de façons diverses à cette poupée


le fait qu'il est entre un chien et un être humain et bien c'est aussi une qualité
qui permet aux enfants de se rattacher à cette poupée cette poupée qui
est comme eux donc il projettent leur sentiments sur la poupée l'angoisse
et la terreur qu'ils ressentent maintenant vont dans Hibouki dit-il et là
il dit quoi encore de quoi est-ce que Hibouki a peur qu'est-ce que vous
pensez ?

là il demande qu'est-ce que vous pouvez faire avec eux avec Hibouki
qu'est-ce que vous pouvez faire pour l'aider

on peut leur et par exemple
là vous voyez on peut lui chanter des chansons une berceuse israelienne connue
il dit oui bien [Musique] s FA

et là le journaliste explique que l'idée
est de s'occuper de la poupée ce qui rend les enfants actifs et donc ça
ça aide contre le sentiment d'impuissance et contre la peur il explique
que d'un point de vue professionnel il est attesté que quand on se met à
agir cela facilite la réaction le fait de gérer les émotions

alors raconte  qu'est-ce que c'est Hibouki c'est un nouvel ami pour toi

oui il s'appelle Hibouki

qu'est-ce qui est spécial dans cette poupée

c'est qu'il aime juste les câlins et donc voilà vous voyez différents
exemples d'enfants et maintenant qu'est-ce que tu vas faire avec lui ?

je vais m'en occuper et je vais faire plein de choses pour lui

ici on voit le même enfant qui disait que c'était chouette à l'hôtel parler
des personnes qu'il aime et des enfants avec lesquels il aimait jouer et
de la personne qui s'occupait de lui de sa jardinière gardienne
d'enfants jardinière d'enfants responsable à la crèche disparue

donc cette poupée a été utilisée au Japon après la catastrophe ; en Ukraine
également et après le tremblement de terre en Turquie le créateur de la
poupée raconte qu'il a vu récemment à Elat une fille de 21 ans pleurer
parce que elle voyait tous les enfants avec Hibouki et ça lui rappelait
sa relation à elle et la façon dont cette poupée l'avait aidé bien
des années auparavant poupée qui est encore chez elle et dont elle est
encore proche

voilà donc à travers cette vidéo on a quelques idées
sur cette poupée et puis sur la façon dont on peut utiliser peut-être
certains types de poupée pour certaines choses pour aider les enfants
dans les situations difficiles **c'était donc ce petit reportage que je
voulais traduire aujourd'hui pour nous mettre du sourire de l'espoir et
de la capacité de prendre soin les uns les unes des autres** à bientôt


