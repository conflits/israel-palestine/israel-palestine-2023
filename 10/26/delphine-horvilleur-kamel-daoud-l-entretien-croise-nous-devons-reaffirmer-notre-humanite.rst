
.. _horvilleur_2023_10_26:

=====================================================================================================================
2023-10-26 **Delphine Horvilleur-Kamel Daoud, l’entretien croisé : Nous devons réaffirmer notre humanité**
=====================================================================================================================

- https://www.nouvelobs.com/idees/20231025.OBS79981/delphine-horvilleur-kamel-daoud-l-entretien-croise-nous-devons-reaffirmer-notre-humanite.html

Delphine Horvilleur-Kamel Daoud, l’entretien croisé : « Nous devons réaffirmer notre humanité »

Le Proche-Orient n’a jamais semblé aussi proche. Depuis ce 7 octobre 2023
ensanglanté et le déclenchement de la guerre de Souccot, le drame
israélo-palestinien est revenu déchirer les cœurs.

Nous sommes, jour pour jour, deux semaines après les massacres perpétrés
par le Hamas dans le sud d’Israël, lorsque Delphine Horvilleur et Kamel Daoud
acceptent de nouer un dialogue devenu pour beaucoup impossible.

Avant ce rendez-vous qu’ils ont tous les deux accueilli comme une évidence,
la rabbin, figure de proue du judaïsme libéral en France, et l’écrivain
franco-algérien, prix Goncourt du premier roman 2015 pour
