.. index::
   pair: Retour d'Israël; 2023-10-25
   pair: Pogrom; Kfar Aza

.. _retour_israel_2023_10_25:

=======================================================================================
2023-10-25 **Retour d'Israël**
=======================================================================================

Mes très chers amis,

Je suis rentré hier après-midi de ce court séjour en Israël aux cotés
des 24 autres membres de notre délégation des représentants du judaïsme
français.

Je pensais vous écrire en tant que co-président de JEM un rapport
circonstancié et chronologique de ce voyage.

Pour être sincère, cela ne m’est pas possible.

Je vous écris simplement en tant qu’homme qui s’adresse à sa grande famille.

De ce voyage je reviens avec des sentiments et des émotions qui cheminent
en moi et qui comme probablement pour chacun de mes compagnons de voyage
vont nous travailler de l’intérieur.
Pour avoir lu depuis ces quelques dernières heures les commentaires des
uns et des autres de ceux-ci dans notre boucle WhatsApp, je me rends
compte de 2 nécessités : d’abord être les témoins fortement déterminés
à relater ce que nous avons vu, entendu et senti ; et être solides dans
nos fonctions pour absorber le désarroi, la tristesse et la colère des
juifs de France.

Ce que nous avons senti, c’est cette odeur de la mort qui imprégnait
15 jours plus tard les maisons que nous avons visitées dans le village
de Kfar Azza.

.. figure:: images/kfar_azza.png

   Kfar Azza (Lien sur la carte satellite : https://urlr.me/CvP4m)

Kfar Azza se tient à moins de 2km de la bordure de Gaza, on voit les
immeubles gazaouis depuis ce village.
C’est peut-être cette odeur qui témoigne le plus cruellement de la terrible
souffrance qu’ont connu ces villageois.

Ce que nous avons vu, ce sont les traces d’une brutalité et d’une sauvagerie
sans nom, qui de maison en maison témoignent de ce que ces familles,
ces personnes âgées, ces couples, ces femmes, ces enfants ont subi.

Ce que nous avons vu aussi à Choura dans ce centre médico-légal militaire,
est proche de l’indicible : des containers réfrigérés que les soldats
ont ouverts devant nous, dans lesquels des corps et malheureusement des
morceaux de corps ne sont pas encore identifiés et témoignent là aussi
d’actes commis par des terroristes qui ne méritent plus le nom d’hommes.

Ma vie professionnelle m’a fait collaborer avec de nombreux militaires
français ou étrangers, je n’avais jamais vu d’officiers supérieurs ou
de soldats dans un tel état de tension, les larmes aux yeux quand ils
parlent, assumant avec une très grande humanité un travail d’identification
des corps afin de rendre à chaque être humain que furent ces personnes,
leur dignité et d’une certaine manière leur humanité.

Tous ensemble, notre groupe et les militaires, sous la direction de Rav
Weissenberg colonel de Tsahal nous avons fait un kaddish.

Ce que nous avons entendu, ce sont les paroles de celles et de ceux,
soldats et civils, qui sont rentrés les premiers dans ces villages et
kibboutzim martyrisés, notamment ces soldats qui pour des raisons
familiales ou personnelles étaient proches et se sont précipités les
armes à la main pour défendre les familles et les villages qu’ils
pouvaient encore espérer sauver.

Ils nous ont dit le déchainement de violence, la détermination des assaillants,
les heures passées, parfois des jours, à combattre et à débusquer
jusqu’au dernier terroriste qui cherchait à tuer encore.

Mais les paroles les plus difficiles que nous ayons entendues, ce fut à
Jérusalem dans une salle de réunion du ministère israélien des affaires
étrangères, en écoutant le témoignage de 3 familles d’otages.

Chaque récit fut douloureux, celui qui m’a peut-être le plus impressionné
est celui d’un homme qui parlait d’une voix douce en s’excusant presque
que cela ne concernait que son fils, soldat de 19 ans dans un des
centres attaqués par des terroristes et enlevé pour être amené à Gaza
par eux.
Il s’excusait pratiquement de n’avoir subi que cet enlèvement de son fils
et qu’aucun autre membre de sa famille vivant dans une autre région n’ait
à subir de violences physiques.
A ses côtés, se tenait sa fille d’une douzaine d’années. Pendant que
son père évoquait donc l’enlèvement de son frère, elle s’est mise à
pleurer.
Ses pleurs ont fendu le cœur de tous les pères, mères, grands-pères et
grands-mères que sont les membres de notre délégation.
A la fin de la réunion, je suis allé prendre cette jeune fille dans
mes bras, j’ai entendu à nouveau ses pleurs et j’ai ressenti dans sa
réaction comme dans le regard de son père un fugace moment d’apaisement
de ne plus se sentir seuls.

**Ce que nous avons entendu enfin ce sont des responsables de la société
civile qui ont eu des mots d’une clarté implacable pour les dirigeants
israéliens : faillite politique, faillite militaire, faillite sociale**.

Pour eux, la seule colonne vertébrale qui a tenu pendant les heures et
les jours qui ont suivi cette crise, c’est la société civile.

C’est la seule qui a su être immédiatement à la hauteur des circonstances.

Cela présage une fois les combats terminés dans quelques semaines ou
quelques mois, une remise en cause extrêmement profonde de toutes les
élites. Mais c’est là un autre sujet.

C’est dans l’adversité que se forgent les temps nouveaux. Israël sera amené
certainement à de profonds changements et nous aurons à accompagner
solidairement ceux-ci.
Cette refondation probable de la société israélienne est peut-être la
lueur d’espoir qu’un jour la paix sera possible entre israéliens et
palestiniens.

Il faut espérer et probablement militer pour que de nouveaux responsables
israéliens et palestiniens soient capables de reprendre le chemin des
accords de paix dans un modèle que leurs ainés n’ont pas su réaliser.

Pour ce qui est du groupe de responsables des organisations juives
françaises que nous formions, il m’a semblé déceler une unité comme je
ne l’avais jamais ressenti.
Les dirigeants du CRIF, ceux du Consistoire Central de Paris, du FSJU,
du B’nai B’rith, de l’UEJF… Yonathan, Élie, Joël, Ariel, Philippe,
Fabienne, …

Nous avons fait la promesse tous ensemble devant les familles d’otages
de ne jamais abandonner celles-ci, tant que tous les otages ne seront
rentrés à la maison et d’agir auprès des politiques et des médias français
d’une voix forte et claire tant que cela sera nécessaire.

Je veux remercier ici Yonathan Arfi et son équipe du CRIF d’avoir su
nous rassembler et nous continuerons je l’espère sincèrement à œuvrer
tous ensemble pour soutenir les Israéliens dans ce moment si difficile.

Nous avons aussi collectivement à canaliser et à accompagner la tristesse,
le désarroi et la colère de l’ensemble des juifs de France.

Nous aurons certainement à travailler solidement et solidairement pour
que la communauté juive française dans son ensemble ne bascule pas dans
un vote de colère, en se tournant par exemple vers un parti qui n’est
pas conforme à nos valeurs.

Voilà mes très chers amis ce que je voulais et pour être très honnête,
ce que j’avais besoin de vous partager.

Chalom à tous et toutes.


Gad Weil

Co-Président de JEM
