.. index::
   pair: LDH ; Actions et mobilisations

.. _ldh_2023_10_25:

=================================================================================================================================
2023-10-25 **Actions et mobilisations** à l’appel de la LDH
=================================================================================================================================

- https://www.ldh-france.org/meeting-pour-la-paix/

ISRAEL PALESTINE
===================

A l’appel de la LDH, mobilisation la plus large possible : pour la paix,
la protection des populations civiles et un cessez le feu immédiat

Des crimes de masse, visant essentiellement des populations civiles, ont
été commis les 7 et 8 octobre 2023 par des milices lourdement armées
du Hamas dans le sud d’Israël.

Ces actes de terrorisme révulsent en nous l’Humanité.

Injustifiables, ils portent en eux le pire et n’annoncent que le pire
pour tous les autres.

Le droit d’Israël à la sécurité est incontestable. Mais les gouvernants
israéliens ont choisi de riposter en mobilisant une énorme capacité de
destruction et de mort.

A Gaza, véritable prison à ciel ouvert, ce sont plus de deux millions
de personnes qui subissent des bombardements massifs et fuient sans issue
viable leurs maisons détruites. Le blocus, en vigueur depuis 2007,
va aujourd’hui jusqu’à la privation d’eau, de vivres, de médicaments,
de carburant et d’électricité y compris dans les hôpitaux.

Le siège total de Gaza imposé depuis plusieurs jours n’est pas une riposte
proportionnée contre le Hamas mais une punition collective contre la
population civile de ce territoire.
Elle a déjà fait plusieurs milliers de victimes dans la population civile.

Le choix de la vengeance plutôt que de la justice a déjà été fait à de
multiples reprises, sans jamais assurer à quiconque ni la sécurité, ni
la paix, ni la justice.

Parce que toutes les vies comptent, parce qu’elles condamnent les crimes
de guerre et les crimes contre l’humanité où qu’ils aient été commis,
nos organisations demandent à la communauté internationale, à l’ONU,
au Conseil de l’Europe, à l’Union européenne et à la France de tout
mettre en œuvre pour faire appliquer le droit humanitaire international.

Ce que ce droit exige, c’est :

- **la libération immédiate, sans condition, de tous les otages civils
  actuellement détenus par le Hamas** ;
- **un cessez le feu immédiat, avec la fin des bombardements et du
  déplacement forcé des populations** ;
- la protection de toutes les personnes civiles quelle que soit leur
  nationalité ;
- la mise en place en urgence de façon durable et suffisante d’un corridor
  humanitaire permettant d’acheminer les produits de première nécessité
  et les médicaments ;
- la levée complète du blocus de la bande de Gaza.

Le cycle infernal de la terreur ne pourra être brisé que par le respect
du droit et d’abord du droit international humanitaire, le soutien à
l’enquête de la Cour pénale internationale ouverte en 2021, la mise en
place et le respect d’une paix durable entre les deux peuples.

**Nous appelons à la mobilisation la plus large possible pour que les
populations d’Israël et de Palestine puissent enfin voir leurs droits
respectés et protégés notamment dans le cadre des résolutions de l’ONU
de 1967.**

C’est pourquoi les organisations soussignées appellent à se réunir
le jeudi 26 octobre 2023 de 19h à 21h (accueil à partir de 18h30)
salle Olympe de Gouges, 15 rue Merlin 75011 Paris.
