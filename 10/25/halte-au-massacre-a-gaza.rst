.. index::
   pair: Grenoble ; Manifestation

.. _appel_manif_2023_10_25:

=================================================================================================================================
2023-10-25 **Halte au massacre à Gaza** appel à manifestation à Grenoble samedi 28 octobre à 14h30 rue Félix Poulat
=================================================================================================================================

- https://www.ldh-france.org/meeting-pour-la-paix/

Halte au massacre à Gaza !
===============================

Nous exigeons l’arrêt immédiat des opérations militaires contre la Bande
de Gaza. L’offensive menée par l’État d’Israël contre la population de
la Bande de Gaza, ses habitations, ses infrastructures est d’une violence
sans précédent.

Nous condamnons les crimes de guerre commis par des commandos du Hamas
contre des civils israéliens.
Comme tout peuple opprimé, le peuple palestinien a le droit à la résistance,
mais rien ne peut justifier de tels actes.

Ces crimes ne peuvent en aucun cas justifier qu’Israël commette d’autres
crimes de guerre avec l’aval de ses alliés.

Des quartiers entiers ont été rasés, des écoles, des universités, des
hôpitaux détruits. 40% des habitations de Gaza sont détruites.
On compte au soir du 24 octobre 2023, 5791 morts (720 dans la seule
journée du 24) dont 2360 enfants et 1421 femmes, plus de 15000 blessés,
sans compter tous ceux qui sont toujours sous les décombres.

Les deux tiers de la population gazaouie ont été déplacés et se trouvent
pour la plupart sans abri.

Du fait d’un siège inhumain, l’eau, l’électricité et la nourriture
manquent cruellement.
Déjà durement touchée, et malgré les quelques camions d’aide humanitaire,
la population de la Bande de Gaza est au bord d’une catastrophe humanitaire
majeure.

Ce sont des crimes de guerre qui visent aujourd’hui les hommes, les
femmes et les enfants de la Bande de Gaza, au nombre de 2,2 millions.

Nous demandons d’urgence au président de la République de cesser son
soutien inconditionnel à la politique israélienne.
La France devrait porter la voix de la paix.

La France doit demander la levée immédiate du siège inhumain imposé à
la population de Gaza, et l’annulation de l’ordre d’évacuation de la
moitié du territoire de la Bande de Gaza.
Au-delà de la catastrophe humanitaire que nous dénonçons, rien ne peut
justifier la destruction massive des infrastructures civiles qu’impliquerait
une telle opération.

Nous exigeons l’arrêt des opérations militaires contre la Bande de Gaza,
dont les populations civiles sont et seront les principales victimes,
dans le cadre d’un cessez-le feu total et immédiat.

Depuis des dizaines d’années la communauté internationale est restée
passive sans faire appliquer le droit international devant les dénis
des droits du peuple palestinien : occupation sans fin et colonisation
de la Cisjordanie y compris Jérusalem-Est, blocus destructeur et inhumain
de la Bande de Gaza depuis 16 ans...

La paix ne sera possible que dans le cadre de la reconnaissance des
droits du peuple palestinien à commencer par son droit à l’autodétermination.

Nous appelons à une manifestation sur les bases de cet appel à Grenoble
samedi 28 octobre à 14h30 rue Félix Poulat.

Les libertés d’expression et de manifestation, libertés démocratiques
fondamentales, doivent être respectées.

Premiers signataires :

Association France Palestine Solidarité (AFPS), Action Chrétienne pour
l’Abolition de la Torture (ACAT Grenoble-Grésivaudan), ACIP-ASADO,
ATTAC 38, Collectif Isérois pour la Palestine,
Confédération Générale du Travail (UD CGT Isère),
Droit Au Logement (DAL 38), Ensemble ! Isère,
Fédération Syndicale Unitaire (FSU 38),
La France Insoumise (LFI),
Ligue Internationale des Femmes pour le Paix et la Liberté (LIFPL),
Mouvement de la Paix-Isère,
Mouvement contre le Racisme et pour l’Amitié entre les Peuples (MRAP),
Nil Isère,
Nouveau Parti Anticapitaliste Isère (NPA 38),
NPA Grenoble Isère,
Parti Communiste des Ouvriers de France (PCOF 38),
Solidarité avec les Groupes d’Artisans Palestiniens (SGAP 38),
Union Communiste Libertaire (UCL 38), Union Syndicale Solidaires Isère,
Union étudiante de Grenoble (UEG), Union Juive Française pour la Paix (UJFP),
Union Nationale des Étudiants de France (UNEF Grenoble),
Sud Lutte de classes Éducation


Tract au format PDF
======================

:download:`Télécharger le tract au format PDF <pdf/appel_israel_palestine_isere_2023_10_28.pdf>`
