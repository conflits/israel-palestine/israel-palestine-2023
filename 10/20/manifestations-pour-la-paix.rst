
.. _manifs_paix_2023_10_20:

=================================================
2023-10-20 **Manifestations pour la paix**
=================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/10/20/manifestations-pour-la-paix/

**Nous relayons les rendez-vous qui suivent et appelons nos lecteurs et
lectrices à y être nombreux·ses pour exprimer notre douleur et y porter
et exiger du gouvernement français qu'il porte les revendications suivantes** :

- **Libération immédiate, sans condition, de tous les otages israéliens
  et d’autres nationalités** actuellement détenus par le Hamas ;
- **Cessez-le-feu immédiat**, avec la fin des bombardements et du déplacement
  forcé de la population ;
- **Protection de toutes les personnes civiles quelle que soit leur nationalité** ;
- **Mise en place d’un corridor humanitaire permettant d’acheminer les
  produits de première nécessité** ;
- **Levée complète du blocus** ;
- **Mise en place d'un réel processus vers une paix juste et durable dans
  le respect des résolutions de l’ONU, du droit international et des
  légitimes aspirations de paix et de justice pour les Palestinien·nes
  et les Israélien·nes**.


Appel à la manifestation du dimanche 22 octobre 2023
=============================================================================

Guerrières de la paix
---------------------------

- https://www.instagram.com/guerrieres_paix_mvt/

.. figure:: images/guerrieres_de_la_paix.png

UEJF
-----

- https://uejf.org/

.. figure:: images/uejf.png

Appel à la manifestation du mardi 24 octobre 2023
=============================================================================

.. figure:: images/syndicats.png
