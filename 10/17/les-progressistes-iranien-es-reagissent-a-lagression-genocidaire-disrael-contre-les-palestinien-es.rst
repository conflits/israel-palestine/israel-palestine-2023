
.. _iraniennes_2023_10_17:

========================================================================================================================
2023-10-17 **Les progressistes iranien·es réagissent à l’agression génocidaire d’Israël contre les Palestinien·es**
========================================================================================================================


- https://www.europe-solidaire.org/spip.php?article68384

Article
==========

**Les progressistes iranien·es condamnent fermement les bombardements
israéliens sur la population de Gaza**.

Tout en soulignant le caractère génocidaire du siège israélien de Gaza,
elles et **ils condamnent aussi fermement l’assaut du Hamas contre les civils
israéliens le 7 octobre 2023, planifié avec l’aide et l’entraînement
intensifs du gouvernement iranien**.

La majorité du public iranien qui s’oppose à son propre gouvernement
sait que, depuis quatre décennies, la République islamique instrumentalise
le sort des Palestiniens à ses propres fins autoritaires.
Néanmoins, elles et éprouvent une profonde sympathie pour le peuple
palestinien dans sa lutte pour l’autodétermination nationale contre
l’occupation israélienne.

Les progressistes iranien·es condamnent fermement les bombardements
israéliens sur la population de Gaza. Tout en soulignant le caractère
génocidaire du siège israélien de Gaza, ils condamnent aussi fermement
l’assaut du Hamas du 7 octobre 2023 contre les civil·es israélien·es, planifié
avec l’entraînement et le soutien intensifs du gouvernement iranien.

L’Association pour les études iraniennes a écrit:

**En tant qu’universitaires, nous avons l’obligation morale de lutter
contre les discours et les actes de haine et d’œuvrer en faveur de la paix,
de la tolérance et de la justice**.

Il est parfois difficile de savoir comment comparer les différentes
expériences de souffrance ou de rendre la justice, mais une chose est
sûre : Les cycles sans fin de l’oppression, de la violence et de la haine
ne font que dérailler et retarder la possibilité d’un avenir pacifique.
(Association pour les études iraniennes, 2023)

La plupart des progressistes iranien·es ont tiré les leçons de la révolution
iranienne de 1979, lorsqu’une organisation religieuse fondamentaliste,
autoritaire et misogyne a été autorisée à représenter les aspirations
des masses.
Iels ne souhaitent pas que cela arrive aux masses palestiniennes.

F. Dashti, rédacteur pour Zamaneh, un site web en langue persane en
Hollande avec des écrivain·es en Iran, a écrit : « Bien sûr, la question
israélo-palestinienne est très compliquée.
Si seuls ces deux éléments principaux étaient impliqués, la situation ne
serait peut-être pas aussi compliquée.

Cependant, il y a d’autres personnes derrière la scène ou parfois sur la
scène qui s’établissent avec des intérêts et des calculs complètement
différents.
Les changements qu’elles ont provoqués et leurs ruses occasionnelles
pour apparaître d’un côté, puis de l’autre, rendent la situation plus
compliquée. (Dashti, 2023)

Khosrow Sadeghi-Borujeni, chercheur dans le domaine du travail et de la
protection sociale en Iran, écrit sur le rôle joué par Israël depuis
1987 dans le soutien au Hamas en tant qu’alternative aux dirigeants
nationalistes palestiniens plus laïques.
Il cite Adam Hanieh, économiste politique jordanien, sur la torture et
le meurtre de membres de gauche palestiniens par le Hamas, ainsi que sur
sa promotion de la misogynie et de l’exploitation capitaliste.

Borujeni conclut que « si ces réalités et les interactions des forces
existantes sur le terrain de la lutte ne sont pas prises en considération,
la défense légitime de la Palestine et la violence d’un peuple captif ne
feront qu’alourdir le bilan humain et serviront de prétexte à une
répression accrue des peuples de Palestine et du Liban, dans l’intérêt
des États-Unis et d’Israël.

Par conséquent, une force qui fait elle-même partie du problème et qui
a bénéficié de ce problème pendant des décennies ne peut pas avancer
sur la voie de la résolution du problème ». (Sadeghi-Borujeni, 2023)

Une déclaration récente publiée par plusieurs organisations socialistes
iraniennes en exil est intitulée « Les peuples de Palestine et d’Israël
ne bénéficieront pas de cette guerre réactionnaire ».

Cette déclaration affirme que « ces dernières années, avec les « Accords d’Abraham »,
les gouvernements arabes réactionnaires ont encouragé une réconciliation
ouverte avec Israël, ce qui a mis en danger la position du Hamas, du Jihad islamique,
du Hezbollah et de la République islamique face à Israël ».

Ainsi, la République islamique d’Iran « a besoin de cette guerre et est
l’une des parties prenantes de ces politiques de promotion de la guerre ».
(Rahe Kargar, 2023)

De nombreux militants et militantes iranien·es sont également très
préoccupé·es par l’instrumentalisation de la cause palestinienne par
la République islamique et par l’utilisation d’une rhétorique
pro-palestinienne pour dissimuler l’intensification de la répression à
l’intérieur du pays.
Alors que le gouvernement iranien parle des souffrances du peuple palestinien
sous le colonialisme israélien, il continue de sévir contre les minorités
nationales iraniennes telles que les Kurdes, dont beaucoup ont été
exécuté·es simplement pour avoir cru au droit des Kurdes à l’autodétermination.

Les militant·es kurdes qui ont fui vers le gouvernement régional du Kurdistan,
dans le nord de l’Irak, pour se réfugier dans les camps du parti
d’opposition kurde, sont maintenant expulsé·es sous la pression du
gouvernement iranien.

Plus récemment, un éminent cinéaste iranien, Dariush Mehrjui, et
son épouse, Vahideh Mohammadifar, scénariste, ont été poignardés
à mort à leur domicile, comme l’ont été plusieurs autres intellectuels
dissidents au cours des dernières décennies.

Avant son assassinat, Mehrjui avait défié le ministère de la culture
dans un message vidéo contre la censure. (Najafi, 2023)

S. Shams, journaliste à Zamaneh, écrit : « Il semble qu’il n’y ait pas
de dialogue profond pour construire la solidarité entre les combattant·es
iranien·es et palestinien·es.

À l’exception d’une lettre de certain·es artistes palestinien·es au
début du soulèvement de Zhina [le mouvement Femme, Vie, Liberté] pour
défendre les combattant·es iranien·es de la liberté, nous n’avons pas
vu d’autres positions claires exprimées sur les luttes actuelles en Iran,
et il semble souvent que les activistes palestiniens·e soient évasifs
lorsqu’il s’agit de parler des problèmes en Iran. » (Shams, 2023)

En réponse, un militant palestinien du boycott, du désinvestissement et
des sanctions déclare : « L’état de guerre permanent limite considérablement
la liberté d’expression. Le peuple palestinien est sous pression.
Elles et ils vivent et luttent dans une situation compliquée et ne
peuvent pas facilement critiquer qui que ce soit. » (Shams, 2023 )

Alors que les pressions subies par le peuple palestinien sont immenses
et que la dernière invasion israélienne de Gaza devient chaque jour
plus sanglante et plus destructrice, la possibilité que cette guerre
devienne une guerre régionale massive avec l’intervention de l’Iran et
de ses milices mandataires est très réelle.

Les progressistes iranien·es soutiennent les Palestinien·es dans leur
lutte contre le génocide.
Cependant, iels veulent également s’assurer que le gouvernement iranien
ne profite pas de cette guerre pour éteindre le mouvement « Femme, Vie, Liberté »
qui a émergé en Iran l’année dernière en tant que lutte pour les droits
des femmes, les droits des minorités opprimées et les droits du travail.

Iels ne veulent pas que le monde oublie que Narges Mohammadi, une militante
féministe iranienne des droits des êtres humains qui est incarcérée, a
reçu le prix Nobel de la paix pour sa lutte courageuse en faveur des
droits des femmes et contre la peine de mort.

Les progressistes iranien·es veulent exprimer leur solidarité avec la
lutte palestinienne sur la base d’une vision qui affirme la vie et qui
défie le fondamentalisme religieux, l’autoritarisme, le colonialisme,
l’impérialisme, le racisme, la misogynie, l’homophobie et l’exploitation
de classe. (Déclaration des revendications minimales, 2023)

Frieda Afary


Ressources
=============

Association for Iranian Studies. (2023) “AIS Council Statement on the
War in Gaza.” October 12.

Fassihi, Farnaz and Ronen Bergman. (2023) “Invasion Prompts a Renewed
Examination of Hamas’s Connections to Iran.” New York Times. October 14.

Dashti, F. (2023) “Vahshat-e Bitafavoti.” Zamaneh. October 10.
https://www.radiozamaneh.com/784876/

Najafi, Elahe. (2023) “Mehrjui as Aqaz to Farjam.” Zamaneh. October 14.
https://www.radiozamaneh.com/785457

Rah-e Kargar. 2023. “Mardom Felestin va Esrail Hich Manafe’I dar in
Jang-e Erteja’I Nadarand.” Rahe Kargar. October 9.
https://rahkargar.com/?p=23706

Ramezanian, Ali. (2023). “Hemayat-e Mali va Taslihati-ye Iran as
Hamas Cheqadr Ast ?” BBC Persian, October 17. https://www.bbc.com/persian/articles/cprxydnn3v7o

Sadeghi-Borujeni, Khosrow (2023). “’Madar-e Sefr Darejeh-e’ Khavaremianeh.”
Naqd-e Eqtesad-e Siasi. October 11.
https://pecritique.com/2023/10/11/مدار–صفردرجهی–خاورمیانه–خسرو–صاد/

Shams, S. (2023) “Mobarezeh-e Jahani Aleyh-e Apartaid dar Felestin :
Peyvandsazi baraye Azadi-ye Hamegani.” Zamaneh. October 8.
https://www.radiozamaneh.com/784342/

Statement of Minimum Demands of Iranian Unions and Civil Society Organizations.
February 14, 2023.

The Statement of Minimum Demands of Independent Iranian Unions and Civil
Society Organizations – Iranian Progressives in Translation

Traduit avec http://www.DeepL.com/Translator (version gratuite)
