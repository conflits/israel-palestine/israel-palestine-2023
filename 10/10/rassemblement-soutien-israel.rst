

.. _crif_grenoble_2023_10_10:

============================================
Rassemblement soutien Israel à Grenoble
============================================

Face au terrorisme, le CRIF Grenoble-Dauphine, le FSJU, les associations
consistoriales et communautaires, les amis d’Israel appellent à un

Rassemblement et hommage aux victimes
En solidarité avec Israël et les Israéliens

ce jour Mardi 10 octobre 2023 à 18h30
Parvis Espace des Cultures Juives
4bis rue des Bains GRENOBLE

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.717361867427827%2C45.18543409502039%2C5.720902383327484%2C45.18697101826111&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.18620/5.71913">Afficher une carte plus grande</a></small>

