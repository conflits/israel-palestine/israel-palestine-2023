
.. _jjr_2023_10_10:

==================================================================================
2023-10-10 **JJR pleure les victimes des massacres en Israël et en Palestine**
==================================================================================

#hamas #massacre #antisemitisme #deshumanisation #deni #Gauche #france Le’haïm (A la #vie) #jjr #paix #anarchistes

.. #kibboutz #KfarAza

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/10/10/jjr-pleure-les-mort%c2%b7es-israelien%c2%b7nes-massacre%c2%b7es-par-le-hamas-ainsi-que-les-palestinien%c2%b7nes-tue%c2%b7es-par-larmee-israelienne/
- https://blogs.mediapart.fr/juives-et-juifs-revolutionnaires/blog/101023/jjr-pleure-les-victimes-des-massacres-en-israel-et-en-palestine
- https://kolektiva.social/@jjr/111211428000939321

Ce samedi 7 octobre 2023, nous nous sommes réveillé·es avec le choc des
images de l’attaque menée par le Hamas, des témoignages terrifiants, la
peur pour nos proches là-bas pour certain·es d’entre nous, et pour tou·te·s,
la peur pour les Juif·ves ici.

A cela s’est ajoutée une avalanche de commentaires et de communiqués.

**Une partie des gauches politiques, décoloniales et antiracistes, s’est
démarquée par le soutien explicite aux tueurs antisémites, reprenant
parfois mot pour mot la communication du Hamas.**

Nous sommes furieux⋅ses contre ceux et celles qui se réjouissent ici du
sang versé par le Hamas.
Furieux·ses de constater le résultat de décennies de déshumanisation
des vies israéliennes.

Nous sommes halluciné·es de constater le peu de cas qu’ils et elles font
des vies palestiniennes puisque ces soutiens du Hamas ne semblent pas
comprendre les répercussions qui s’annoncent déjà sur la situation
au Moyen-Orient.

Le bilan actuel fait état de plus de 900 mort⋅es et 2 616 blessé⋅es en
Israël au cours de ces attaques, dont la grande majorité étaient et
sont des civil·es désarmé·es.

Nous voulons rappeler une chose : rien ne peut justifier l’exécution
délibérée de civils, qu’iels soient israélien·nes ou palestinien·nes.

**Rien.**

On ne peut pas apporter « son soutien aux moyens de luttes que les
Palestinien·nes ont choisi pour résister » (communiqué du NPA du 7 octobre 2023)
**quand ces derniers sont des meurtres de masse de civils, des enlèvements,
et des assassinats d’hommes, femmes, enfants et vieillards.**

Encore moins lorsque ces meurtres sont **mis en scène sous formes de
spectacles glorieux de décapitations, d’expositions de cadavres, de
sévices et d’humiliations infligées à des corps désincarnés**.

Ceci est le macabre résultat d’une opération indiscriminée sur des Juif·ves
et toute personne se trouvant sur leur passage, ne visant aucune cible
stratégique, militaire ou économique.

Parmi les victimes **nous déplorons des jeunes participant à une rave party,
des militant⋅es pour la paix, des anarchistes contre le mur, des travailleurs
détachés thaïlandais, et bien d’autres**.

**Les réactions de soutien au Hamas sonnent comme une justification ignoble
des crimes de guerre antisémites perpétrés sur des victimes dont le caractère
civil est dénié tant par le Hamas que par ses relais dans une partie des
gauches occidentales**.

**Le déni du caractère civil des victimes est le pivot idéologique et
argumentatif du Hamas qui considère l’ensemble des Juif·ves israélien·nes
comme des colons** et de ce fait comme cibles légitimes.

Cette assimilation pernicieuse des Juif·ves aux israéliens et aux colons
rend chaque assassinat et chaque enlèvement acceptable.
Cheikh Yassine, fondateur du Hamas disait à ce propos : « Tout juif est
une cible et peut être tué. ».
La logique exterminatrice menée par le Hamas a un seul objectif : la mise
en fuite des israélien·nes.

**Le message du Hamas est clair : « rentrez-chez vous ».
Mais où est ce « chez eux » supposé ?**

Il est pourtant clair que personne ne partira de cette partie du Moyen-Orient,
même lorsqu’il s’embrase : israélien·nes et palestinien·nes n’ont nulle
part où partir.

**Cette violence, celle qu’une partie des gauches justifie, a des conséquences
pour toustes les Juif·ves de diaspora**.
Depuis la seconde intifada de 2000, chaque épisode de tension au Moyen-Orient,
là-bas, est suivie d’une importation d’hostilités identitaires ici, et
se traduit par une vague d’actes antisémites.
Certain·es renoncent avec déchirement à porter leurs signes religieux :
kippa, étoile de David et mezzouza à l’entrée de leur habitation.

Déjà des dizaines d’actes antisémites ont été recensés depuis ce week-end,
alors que sur les réseaux sociaux, où des posts glorifiant les massacres
recueillent des centaines de milliers de likes, l’atmosphère est irrespirable.
**Tout antiraciste conséquent devrait s’en alarmer et se tenir aux côtés
des Juifs et des Juives**.

On peut et on doit condamner la politique du gouvernement israélien et
ses crimes envers les Palestinien·nes **sans faire l’apologie des crimes
de guerre du Hamas**.
On peut dénoncer l’invisibilisation des souffrances palestiniennes **sans
effacer et nier celles des victimes civiles israéliennes**.

C’est possible.

Et c’est la seule voie dans laquelle peut s’honorer la gauche.
**Nous saluons à ce titre la prise de position claire du député LFI
Rodrigo Arenas**.

Toutes celles et ceux qui glorifient le Hamas, sans s’intéresser aux
infamies antisémites dont est faite leur charte [https://fr.wikipedia.org/wiki/Charte_du_Hamas#Antisémitisme],
sans prendre en compte leurs méthodes criminelles, y compris à l’égard
des populations palestiniennes sous leur contrôle, affirment une pseudo
radicalité politique plus qu’ils n’œuvrent à la lutte pour un monde
débarrassé de toutes ses oppressions.
Rappelons enfin que les règles internationales comme la convention de
Genève ne sont pas des caprices bourgeois, mais des avancées sociales
majeures visant à protéger en temps de guerre celles et ceux qui en ont
le plus besoin, civil·es et prisonnier·es, et dont il n’est absolument
pas souhaitable de s’abstraire.

Soutenir le Hamas, le Djihad Islamique ou le Hezbollah, tous financés
par l’Iran des ayatollahs, ce n’est pas faire acte d’un soutien héroïque
aux Palestinien·nes qui souffrent, c’est envoyer un message des plus
funestes à toute personne juive, en lui expliquant qu’elle et ses proches
ou lointains semblables qui ont fait l’affront de respirer, ne méritent
pas de continuer à le faire.

En Israël vivent une majorité de personnes réfugiées ou issues de familles
réfugiées.
Sionistes ou non, c’est la nécessité élémentaire d’habiter quelque part
qui les a amenés là.
Réfugié·es après la Shoah quand la plupart des pays  leur fermaient les
frontières.

**Réfugié·es devenus apatrides après la guerre de 1948 et les expulsions
d’Égypte, d’Irak, de Syrie, du Liban, du Yémen…
Ainsi, réduire l’ensemble des Israélien·nes à des colons pour justifier
leur assassinat est une simplification de l’histoire de l’antisémitisme,
de la persécution millénaire des Juif·ves et de ses conséquences.**

Aujourd’hui, la plupart des israélien·nes sont des sabra, né·es en Israël,
c’est leur pays et iels n’en ont pas d’autres.

Oui, la création d’Israël a aussi été le fruit de pratiques coloniales
et elle a eu pour conséquence la Nakba pour les Palestinien·nes.

Oui, elle est aussi caractérisée par la colonisation et l’occupation
brutale de la Cisjordanie et le blocus de Gaza, qui plus est sous le
gouvernement d’extrême droite actuel.

**Mais cela ne peut justifier une déshumanisation des populations israéliennes**.

Regarder en face la responsabilité de l’antisémitisme, d’où qu’il vienne,
dans la situation actuelle impose de sortir des schémas simplistes.
Regarder l’ensemble de ces réalités en face, des malheurs qui s’ajoutent
et ne se compensent pas les uns les autres, est le seul chemin de la
justice.

Déshumaniser les Israélien·nes n’est pas plus acceptable que déshumaniser
les Palestinien·nes. Derrière les morts il y a des familles et des proches
qui pleurent chaque jour.
Et derrière les mots d’ordre et les slogans accrocheurs, se trouvent des
violences inouïes qui ne peuvent se comprendre que par l’écoute et l’humilité.

Si les événements sont traumatiques pour beaucoup de monde, notamment
dans la minorité juive, il reste nécessaire de conserver une attitude
solidaire également envers les souffrances du peuple palestinien, victime
de l’occupation, du colonialisme et de la guerre depuis plusieurs décennies.

**Les traumatismes ne s’annulent pas, ils ne font que s’accumuler**.

Se réjouir ou justifier le massacre en cours à Gaza n’est pas acceptable,
et ne le sera jamais.
Le bilan actuel fait état de 687 mort⋅es et 3727 blessé⋅es.

Les bombardements israéliens sont en train de tout raser, ne soyons pas
aveugles, ce sont des piles de cadavres de palestinien·nes qui se trouvent
dessous.
Ce sont des milliers de Palestinien·nes qui ont été tué·es ces dernières
années, parmi lesquels de nombreux enfants et des civils, qui ne méritaient
pas non plus de mourir mais de vivre libres et en paix, loin de la guerre,
des chars de l’armée israélienne comme des assassins du Hamas.

Et trop souvent, leur mort n’a donné lieu en Europe qu’à du silence et
de l’indifférence. Ce sont aujourd’hui deux millions de Palestinien·nes
qui vivent dans des conditions inacceptables dans une ville, Gaza, qui
n’est qu’une prison à ciel ouvert.

**Nous ne pouvons pas accepter leur déshumanisation non plus**.

La fuite en avant militariste et le maintien du statu quo colonial n’est
pas une solution mais une vision de cauchemar qui ne pourra mener qu’à
un enfer pire encore.

Beaucoup de proches de victimes pleurent en ce moment, des proches juif·ves,
des proches palestinien·nes, des proches israélien·nes, et nous ne voyons
pas ce qu’il y a à célébrer face à leurs deuils.

**Une logique mortifère traverse l’ensemble du spectre politique : celle
qui présente ces morts comme un mal nécessaire.**

Un vrai projet de gauche est de penser la désescalade et le droit à
l’égalité pour tous ceux et celles qui habitent cette terre.

A l’inverse, les extrêmes droites fascisantes espèrent une guerre de
civilisations et se réjouissent des massacres en cours.

**Notre judéité et notre engagement à gauche nous poussent à célébrer la vie**.

Ce shabbat lors du kiddouch, et à d’autres occasions, nous redirons
Le’haïm (A la vie).
Par nos émotions et réactions, aussi sincères et douloureuses soient-elles,
nous devons prendre nos distances avec ce fantasme nihiliste du choc
des civilisations.

**Ne confondons pas la pulsion de vie exprimée par celles et ceux qui, en
Israël comme en Palestine, luttent pour la paix, la justice et la
démocratie** avec les pulsions de morts désirées par le Hamas et
l’extrême droite israélienne.
