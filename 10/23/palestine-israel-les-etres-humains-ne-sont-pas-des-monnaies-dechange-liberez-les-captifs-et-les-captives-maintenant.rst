.. index::
   pair: SPI ; Société Psychanalytique d’Israël)


.. _etres_humains_2023_10_23:

==================================================================================================================================================
2023-10-23 ❤️❤️ **Palestine / Israël : Les êtres humains ne sont pas des monnaies d’échange – libérez les captifs et les captives maintenant**
==================================================================================================================================================

- https://www.europe-solidaire.org/spip.php?article68386



Selon les informations les plus récentes, au moins 222 personnes enlevées
lors de l’attaque du Hamas contre Israël le 7 octobre 2023, dont des soldats
et des ressortissant·es étranger·es, sont actuellement détenues dans la
bande de Gaza.

Parmi elles, on compte au moins 30 bébés, enfants et adolescent·es de
moins de 18 ans, entre 10 et 20 personnes âgées de plus de 60 ans, ainsi
que des personnes souffrant de troubles médicaux, de blessures ou de
handicaps.

Des dizaines de personnes étant toujours portées disparues depuis l’attaque,
le nombre de captifs est probablement plus élevé.

Les familles, les ami·es et les connaissances n’ont pratiquement aucune
information sur leurs proches, et ne savent même pas qui les détient et
s’iels sont en sécurité.

Jusqu’à présent, le Hamas a fait état de plusieurs captifs/captives tué·es
lors de frappes aériennes israéliennes.

Les ami·es et les parents ne savent pas si les captifs et les captives
reçoivent suffisamment de nourriture, d’eau et de vêtements, s’iels sont
autorisés à maintenir une hygiène de base, quel est leur état de santé
et s’iels reçoivent le traitement et les médicaments dont iels ont besoin.

Les circonstances des enlèvements montrent clairement que l’objectif
était de retenir les captifs et les captives en tant qu’otages.

Selon les médias, le Hamas a émis plusieurs demandes, principalement
celle de libérer les Palestinien·es des prisons israéliennes.

D’autres propositions auraient été de libérer certains captifs ou captives
en échange d’un cessez-le-feu humanitaire, ou de libérer les femmes et
les enfants en échange de la libération de femmes et de mineurs des
prisons israéliennes.

Selon un autre rapport, le Hamas aurait menacé d’exécuter les captifs et
les captives si Israël ne cessait pas de bombarder Gaza.

**La prise d’otages, c’est-à-dire l’enlèvement d’innocents et leur détention
comme monnaie d’échange, est immorale**.

**C’est pourquoi elle est absolument interdite par le droit international :
il n’y a ni exception, ni excuse, ni circonstance atténuante**.

Cette interdiction s’applique à tous les êtres humains – civils et combattants –
et est considérée comme faisant partie du droit international coutumier,
qui lie toutes les parties à un conflit armé, international ou non.

**La violation de cette interdiction constitue un crime de guerre et figure
en tant que tel dans le statut de Rome de la Cour pénale internationale de La Haye**.

L’interdiction de prendre des otages figure à l’article 3 commun à toutes
les conventions de Genève, qui est considéré comme une « convention en miniature ».

Cet article énonce les exigences minimales pour toutes les parties au
conflit et résume l’essence du droit international humanitaire.
Le commentaire du CICR sur les conventions de Genève précise que l’article
s’applique automatiquement à toute partie, qu’elle l’ait ou non accepté,
et qu’une violation par une partie ne justifie pas une violation par
une autre.

Selon la Convention internationale contre la prise d’otages, la différence
entre une arrestation légitime et une prise d’otages réside dans l’intention :
les otages sont détenus sous la menace d’un préjudice afin de forcer
un tiers « à accomplir ou à s’abstenir d’accomplir un acte en tant que
condition explicite ou implicite » de leur libération.

Le Hamas doit immédiatement libérer tous les otages, sans conditions.

D’ici là, il doit fournir des informations précises à leur sujet, veiller
à ce qu’iels soient en sécurité et protégés, et les traiter avec humanité
et dignité.
Les otages doivent recevoir les traitements et les médicaments dont iels
ont besoin, suffisamment de nourriture et d’eau, et dans des conditions
qui leur permettent d’avoir une bonne hygiène.

Israël doit agir maintenant pour conclure un accord de libération des
captifs et des captives.

Leur vie est en danger et l’État a le devoir de faire tout ce qui est en
son pouvoir pour les ramener.

Les rapports des médias et les déclarations des familles des captifs et
des captives indiquent que ce n’est pas le cas.

Au début de la guerre, l’ambassadeur israélien auprès des Nations unies,
Gilad Erdan, a clairement indiqué que la libération des captifs et des
captives n’était pas la priorité absolue d’Israël.

**Le coordinateur spécial nommé par le gouvernement israélien pour s’occuper
de la question ne semble pas faire suffisamment d’efforts pour les récupérer.
Si ces informations sont vraies, l’État abdique son rôle**.

Le prix de tout accord avec le Hamas est connu : Israël devra libérer des
milliers de prisonnier·es palestinien·es.

L’utilisation de prisonnier·es condamné·es comme atout dans les négociations
politiques peut susciter des objections.
Cependant, les prisonnier·es palestiniens·e sont condamnés par le système
judiciaire militaire israélien, qui a mis des centaines de milliers de
Palestinien·es derrière les barreaux depuis le début de l’occupation.

Contrairement au système judiciaire israélien, les tribunaux militaires
ne sont pas des arbitres neutres : ils servent des objectifs politiques
et œuvrent au maintien et à la perpétuation de l’occupation.

Les Palestinien·es n’ont pratiquement aucune chance d’être acquitté·es
dans ce système ; la plupart des affaires sont closes par des accords
de plaidoyer, et le taux de condamnation est presque de 100%.
Nombre de ces prisonnier·es sont des prisonnier·es politiques et des
milliers d’entre elles et eux ont été condamné·es sans preuve, sans
procédure régulière ou sans possibilité de se défendre.

Des centaines d’autres sont maintenu·es en détention administrative,
sans même pouvoir prétendre à un procès équitable.

Même s’il disposait d’un système judiciaire militaire équitable et
objectif qui recherche réellement la justice, Israël serait toujours
obligé de conclure un accord de libération de prisonnier·es afin de
libérer les captifs et les captives.

Les abandonner est un choix immoral que l’État ne doit pas faire.

Des dizaines de vies sont en jeu.

Le choix entre un sauvetage certain et un danger futur, réel ou non,
est évident.

23 octobre 2023

P.-S.

- Entre les lignes entre les mots, :
  https://entreleslignesentrelesmots.wordpress.com/2023/10/24/palestine-a-la-mort-et-autres-textes/

- https://www.btselem.org/israeli_civilians/20231023_human_beings_are_not_bargaining_chips_release_the_captives_now

- Traduit avec http://www.DeepL.com/Translator (version gratuite)
