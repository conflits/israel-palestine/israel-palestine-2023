.. index::
   pair: SPI ; Société Psychanalytique d’Israël)


.. _spi_2023_10_23:

=======================================================================================================================
2023-10-23 ❤️❤️ **Lettre écrite par des psychanalystes israéliens issus de la SPI (Société Psychanalytique d’Israël)**
=======================================================================================================================

- :ref:`chinsky:nouvelles_2023_10_16`

Nous, Psychanalystes Israéliens, psychanalystes d’enfants comme d’adultes,
à l’instar de tous les citoyens israéliens, sommes sous l’effet d’un choc
traumatique sans précédent à la suite des agissements odieux perpétrés
par des terroristes palestiniens du Hamas le 7 octobre 2023 dans des kibboutz,
villages et agglomérations du Sud d’Israël.

Dans les journées qui viennent de s'écouler nous avons été confrontés à
une terrifiante réalité : celle du massacre de civils : enfants, femmes
et personnes âgées dans leurs maisons, de l’enlèvement d’autres personnes
vers Gaza, accompagné de sévices sexuels et humiliations de toutes sortes,
qui sont autant d’atteintes à la dignité humaine, et ressortissent de
l’indicible.

La cruauté, le sadisme dirigées contre des bébés, des enfants et de jeunes
adolescents, beaucoup d’entre eux ont été mutilés à vif, violés et torturés
après avoir été forcés d’assister à l’assassinat de leur leurs parents,
et parfois de leur famille tout entière, relève de l’impensable.

Ceci ne relève pas de la seule et implacable haine des Juifs (que le Hamas
propage de manière tout à fait ouverte et claire dans ses réseaux officiels),
ni d’un conflit de nature territorial ou religieux, mais d’un
crime contre l’humanité.

Notre préoccupation essentielle aujourd’hui ce sont les enfants kidnappés
à Gaza, et qui nous osons l’espérer sont encore en vie, et tenus captifs
par des terroristes assassins.

Certains de ces enfants sont devenus brutalement et sans aucune pitié,
orphelins d’un instant à l’autre, alors que les terroristes violaient
leurs parents, pour les tuer ensuite, sous les yeux de leurs enfants
obligés d’assister à ces scènes effroyables.

Les terroristes tiraient sur le corps des parents, les brûlant ensuite,
tout ceci devant les yeux des enfants.

En même temps les terroristes fêtaient le massacre, tout en le filmant
et le diffusant tous azimuts sur leurs réseaux, via leurs téléphones
portables.

Une de leurs vidéos qui montre un jeune enfant errant dans les rues
de Gaza, brutalisé par les enfants palestiniens a envahi la toile, des
images qui nous hantent et qui ne peuvent être métabolisées par un
cerveau qui se dit humain.

Le fait de cibler de manière délibérée et sadique des bébés et des
enfants pour les violer, les torturer et les assassiner est une manière
de faire voler en éclats ce qu’il y a de plus fondamentalement humain
en l’humain.

Tentative de tuer toute sensibilité à la vulnérabilité, au développement
de l’humain, à l’amour et la bonté.

**C’est une culture infiltrée par la mort, la haine et la destruction.**

Ceci n’a rien à voir avec toute forme de revendication territoriale ou
de conflit quel qu’il soit, mais a tout à voir avec une idéalisation
perverse de la mort et de la torture.

Ainsi, ceux qui ont perpétrés ces crimes, se sont sortis de fait de toute
inscription possible dans l’humain.

Nous appelons solennellement tout gouvernement et toute groupement ou
association, à s’exprimer haut et fort pour condamner ces exactions
monstrueuses et ceux qui les ont commises et à demander la libération
immédiate et sans condition des enfants enlevés à Gaza.

Rédaction : Tal Sharir Wolpe, Ehud Wolpe, Joshua Dorban, Psychanalystes
formateurs pour enfants et adultes.

Signataires : Viviane Chetrit- Vatine , Présidente de la Société Psychanalytique en
Israël Vered Nevo, Vice-Présidente, Eva Yakobov, Trésoriere, Tsili Granek, Secrétaire
générale.

