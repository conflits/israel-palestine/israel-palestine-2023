
========================================================
2023-11-21 **Avancée pour une partie des otages**
========================================================


#Otages israéliens: c'est la mobilisation de la population qui a contraint
Netanyahou à accepter un accord dont il ne voulait pas.

Les familles des otages ont multiplié les manifestations pour l'imposer
 (voir ci-dessous)

L'accord incluerait une trêve de 4 jours et la libération de 50 otages
dont 30 enfants avec 8 de leur mères et 12 autres femmes ainsi que 150
prisonniers-ères palestinien-nes.

Les ministres d'extrême-droite s'opposent formellement à cet accord
qu'ils considèrent comme contraire à l'offensive militaire.

Il faut maintenant obtenir la libération de tous les otages, dont le sort
est en danger permanent et un cessez-le-feu à Gaza.

Sur la pancarte ci-dessous "Quel est le prix de mes enfants," et sur
le T-Shirt '" limogeage immédiat ( de Netanyahou)

.. figure:: images/manifestation.png
