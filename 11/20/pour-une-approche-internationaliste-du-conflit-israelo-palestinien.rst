.. index::
   pair: Janette Habel; Pour une approche internationaliste du conflit israélo-palestinien (2023-11-20)

.. _habel_2023_11_20:

======================================================================================================
2023-11-20 **Pour une approche internationaliste du conflit israélo-palestinien** par Janette Habel
======================================================================================================

:download:`Télécharger l'article au format PDF <pdfs/article_janette_habel.pdf>`

L'auteure Janette Habel
==============================

Janette Habel est maîtresse de conférences à l’Institut des hautes études d’Amérique
latine, elle est membre du Conseil scientifique d’Attac


Préambule
=============

« Que nous soyons Israéliens ou Palestiniens, Libanais, Syriens,
juifs ou musulmans, chrétiens ou athées, Français ou Américains, nous
ne nous méfierons jamais assez du recours au « nous contre eux » qui signe
fatalement le début de l’obscurantisme et de la cécité ».  

Dominique Eddé, Le Monde, 1er et 2 novembre 2023 

Pour une approche internationaliste du conflit israélo-palestinien
====================================================================
 
Au moment où nous écrivons ces lignes, les bombardements de Gaza par l’armée israélienne infligent une punition
collective à une population palestinienne soumise à un blocus total, sans
eau, sans nourriture, sans électricité, sans médicaments. 

La situation des hôpitaux est désastreuse, la morphine manque, des chirurgiens font
des opérations chirurgicales avec du paracétamol. En Cisjordanie les colons
attaquent les Arabes et les Bédouins en appelant à leur expulsion, couverts par
le gouvernement israélien et ses ministres d’extrême droite. L’embrasement
régional menace. Les échanges de tirs sont quasi-quotidiens entre l’armée
israélienne et des groupes armés pro-palestiniens, dont le Parti de Dieu,
le Hezbollah libanais. Pour faire bonne mesure les actes antisémites explosent
dans le monde. C’est dans ce contexte que le débat en France s’est engagé
de façon « campiste » (pour ou contre un camp pris en bloc), c’est-à-dire
de la pire des manières. Ne pas trouver de justification aux actes de barbarie
d’où qu’ils viennent devrait être une exigence pour tous. Pour rendre
ce conflit intelligible il faut revisiter l’histoire.  Les réflexions
qui suivent discutent certaines des approches historiques de la gauche. Cette
relecture intervient dans un moment de crise alors que deux peuples se vouent une
destruction mutuelle.  Un tournant majeur dans le conflit israélo palestinien
« Le sang coule à Gaza et vous polémiquez encore sur le Hamas ? » Cette
interrogation résume à elle seule la perte de repères d’une partie de
la gauche radicale pour qui les bombardements israéliens disculpent le Hamas
des tueries qu’il a perpétrées le 7 octobre. L’oppression subie par le
peuple palestinien depuis des décennies, la politique criminelle et raciste du
gouvernement Netanyahu relativisent- elles les atrocités du Hamas ? Oubliés
la terreur exercée par le Hamas envers des civils, les femmes éventrées,
les enfants décapités, les assassinats en direct de vieillards, les viols,
des actes barbares ouvertement antisémites… Tout cela a été planifié,
filmé, diffusé sur les réseaux sociaux comme l’avait fait Daech en
filmant les exécutions en Syrie. Pourtant les appels à manifestation en
soutien au peuple palestinien ont fait l’objet d’âpres négociations pour
que figurent dans les appels, les affiches, la libération des otages et la
dénonciation des crimes terroristes du Hamas. Elles ont souvent débouché sur
un refus au motif que la défense inconditionnelle des « colonisés » devait
primer sur la critique de leurs méthodes de lutte contre les « colonisateurs
». Étrange conception qui identifie les peuples à leurs dirigeants, et en
l‘occurrence les Israéliens à leur président, alors même que Netanyahu
était conspué quelques semaines auparavant par des centaines de milliers de
manifestants israéliens. Cette vision campiste compromet les possibilités
d’une solution politique en ne prenant pas en compte l’histoire longue des
deux peuples. Pour les juifs israéliens, les carnages du Hamas ont réveillé
l’angoisse de la solution finale en menaçant l’existence d’Israël
qu’ils voient comme garant ultime de leur sécurité. La destruction des
juifs d’Europe les hante toujours. Pour les Palestiniens écrasés sous
les bombes, la folie meurtrière du gouvernement de Netanyahu 2 réveille les
spoliations subies et leur expulsion, la Nakba.  Mais écrire que les massacres
du 7 octobre ont ravivé le traumatisme de la Shoah, c’est s’exposer au
soupçon. N’est-ce pas cautionner l’instrumentalisation qu’en fait le
gouvernement Netanyahu ? Enzo Traverso1 récuse cette idée. Pour lui « le
7 octobre a été un massacre épouvantable mais le qualifier de plus grand
pogrom de l’histoire après l’Holocauste signifie suggérer une continuité
entre les deux. Cela induit une interprétation assez simple : ce qui s’est
passé le 7 octobre n’est pas l’expression d’une haine engendrée par
des décennies de violences systématiques et de spoliations subies par les
Palestiniens ; c’est un nouvel épisode dans la longue séquence historique
de l’antisémitisme qui va de l’antijudaïsme moyenâgeux jusqu’à la
Shoah en passant par les pogroms dans l’Empire des Tsars. Le Hamas serait
donc le énième avatar d’un antisémitisme éternel. Cette lecture rend
inintelligible la situation, cristallise ces antagonismes et sert à légitimer
la réponse israélienne ». Mais réfuter le qualificatif de pogrom pour lui
substituer le vocable de « massacre épouvantable » n’est-ce pas nier le
caractère antisémite, affiché par les agresseurs eux-mêmes, de ce qui
s’est passé le 7 octobre ? Le fait que les auteurs de ces faits soient
des Palestiniens, eux-mêmes victimes de violences, implique-t-il de cacher
leur caractère antisémite ? Enzo Traverso s’interroge : « le Hamas serait
donc le énième avatar d’un antisémitisme éternel ».  Le dernier avatar
hélas non, mais un avatar oui, comme sa Charte, même expurgée en 2017, en
témoigne.  Enzo Traverso poursuit : « le 7 octobre n’est que l’expression
d’une haine engendrée par des décennies de violences systématiques et
de spoliations subies par les Palestiniens et non un nouvel épisode dans la
longue séquence historique de l’antisémitisme qui va de l’anti-judaïsme
moyenâgeux jusqu’à la 1 Enzo Traverso, « La guerre à Gaza brouille la
mémoire de l’holocauste », Mediapart, 19 novembre 2023.  Shoah en passant
par les pogroms…». En réalité le 7 octobre fut les deux à la fois :
un antisémitisme islamiste ancien ravivé et déchaîné par la politique
annexionniste des gouvernements nationalistes israéliens.  Qualifier d’une
part de « classique guerre de partisan » les actes terroristes planifiés et
soigneusement préparés du Hamas aux cris de « mort aux juifs », et d’autre
part de « guerre génocidaire » la riposte du gouvernement israélien tuant
des milliers de civils pour détruire le Hamas, c’est ajouter la confusion à
la confusion. Le summum de la confusion étant de comparer la guerre à Gaza
avec la guerre du Vietnam. Où et quand les combattants vietnamiens ont-ils
organisé des actes terroristes sur le sol américain ? Où et quand ont-ils
appelé à la destruction des USA ? Mal nommer les choses ajoute au malheur du
monde disait Albert Camus.  Nous sommes au cœur des débats. Il ne s’agit
pas d’exonérer le gouvernement israélien de ses responsabilités premières,
de la politique d’apartheid qu’il applique dans les colonies et des crimes
de guerre qu’il commet à Gaza.  Mais en identifiant le peuple israélien au
gouvernement d’extrême droite de Netanyahu et en considérant le Hamas –
mouvement islamiste réactionnaire – comme le représentant légitime du peuple
palestinien, on justifie le « nous contre eux » et la haine qui va avec. On
désarme les opposants à Netanyahu comme les opposants au Hamas. Réduire le
conflit israélo- palestinien à la guerre des « colonisés » contre les «
colonisateurs », des « occupés » contre « l’occupant », c’est oublier
les différenciations de classes, les oppressions spécifiques de genre et
les clivages politiques qui existent au sein des deux nations, israélienne
et palestinienne. L’arrêt des bombardements, la reconnaissance d’un Etat
palestinien viable, supposent un changement radical de la politique israélienne
et de ses gouvernants, et dans l’immédiat, le départ du gouvernement
Netanyahu et de ses ministres racistes. « Avant de parler d’une solution à
deux États, il faudra que ce gouvernement 3 disparaisse » affirme Charles
Enderlin2.  Mais pour que les Israéliens se mobilisent pour cet objectif,
il faut que cessent les menaces que font peser les appels du Hamas à la «
destruction de l’État sioniste » et les pogroms aux cris de « mort aux juifs
». Une dialectique complexe souvent incomprise.  Le récit de Yaniv Iczkovits,
écrivain et ancien soldat, résume mieux que tout commentaire l’état
d’esprit de nombreux Israéliens : « Le 7 octobre m’a changé. En 2002,
comme des centaines d’officiers et de soldats j’ai refusé de servir dans
les territoires occupés… J’ai payé le prix de ce refus : on m’a envoyé
pour cela 28 jours dans une prison militaire3… Aujourd’hui je suis toujours
convaincu que l’occupation israélienne est immorale et que les extrémistes
israéliens veulent anéantir toute possibilité de réconciliation… Reste
que pour cette guerre-ci je pars combattre sans hésiter. Pourquoi ? À mon
avis ce qui s’est passé le 7 octobre est sans lien avec l’occupation…
ce qui s’est passé le 7 octobre c’est qu’une organisation terroriste
qui contrôle de force la vie de millions d’habitants de la bande de Gaza
a envoyé des milliers de terroristes assassiner, massacrer, violer, brûler
des civils innocents »… « Laisser une organisation terroriste exister
à nos frontières signifie compter les jours jusqu’au prochain drame.
C’est pour cette raison que nous nous battons maintenant4 ».  Il n’y
a pas de symétrie entre la situation du peuple palestinien aujourd’hui
sous les bombes et la population israélienne. Mais il s’agit de comprendre
l’interaction, difficile mais nécessaire, qu’il faut construire entre les
deux peuples. C’est admettre que pour des raisons historiques, un changement
radical en Israël ne peut intervenir que si la sécurité de sa population,
et donc l’existence de son Etat, sont assurées. Rappeler la barbarie
des actes commis par le Hamas, ce n’est pas minimiser les bombardements
subis par la population palestinienne. C’est aider à la mobilisation 2
Ch. Enderlin, AOC media , 4 novembre 2023.  3 Le Monde, 1er-2 novembre 2023.
4 Le Monde 1er -2 novembre 2023.  des Israéliens. « Nous insistons sur le fait
qu’il n’y a pas de contradiction entre le fait de s’opposer fermement
à l'assujettissement et à l’occupation des Palestiniens par Israël et
le fait de condamner sans équivoque les actes de violence brutaux commis
contre des civils innocents » ont déclaré 60 intellectuels israéliens.
De la fin et des moyens On peut faire un parallélisme entre le gouvernement
israélien et le Hamas : pour l’un comme pour l’autre, la fin justifie
les moyens, tous les moyens. Pour le gouvernement Netanyahu l’objectif est
l’expulsion des Palestiniens, un objectif que poursuit l’offensive en
cours. Le Hamas lui, veut « la destruction d’Israël », une « entité
sioniste illégitime et illégale ». Sa Charte prévoit toujours le Djihad
pour « la libération de la Palestine », cette « terre bénie et sacrée
». Les horreurs auxquelles on a assisté les 7 et 8 octobre illustrent les
impasses auxquelles mènent ces conceptions.  Si en France la critique du
gouvernement Netanyahu est partagée à gauche, la qualification du Hamas comme
organisation terroriste fait débat. Enzo Traverso, estimant d’autorité que
« les Palestiniens reconnaissent le Hamas comme une force armée qui résiste
à l’occupation », considère que « ce n’est pas à nous de dire qui fait
partie de la résistance palestinienne ». La conclusion logique est qu’il
ne nous appartient pas de dénoncer l’oppression du peuple gazaoui par le
Hamas, par ailleurs libre de tuer les juifs comme il l’entend ! En miroir,
Danièle Obono critique les propos de François Ruffin qualifiant le Hamas
d’organisation terroriste, au motif que « quand on est de gauche, on ne
sacrifie pas ses principes par opportunisme ». Mais de quels opportunistes
parle-t-on ? Et de quels principes s’agit-il ? Citons la réponse des
intellectuels israéliens, parmi lesquels l’écrivain David Grossman :
« À notre grand désarroi, certains éléments de la gauche mondiale,
des individus qui étaient jusqu'à présent nos partenaires politiques, ont
réagi 4 avec indifférence à ces événements horribles et ont même parfois
justifié les actions du Hamas. Légitimer ou excuser de tels actes revient
à trahir les principes essentiels de la gauche ». Le soutien inconditionnel
à toute action du Hamas n’est pas recevable, comme ne l’est pas tout
soutien inconditionnel à quelque organisation ou mouvement politique que ce
soit prétendant défendre les opprimés. Le Hamas use de moyens de terreur
contre les civils israéliens, il en a aussi usé contre son propre peuple. «
Quand on soutient des peuples opprimés, on doit choisir qui l’on défend,
et le Hamas ne doit pas en faire partie » écrit le député LFI Rodrigo
Arenas. Sa qualification d’organisation terroriste devrait découler de
ses actes. Les arguments juridiques faisant référence aux catégories du
droit international pour ne pas le qualifier ainsi masquent mal chez certains
l’absence de condamnation des crimes commis, dès lors qu’ils concernent
des civils identifiés indistinctement comme des « agents sionistes ». Une
identification d’autant plus fallacieuse que nombre de ceux qui ont été
massacrés dans les kibboutz le 7 octobre étaient des militants de gauche
luttant pour la paix.  Des conceptions binaires Ces conceptions binaires
ont une histoire, celle des luttes de libération nationale au XXe siècle,
celles de l’affrontement entre mouvements de libération et les puissances
impériales. Le soutien inconditionnel aux organisations combattantes les avait
exonérées de toute critique quels que soient les moyens employés. Mais
aujourd’hui, les causes anticoloniales aussi justes soient-elles, se
discréditent quand leurs défenseurs ont recours à des massacres de civils
et à un terrorisme aveugle. La relecture critique des luttes de libération
nationale du XXe siècle, des méthodes de lutte utilisées par certaines
organisations armées montrent comment ces moyens ont hypothéqué la fin
une fois le pouvoir conquis. L’exemple algérien en est une illustration.
Le bilan de l’anticolonialisme est l’objet d’instrumentalisations
divergentes et de nombreux débats. « Une des difficultés actuelles des
mouvements antiracistes et des mouvements pour l’émancipation en général
est la tendance à l’imposition du seul angle colonial pour lire le racisme
et l’oppression » remarque Philippe Corcuff.  Le philosophe Moshe Postone5
propose une réflexion critique sur la résurgence de « ces formes dualistes
d’internationalisme » et constate que « développer un internationalisme en
rupture avec le dualisme de la guerre froide semble être devenu extrêmement
difficile dans le monde d’aujourd’hui ». Évoquant la guerre en Irak, il
observe « que les progressistes ont été confrontés à une situation qu’ils
auraient dû comprendre comme un dilemme, un conflit entre, d’un côté,
une puissance impérialiste mondiale agressive et, de l’autre, un mouvement
anti-mondialisation profondément réactionnaire ». Paraphrasons Moshe Postone
: aujourd’hui le conflit oppose le gouvernement Netanyahu, une puissance
agressive et raciste, à un mouvement islamiste réactionnaire. La critique
radicale du gouvernement israélien ne doit pas servir à excuser Le Hamas qui
se définit lui-même comme un Mouvement de la Résistance islamique. Fondé
en 1987 par les Frères Musulmans lors de la première Intifada, voilà ce
qu’écrit l’historien Jean Numa Ducange : « L’islamisme, idéologie
politico-religieuse disposant de ramifications étatiques et internationales
puissantes, a fait de la haine du juif d’où qu’il soit un de ses thèmes
privilégiés. Il a écrasé – et continue à le faire dès que c’est
nécessaire – tout courant de gauche ou issu du mouvement ouvrier qui
pourrait contester son hégémonie. La lutte des classes est reléguée aux
oubliettes de l’histoire au profit d’une vision archaïque et essentialiste
des peuples. En France, comme dans de nombreux pays, ce contexte contribue à
raviver un antisémitisme qui se portait déjà bien. Revêtant de nouveaux 5
Moshe Postone, Critique du fétiche capital, Le capitalisme, l’antisémitisme
et la gauche, PUF, 2013.  5 habits, il prolonge aussi de vieilles traditions
solidement ancrées. N’en déplaise à quelques esprits mal informés ou
pratiquant sciemment une mémoire sélective, cet antisémitisme parcourt tout le
spectre politique ».  Le Hamas contrôle la bande de Gaza depuis qu’en 2006
il a gagné les élections législatives. Plus que de la remise en cause de la
politique israélienne de colonisation en violation du droit international, il
s’agit d’un vieux fond d’antisémitisme sous-couvert d'antisionisme. Le
Hamas ne dit jamais « les Israéliens » mais « les juifs ». Ce discours
a contaminé l’Autorité palestinienne, ou du moins son président Mahmoud
Abbas. Le 24 août dernier lors d’un discours prononcé devant son parti,
il s’est emporté dans un argumentaire antisémite et négationniste :
« Ils disent qu’Hitler a tué les juifs parce qu’ils étaient juifs…
C’est faux.  Les Européens ont combattu ces gens en raison de leur rôle
dans la société qui avait trait à l’usure, à l’argent… ». Sans doute
Mahmoud Abbas pensait-il ainsi se dédouaner de la corruption et du discrédit
qui expliquent le déclin de son mouvement.  L’antisémitisme est très
utile pour cibler des boucs émissaires, dresser les peuples les uns contre
les autres et masquer l’incurie de certains dirigeants arabes. Il ne faut
cependant pas oublier que la puissance du Hamas est due au soutien manipulateur
du gouvernement Netanyahu, qui a tout fait pour affaiblir l’Autorité
palestinienne.  Le sionisme, un nationalisme juif De quoi le sionisme est-il le
nom ? À la fois diabolisation de l’État israélien qu’il faut détruire,
machination machiavélique contre les Palestiniens, le sens originel du sionisme
est effacé. Le « sionisme » désigne le projet politique visant à créer un
État juif. La naissance de ce mouvement nationaliste est « un sous-produit de
l’antisémitisme et son développement s’avère en dernière instance une
conséquence du non-avènement du socialisme » constatait Nathan Weinstock. À
l’origine, ce mouvement nationaliste était loin de faire l’unanimité,
il était même minoritaire dans plusieurs grandes villes de Pologne. Des
conflits nombreux l’opposaient au mouvement ouvrier juif qui jouait un rôle
déterminant dans les luttes de classes, lors des combats contre l’empire
tsariste, à l’appel de dirigeants socialistes qui ont nom Rosa Luxembourg,
Leon Trotsky qui eux se battaient pour une révolution socialiste internationale.
Toute l’histoire du conflit judéo-arabe puis israélo-palestinien est marquée
du sceau de l’instrumentalisation par les grandes puissances de l’émigration
des juifs qui fuyaient les pogroms pour aller s’installer dans la Palestine
ottomane de la fin du XIXe siècle, tous abusivement qualifiés de sionistes. «
Il n’existe pas de concept plus confus que ceux de « sionistes » ou de «
sionisme » écrit Abraham Yehoshua6.  Jusqu’à la création de l’État
d’Israël la définition de sioniste désignait la volonté de fonder un
État juif sur la terre d’Israël (une appellation biblique). Pour Theodore
Herzl le fondateur du sionisme, il s’agissait bien de lutter pour un État,
L’État des juifs – titre de son livre – seule solution pour échapper
aux persécutions éternelles. Un État, c’est-à-dire « une existence juive
souveraine » précise A. Yehoshua. Même si l’aspiration à fonder un État
faisait partie de la vision sioniste, tous les sionistes ne pensaient pas à
un État : pour certains il s’agissait d’une autonomie territoriale, ou
d’une Fédération, pour d’autres d’un Foyer national juif 7, voire un
Centre spirituel. « Le lien causal entre les persécutions raciales et les
progrès du nationalisme juif est évident » observe N. Weinstock8. Dès le
début du XXe siècle un Fonds national juif est créé, il facilite l’achat
des terres avec l’aide du gouvernement britannique. Mais les milliers de
juifs qui arrivent en Palestine, nombreux après 6 Pour une normalité juive,
Ed.Liana Lévi, 1981, p.  90.  7 Le Mandat britannique sur la Palestine date
de 1923.  Il entérinait la Déclaration Balfour, il soulignait « les liens
historiques du peuple juif avec la Palestine » et l’objectif d’y créer
un Foyer national juif.  8 Nathan Weinstock, Le sionisme contre Israël, Ed.
François Maspéro, 1969.  6 l’arrivée de Hitler au pouvoir, ne sont
pas tous des colonisateurs. Ces migrants sont des réfugiés, des familles
qui s’installent en Palestine après la vague de pogroms qui sévit en
Russie et en Pologne à partir de 1881. Ils espèrent y trouver « un asile
permanent » selon l’expression de Léon Pinsker9 un refuge et la sécurité,
d’autant qu’il existe déjà une minorité juive ancienne en Palestine,
aux côtés de la présence majoritaire des Arabes et des Palestiniens. Les
conflits nés de l’occupation des terres appuyés par la Grande-Bretagne
et les manifestations d’antijudaïsme vont provoquer le départ d’une
partie des migrants. Beaucoup se réfugieront aux États-Unis, en France, en
Argentine.  Pour affirmer le droit à l’existence de l’État d’Israël,
des non-juifs se sont définis eux-mêmes comme sionistes, Régis Debray se
déclarant plus précisément « sioniste pro- palestinien » pour justifier un
droit égal pour les Palestiniens. Aujourd’hui l’extrême droite religieuse
israélienne se revendique du sionisme, ce qui conforte ceux qui à l’extrême
gauche se disent antisionistes pour signifier leur condamnation de la politique
israélienne. Certains antisionistes non antisémites estiment que la création
de l’État d’Israël a été une erreur qu’il faudrait corriger tandis
que d’autres prétendument antisionistes sont purement et simplement des
antisémites. Il existe également des antisémites d’extrême droite
soutenant le gouvernement de B. Netanyahu.  De l’« Entité sioniste » à
l’État colonial La création de l’État israélien né de la décision
de l’ONU10 de novembre 1947 a coïncidé avec la Nakba, la « catastrophe
» en arabe. Elle fut suivie de l’expulsion et de la spoliation de 760
000 palestiniens, puis de l’occupation des territoires annexés en 1967.
Par une bifurcation de l’histoire, elle a accouché d’une nation, une
nation israélienne où vivent des citoyens juifs, arabes (21 % de 98 Léon
Pinsker, Autoémancipation, Jérusalem, 1958.  10 Lors du partage de la Palestine
mandataire décidé à l’ONU le 29 novembre 1947.  la population), chrétiens,
druzes et d’autres minorités. Les Arabes israéliens ont un parti et des
députés. Bien que disposant des mêmes droits individuels que les juifs
ils font l’objet de discriminations spécifiques liées à la définition
de l’État comme État juif. Dès sa naissance, l’État israélien a
d’abord été un État refuge. Les millions de juifs qui vont le rejoindre
après 1948 sont tous inspirés par une même conviction : l’extermination
de six millions de juifs pendant la deuxième guerre mondiale a eu lieu dans
l’indifférence générale des puissances alliées. L’histoire a montré
qu’ils ne sont en sécurité nulle part.  Ne pouvant compter sur personne,
ils doivent désormais se défendre seuls, quel qu’en soit le prix, y compris
militairement.  L’État israélien a 75 ans. Presque un siècle pendant lequel
la guerre n’a jamais cessé. Pendant ces années, sa légitimité a toujours
été contestée au motif incontestable que cet État s’était construit en
expulsant un autre peuple. « Il ne s’agit pas d’un conflit ordinaire entre
deux nations » déclarait dès 1967 l’Organisation Socialiste Israélienne
(Matzpen). Le fait national israélien, cette anomalie singulière, a toujours
été méconnu, nié, à la fois dans le monde arabe et par certains secteurs de
la gauche préconisant, en défense des Palestiniens spoliés, la destruction de
cette « entité sioniste » illégitime, très tôt assimilée à une conquête
coloniale traditionnelle. Cette approche soulève plusieurs interrogations liées
au contexte historique de l’après-guerre. Elle interroge aussi aujourd’hui.
Après le génocide, après les chambres à gaz, quelle réponse fallait-il
apporter aux centaines de milliers de juifs errants qui avaient survécu à la
recherche d’un asile qui leur était refusé ? Où devaient-ils aller ? En
Pologne où des pogroms persistants les avaient accueillis à leur retour des
camps11 ?  En Afrique comme cela leur avait été proposé ? Refoulés des ports
où ils tentaient d’accoster (y compris à Cuba), entassés sur des bateaux
tels Exodus, sans base arrière, sans « chez eux » où se réfugier. Les
grandes 11 Jan T. Gross, Les Voisins. 10 juillet 1941. Un massacre de Juifs
en Pologne, Fayard, 2002.  7 puissances se sont défaussées de leur silence
pendant le génocide en parrainant la naissance d’un État sur le territoire
palestinien. Oubliant leurs responsabilités propres, elles ont fait du sionisme
la réponse au droit à un État « pour un peuple en détresse 12». Un ancien
militant trotskyste aujourd’hui disparu qui contestait la légitimité de la
création d’Israël, à qui je demandais quelle était sa solution, m’avait
répondu « il fallait se battre pour l’ouverture des frontières »… Le
droit à la sécurité pour un « peuple en détresse » et donc le droit à
un État ne concerne pas seulement le peuple juif. Dès 1967, l’Organisation
socialiste israélienne (Matzpen) rappelait que le problème kurde n’était
toujours pas résolu. Aujourd’hui, ce qu’on pourrait qualifier de ruse de
l’histoire si la situation n’était pas aussi tragique, a fait du peuple
palestinien « un peuple en détresse » qui a droit à un État pour assurer
sa sécurité.  Le projet de fonder un État juif dans une Palestine arabe
grâce à la protection de la Grande-Bretagne et de ses intérêts propres,
allait se heurter aux dirigeants nationalistes arabes appelant à la guerre
sainte (Djihad) pour la libération de la Palestine. La propagande des régimes
arabes sur l’extermination de la population israélienne provoquera une
psychose collective13. Nathan Weinstock l’avait déjà reconnu il y a plus
d’un demi-siècle: « Il ne sert à rien de se lamenter sur le passé. Le
fait est qu’il existe désormais au cœur du monde arabe une communauté
israélienne qui présente d’indéniables caractéristiques nationales ».
Le fait national israélien avait été également reconnu dès 1966 par les
organisations étudiantes arabes de Paris qui affirmaient dans une résolution
en mai 1966 14: « Il est évident que notre opposition légitime et résolue
à l’existence de l’État d’Israël ne peut nous empêcher d’affirmer
hautement que la finalité des patriotes palestiniens n’est 12 L’expression
est d’Abraham Yehoshua.  13 Ibid. N.W. p. 562-563.  14 Ibid. Cité par N.W.,
p. 562.  pas de rejeter les populations civiles juives de Palestine à la
mer » se différenciant ainsi des positions de l’OLP de l’époque. Une
opinion rejetée par d’autres organisations nationalistes arabes prêchant
toujours la destruction de l’« État sioniste » dont les conditions de
la naissance interdiraient l’existence faute de légitimité historique.
Cette assertion conduit à remplacer la question territoriale (un territoire
pour deux peuples), par une absurde dispute historico- religieuse pour savoir
« qui étaient présents les premiers ? ».  Les ambigüités d’une
comparaison Le rapprochement entre l’État israélien défini comme un
État colonial et l’Algérie, les Israéliens étant assimilés aux «
pieds noirs », est-il pertinent ? Le soutien apporté par l’impérialisme
américain à l’État israélien et l’allégeance politique dont font
preuve les gouvernements israéliens ont renforcé cette comparaison. Loin
de se limiter à une caractérisation théorique abstraite, la définition
de « la nature coloniale » de l’État d’Israël a eu des prolongements
politiques délétères. La grille de lecture tiers-mondiste opposant un
État colonial, exploiteur qu’il faut détruire, à un peuple colonisé, le
peuple palestinien, compromet la formulation d’une stratégie cherchant à
séparer la majorité du peuple israélien de ses directions. L’éditorial
de la revue Contretemps web qui revendique son « rattachement à la tradition
de la gauche anticoloniale et internationaliste, celle qui a notamment soutenu
l’indépendance de l’Algérie » en est un exemple. On parle d’une guerre
entre « Israël » et « le peuple palestinien », d’un État Israël avec son
armée face à un peuple opprimé. De classes sociales, il n’est pas question.
Qualifiant « l’offensive du 7 octobre » d’« opération militaire d’une
ampleur inédite des forces armées palestiniennes dirigées depuis Gaza par
le Hamas » selon les termes mêmes du Hamas, l’éditorial de la revue estime
que « l’issue au conflit israélo- 8 palestinien ne peut émerger que dans la
lutte contre le colonialisme, pour la libération de la Palestine. Le système
colonial est intrinsèquement violence, destruction et apartheid (la récente
qualification des Palestiniens de Gaza d’« animaux humains » par un ministre
israélien en est une expression), seule sa désintégration peut ouvrir une
véritable ère de paix, délivrant à la fois les Palestiniens et Israéliens
d’aujourd’hui, qui enfin pourraient vivre ensemble libres et à égalité
».15 Les expressions obscènes du ministre israélien sont ici rappelées pour
conforter la légitimité de l’objectif de la « libération de la Palestine »
par la « désintégration du système colonial ». Et les auteurs de conclure
que cette « désintégration pourrait ouvrir une véritable ère de paix »
permettant aux Palestiniens et aux Israéliens d’aujourd’hui « de vivre
ensemble libres et à égalité » !  Angélisme ou ignorance du degré de
conflictualité nationaliste ? Le fanatisme religieux qui prévaut hélas au
sein de secteurs des deux populations interdit pour un temps indéterminé leur
cohabitation au sein d’un État binational.  L’éditorial suggère-t-il
l’évacuation de la terre de Palestine par les Israéliens ? Des pieds noirs
qui n’auraient qu’à retourner d’où ils viennent, dans leur métropole
d’origine ?  mais quelle métropole ? Certains laissent entendre qu’ils
pourraient peut-être s’établir aux États-Unis. Comme le remarquent les
Jeunes Juives et Juifs Révolutionnaires (JJR), aujourd’hui la majorité
des juifs Israéliens sont des « sabras », nés en Israël, c’est leur
pays et ils n’en ont pas d’autre. Le qualificatif d’« État colonial »
entretient la confusion en mettant dans le même sac les colonies de Cisjordanie
et l’État d’Israël et en réunissant sous le même drapeau les partisans
du démantèlement des colonies et les partisans de la destruction d’Israël.
Cette assimilation de l’État israélien à un État colonial comme un
autre résiste mal à l’analyse et pose des problèmes politiques.  Aucune
métropole ou mère-patrie n’est à 15 Contretemps, version électronique,
6 novembre 2023.  l’origine de l’exode des juifs en Palestine à la
différence d’autres conquêtes coloniales, sauf à identifier la Fondation
Rothschild – qui a financé l’installation d’une partie des réfugiés
– à un État.  À la différence du corps expéditionnaire français dont on
connaît le rôle dans la colonisation de l’Algérie, quelle armée de quel
État a accompagné la colonisation juive ? Celle-ci répondait-elle à des
impératifs d’exploitation des ressources ?  Consternée par la confusion
qui règne sur cette question notamment au sein de la gauche décoloniale,
Sandrine Rousseau remarque que « La situation d’Israël n’a rien à voir
avec celle des colonies françaises. La colonisation dont nous avons été
les auteurs est une colonisation d’intérêt économique, de puissance
nationaliste. Israël est un pays dont une partie de la création et du
développement vient d’un réflexe de survie post génocide ». Les États
coloniaux classiques se livraient au pillage des ressources du pays conquis,
alors que le processus de la colonisation en Palestine était guidé par
des considérations sécuritaires.  L’immigration juive n’était pas le
fait d’un État, à la différence de la France, de la Grande-Bretagne, de
l’Espagne, de la Belgique, du Portugal.  Après la guerre, le peuple juif
avait même été accusé de ne pas avoir résisté à la déportation. Menacé
de disparition, il avait le droit de se défendre, le droit à une armée,
le droit à un État. Ce droit n’aurait pas dû être exercé aux dépens
d’un autre peuple, mais la responsabilité essentielle n’en incombe pas
au peuple juif mais à l’ensemble des grandes puissances qui non seulement
avaient abandonné le peuple juif, mais au lendemain de la guerre refusaient
d’ouvrir leurs frontières.  La qualification d’État colonial mélange
des réalités et des histoires politiques distinctes. Des universitaires
palestiniens et arabes ont adopté le colonialisme de peuplement comme
catégorie politique dont les caractéristiques le distinguent des autres
formes de colonialisme. Pour ces auteurs « la terminologie de colonialisme de
peuplement 9 permettrait de déconstruire les idées dominantes qui prévalent
aujourd’hui, telles que la croyance que les racines du conflit se situent dans
l’identité ethnique, religieuse ou nationale »16. Cette « déconstruction »
vise à nier la complexité de ce conflit territorial – deux peuples pour un
seul territoire – sur lequel se greffent des conflits ethniques et religieux
interdisant pour les uns et les autres le partage de la « Terre sainte ». On
cherchera en vain dans ces propositions un rappel du contexte historique dans
lequel a eu lieu la colonisation juive avant et après 1948.  Jusqu’en 1948,
le mouvement sioniste avait acquis moins de 10 % du territoire de la Palestine
mandataire, difficile d’évoquer « un colonialisme de peuplement ». Mais
après 1948, les annexions en Cisjordanie se sont développées. Alors que
l’État israélien est reconnu par le droit international, ces colonies de
peuplement sont condamnées par ce même droit17 qui interdit à une puissance
occupante de transférer sa propre population dans le territoire qu’elle
occupe. L’argument sécuritaire utilisé par les autorités israéliennes
– la protection nécessaire face aux attaques de groupes armés – a été
rejeté.  À la différence de la qualification d’Israël comme État colonial,
inaudible par les Israéliens, la critique de la politique de colonisation en
Cisjordanie est davantage partagée.  Quelle stratégie internationaliste ?
Comment aider les masses israéliennes à contester les politiques de leurs
dirigeants ?  L’État israélien est un État capitaliste au sein duquel la
bourgeoisie exploite un prolétariat lui-même divisé entre juifs ashkénazes,
sépharades, Palestiniens surexploités, immigrés éthiopiens. Les classes
sociales existent bel et bien mais les conflits les opposant à leur gouvernement
sont 16 Palestine-israël : accumulation coloniale par dépossession Areej
Sabbagh-Khoury, Alternatives Sud, Anticolonialisme(s) (septembre 2023).  17 Cf
la Quatrième Convention de Genève de 1949.  subordonnés aux exigences de leur
sécurité et de leur protection. Lorsque dans le passé des groupes armés
palestiniens ont commis des attentats suicides dans des villes israéliennes,
tuant des civils dans des autobus, ces attentats ont provoqué un retournement
de l’opinion israélienne convaincue que les Palestiniens ne voulaient pas
la paix. Depuis lors, la droitisation de l’électorat n’a pas cessé,
renforçant les affrontements communautaires et les fanatismes religieux.
Il est à craindre que les massacres du 7 octobre produisent les mêmes
effets.  La société israélienne n’est pas homogène, ni socialement ni
politiquement. Le mouvement pro-démocratie l’a profondément divisée, « il
n’est pas mort, loin de là » affirme Ch. Enderlin. De nombreux Israéliens
demandent déjà des comptes au gouvernement de Netanyahu mais son renversement
n’est pas envisageable en temps de guerre. « Ce ne sera pas facile si les
messianiques au pouvoir poursuivent la mise en place du régime autoritaire
et théocratique dont ils rêvent », constate encore Ch. Enderlin. Pour les
orthodoxes religieux, la terre d’Israël a été donnée par Dieu au peuple
juif et il est interdit d’en céder ne serait-ce qu’un centimètre carré
à un étranger. Yitzhak Rabin Premier ministre israélien, a été assassiné
le 4 novembre 1995 par un religieux pour torpiller le processus d’Oslo. À
l’inverse, pour le Hamas et le Djihad islamique, « un État juif ne doit pas
exister en terre d’Islam. » C’est dire la difficulté d’une sortie de
crise. Celle préconisée par les instances internationales– la reconnaissance
de deux États conformément à la résolution 242 des Nations unies de novembre
1967 – suppose un État israélien comportant une minorité arabe et un État
arabo-palestinien comprenant une minorité juive, les minoritaires jouissant
des mêmes droits que les majoritaires. Ce qui suppose une implication directe
des instances internationales pour éviter que ne se reproduise la séquence
des Accords d’Oslo dont les dirigeants israéliens étaient en principe
les garants. La guerre en cours a 10 rendu cette solution politique encore
plus ardue.  Il n’y aura pas d’issue pacifique durable à ce conflit sans
qu’au sein des deux peuples émergent des voix capables de construire une
alternative de paix contre le gouvernement israélien, et contre le Hamas.
Contre le gouvernement israélien qui a fait adopter par la Knesset en 2018 une
Loi fondamentale définissant Israël comme « l’État-Nation du peuple juif
», niant ainsi l’existence des 21 % d’Arabes israéliens. Mais aussi contre
le Hamas qui a fait de la haine du juif un de ses sujets de prédilection. « Un
demi-siècle d’occupation a engendré un monstre à Gaza. Samedi 7 octobre,
il s’est réveillé » 18.  20 novembre 2023 Janette Habel est maîtresse de
conférences à l’Institut des hautes études d’Amérique latine, elle est
membre du Conseil scientifique d’Attac 18 B. Barthe, Le Monde, 15-16/10/2023.
