.. index::
   pair: Eva Illouz; Génocide à Gaza ? Eva Illouz répond à Didier Fassin

.. _eva_illouz_2023_11_15:

=======================================================================
2023-11-15 **Génocide à Gaza ?** Eva Illouz répond à Didier Fassin
=======================================================================

- https://k-larevue.com/genocide-a-gaza-eva-illouz-repond-a-didier-fassin/
- https://fr.wikipedia.org/wiki/Eva_Illouz

Introduction
==============

Israël est-il en train de commettre un « génocide » à Gaza ?

C’est ce que suggère Didier Fassin dans une tribune récemment publiée
sur le site de la revue AOC.

Une réponse lui a déjà été apportée par dans le même média par Bruno Karsenti,
Jacques Ehrenfreund, Julia Christ, Jean-Philippe Heurtin, Luc Boltanski
et Danny Trom.

Ici, Eva Illouz critique la méthode du sociologue qui biaise toute son
argumentation.

Selon elle, **dans la période tourmentée que nous vivons, choisir les
mots justes est un devoir moral et intellectuel**.

Un texte publié en partenariat avec Philosophie Magazine.

**Génocide à Gaza ?** Eva Illouz répond à Didier Fassin
=======================================================

- https://www.philomag.com/articles/quelle-est-lideologie-de-lextreme-droite-israelienne
- https://k-larevue.com/la-faillite-morale-de-ma-gauche-une-vue-des-etats-unis/

La guerre entre le Hamas et Israël a déjà fait un nombre choquant
de victimes humaines de part et d’autre.

**Non moins choquante a été la réaction de la société civile et de l’opinion
publique des démocraties européennes et américaine**.

- nous savions la politique israélienne de `plus en plus indifférente <https://www.philomag.com/articles/quelle-est-lideologie-de-lextreme-droite-israelienne>`_
  au droit international ;

- nous savions que l’antisionisme cachait souvent le refus d’accorder
  aux Juifs ce qu’on reconnaît à tout autre peuple,

- nous savions que le conflit moyen-oriental irradiait sur les diasporas européennes.

**Nous ne savions pas qu’un massacre barbare de bébés, femmes enceintes, vieillards, civils pour la plupart dévoués à la cause de la paix, serait accueilli avec exultation ou indifférence**
==================================================================================================================================================================================================

**Mais nous ne savions pas qu’un massacre barbare de bébés, femmes enceintes,
vieillards, civils pour la plupart dévoués à la cause de la paix,
serait accueilli avec exultation ou indifférence par des musulmans au
travers du monde et par des universitaires, artistes et intellectuels
des démocraties occidentales**.

Par exemple, quelques jours après le massacre de civils du 7 octobre 2023
et avant l’opération à Gaza, 33 groupes d’étudiants à Harvard faisaient
une déclaration qui passait sous silence les victimes du massacre et
accusait Israël, et Israël seulement, des actions du `Hamas <https://k-larevue.com/la-faillite-morale-de-ma-gauche-une-vue-des-etats-unis/>`_.

Dans un esprit similaire, un grand nombre de pétitions ont circulé parmi
les artistes, les sociologues, les anthropologues, les organisations
d’étudiants.

**Que des crimes contre l’humanité soient justifiés ou passés sous silence par une partie conséquente de ces groupes est un fait politique autant que sociologique majeur**
============================================================================================================================================================================

**Que des crimes contre l’humanité soient justifiés ou passés sous silence
par une partie conséquente de ces groupes est un fait politique autant
que sociologique majeur** qui mérite d’être analysé pour ce qu’il se
donne, c’est-à-dire une position intellectuelle et morale.
Une méthode peu rigoureuse

Un article paru dans la revue AOC et signé par Didier Fassin, anthropologue
de renommée internationale, nous en fournit une illustration.

Si je prends cet exemple, c’est justement parce que Didier Fassin est
l’auteur d’une œuvre considérable et titulaire d’une chaire intitulée «
Morale et enjeux politiques dans les sociétés contemporaines »
au Collège de France – et donc **membre de l’institution de recherche la
plus prestigieuse de France**.


Le génocide des Hereros et Namas
===================================

- :ref:`hereros_namas`

Je résumerai rapidement le texte de Fassin : l’attaque du Hamas est analogue
à un évènement historique dont on se souvient peu, un massacre que les
Héréros d’Afrique ont perpétré en 1904 contre les colonisateurs allemands
qui s’étaient installés en Namibie deux décennies auparavant.

Les Allemands avaient privé les Héréros de leur terre et les avaient
réduits en esclavage.
En réaction, dans un raid qui est resté dans l’histoire de cette région,
les Héréros tuent une centaine d’Allemands, et cette attaque est vécue
comme une humiliation par la kommandantur allemande qui va riposter en les
exterminant.

Ce génocide sera vu par les historiens comme la répétition générale du
grand génocide que fut la Shoah.
Or, selon Fassin, il y a des « préoccupantes similitudes » entre ce qui
s’est joué dans le Sud-Ouest africain et ce qui se joue aujourd’hui à
Gaza.
De la même façon que les Allemands colonisateurs en Afrique de l’Ouest
commettaient le premier génocide du XXe siècle, les Israéliens seraient
ainsi les auteurs du premier génocide du XXIe siècle.


Parlons donc de cette « préoccupante similitude »
====================================================

Parlons donc de cette « préoccupante similitude »

Les sciences sociales – sociologie et anthropologie notamment – se sont
constituées précisément autour du problème épistémologique de la similitude.

Gabriel Tarde (1843-1904) observait que la science ne pouvait surmonter
le problème de la complexité et du chaos qu’en observant
**des similitudes au milieu [des] différences**.
Comme le souligne Bernard Lahire, Durkheim aussi s’était opposé aux
historiens qui n’arrivaient pas à surmonter la multiplicité d’évènements
en apparence uniques.

Une vraie science sociale se devait de dégager les éléments récurrents,
c’est-à-dire les similarités entre évènements en apparence différents.

**Or Fassin n’est pas face à une grande variété de phénomènes et de cas**
--------------------------------------------------------------------------

**Or Fassin n’est pas face à une grande variété de phénomènes et de cas**.

Il choisit délibérément de construire un parallèle entre deux cas uniques.

Comme le dit très bien l’anthropologue Philippe Descola, il faut faire
une distinction entre la comparaison, simple figure de rhétorique, et le
comparatisme, démarche épistémologique moderne qui ne peut être maniée
qu’avec précaution.

**La démarche de Fassin ne tient pas du comparatisme mais bien plutôt de
la rhétorique**.

Examinons donc les deux similitudes qu’il se propose d’identifier dans
l’histoire : les Allemands colonisateurs du début du XXe siècle sont
similaires aux Israéliens du XXIe siècle ; et l’attaque du Hamas est
similaire à l’attaque des Héréros.

Une comparaison qui ne tient pas

Après la défaite française et l’unification de l’Allemagne en
1871, les Allemands commencent à construire une flotte qui va leur
permettre d’établir un empire colonial à des milliers de kilomètres
de l’Allemagne unifiée, qui voulait rivaliser avec les deux autres
grands empires coloniaux (France et Grande-Bretagne).

La colonisation de la Namibie procédait donc d’un vaste projet d’expansion
territoriale et économique. Par exemple, la Deutsche Kolonialgesellschaft für
Südwestafrika (Société coloniale allemande pour l’Afrique du Sud-Ouest)
fut fondée avec le soutien des banques, d’industriels et de politiciens,
avec pour but d’exploiter gisements miniers et diamants.

Il serait difficile de trouver même une vague ressemblance avec les colons juifs qui se sont progressivement installés en Palestine
---------------------------------------------------------------------------------------------------------------------------------------

Il serait difficile de trouver même une vague ressemblance avec les
colons juifs qui se sont progressivement installés en Palestine sous
mandat britannique.

Les différences sont connues, mais il faut les répéter.

**Il y a eu une présence juive ininterrompue en Palestine depuis l’Antiquité
ainsi qu’une affinité historique et mémorielle entre les Juifs et cette
terre où se situait le Temple, qui était le centre de la vie religieuse
juive**.

**Une telle affinité religieuse et culturelle était tout simplement inexistante
dans le cas des Allemands en Namibie**.


Le colonialisme  juif était d’abord un nationalisme : **son but était de constituer une nation,  et non d’étendre la puissance d’une nation préexistante**
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

**La seconde raison – et la plus importante – tient au fait que le colonialisme
juif était d’abord un nationalisme : son but était de constituer une nation,
et non d’étendre la puissance d’une nation préexistante**.

Ce nationalisme était le fait d’immigrés pauvres et démunis, l’équivalent
des réfugiés érythréens et syriens qui hantent la conscience européenne
d’aujourd’hui.

Le sionisme devait être une solution à l’insécurité ontologique que le monde
– musulman et chrétien, scientifique et nationaliste – avait créée pour les
Juifs.

Les Juifs sionistes ont donc été fondamentalement hybrides : à la fois
colonisateurs et grands persécutés de l’histoire, **colonisateurs parce que
persécutés par l’histoire**.

Il est impossible de trouver des parallèles et des similitudes entre le
colonialisme impérial d’une nation puissante et le nationalisme de va-nu-pieds
se battant pour leur survie et **recevant l’approbation légale et morale
de la communauté internationale**.


S’ils ne sont pas rentrés dans le panthéon des victimes post-coloniales, c’est pour trois raisons
======================================================================================================

S’ils ne sont pas rentrés dans le panthéon des victimes post-coloniales,
c’est pour trois raisons:

- parce que la nation juive fut reconnue comme  légitime par les Nations
  unies en 1948 ;
- parce que les Juifs ont gagné plusieurs guerres successives contre
  les Arabes ; et
- parce que ces anciennes victimes juives désormais victorieuses créaient
  de nouvelles victimes, les Palestiniens, chassés de leurs terres et
  qui vivent depuis et à des degrés divers selon les époques sous la
  férule implacable des Israéliens.

Un lecteur rapide et peu rigoureux conclurait facilement des propos de
Fassin que les Israéliens sont des colonisateurs comme les Allemands
l’avaient été.

Tournons-nous maintenant vers la réaction des Héréros et du Hamas
===================================================================

Tournons-nous maintenant vers la réaction des Héréros et du Hamas qui
semble être le cœur de la comparaison de Fassin.

Les Allemands avaient passé un accord avec le chef héréro Samuel Maharero
(1856-1923).

Or malgré cet accord, les Allemands commettent des viols, tuent des Héréros
et exhument même des crânes pour les revendre.

Le comportement barbare des Allemands et leur désistement du contrat
qu’ils passent avec la tribu ne peuvent qu’inviter à des représailles
en 1904.

Comparons donc au massacre exécuté par le Hamas : la bande de Gaza a été
évacuée en 2005 par les Israéliens, et Israël remit Gaza aux Palestiniens
clefs en main.

Lorsque le Hamas tue des membres du Fatah et les chasse de Gaza en 2007,
Israël commence un blocus pour empêcher le Hamas de s’armer (et **de façon
incohérente, a aussi tout fait pour renforcer le Hamas** aux dépens de
l’Autorité palestinienne).

Immédiatement après, le Hamas envoie des roquettes sur Israël, enclenchant
un cycle d’escalades de la violence, au cours desquelles **Israël surenchérira
toujours sur les attaques du Hamas**.

Malgré cet état de guerre larvée, le Hamas a reçu par l’intermédiaire
d’Israël plusieurs milliards de dollars d’aide internationale, dont une
grande partie a été investie **dans un large arsenal militaire et la
construction de 500 kilomètres de souterrains censés assurer la protection
des combattants et non des civils**.

Il s’agit donc d’un conflit armé entre deux entités politiques et militaires,
même si elles sont de taille asymétrique.

L’attaque du Hamas portait le fer sur un territoire souverain.

Gaza elle-même est un territoire semi-souverain dans lequel aucun Israélien
ne peut pénétrer.
**On peine à trouver même une similitude avec les Héréros**.

Le Hamas est un groupe politique fondamentaliste
=====================================================

**Le Hamas est un groupe politique fondamentaliste.**

Sa charte écrite en 1988 stipule dans l’article 7 que le Hamas n’est
qu’un maillon de la longue chaîne du combat contre les envahisseurs
sionistes.

**Citant un hadith du Sahih d’al-Bukhari, la charte déclare que le jour du
Jugement n’arrivera que lorsque les musulmans tueront les Juifs**.

**La charte modifiée de 2017 stipule que le Hamas mènera « le djihad contre
Israël jusqu’à sa destruction »**.

**Les Héréros n’étaient pas millénaristes, ne défendaient pas l’idéologie
d’un livre sacré, ne tuaient pas leur propre population au nom de leur
idéologie, n’avaient a priori aucune intention génocidaire vis-à-vis
des Allemands et n’avaient à leur disposition aucun arsenal militaire**.

**Tout parallèle entre les deux groupes profane la mémoire des Héréros**
========================================================================

**Tout parallèle entre les deux groupes profane la mémoire des Héréros**.
**Crimes de guerre, peut-être, mais pas un génocide.**

Mais une fois établie la similitude entre les deux groupes, il devient
plus facile de conclure que les massacres du Hamas étaient un acte de
résistance contre un colonisateur qui, par analogie, n’appartiendrait
ni à sa propre terre ni à sa nation.

Si les Israéliens n’ont rien à faire en Israël, cela aide à établir le
caractère génocidaire de son opération militaire.

Là aussi, on ne peut qu’être perplexe sur l’usage des mots et des concepts.


L’article 2 de la Convention internationale contre le génocide
=================================================================

L’article 2 de la Convention internationale contre le génocide (The Convention
on the Prevention and Punishment of the Crime of Genocide) de 1948 déclare
de façon claire que l’intention de tuer en partie ou entièrement un peuple
est nécessaire pour établir un génocide.

Le 13 octobre 2023, l’armée israélienne appelle les civils à évacuer pour
aller au sud du Wadi Gaza.

900 000 Gazaouis sont évacués malgré les tentatives du Hamas de les empêcher
de bouger afin qu’ils leur servent de bouclier humain.

Israël crée des couloirs humanitaires.

Avec la coordination des États-Unis, les Nations unies et l’Égypte, l’aide
humanitaire arrive à Gaza au travers du checkpoint de Rafa, **certes tard,
certes de façon insuffisante, mais suffisamment pour suggérer que le mot
de génocide ne convient pas à la situation**.

L’armée israélienne a distribué 1,5 million de brochures en arabe pour
avertir les habitants de partir vers le sud.

Elle a aussi fait une vaste campagne en arabe sur les médias sociaux et
a passé des milliers d’appels téléphoniques pour informer les habitants
du camp de réfugiés de Jabaliya d’évacuer.

Fassin s’offusque du nombre de bombes jetées sur Gaza (**avec raison puisque
ces bombes ont fait un nombre très grand de morts palestiniens**) mais il
omet de mentionner que depuis le 7 octobre, plus de 9 000 roquettes ont
été lancées sur Israël par le Hamas (sans doute plus au moment où cet
article sera publié), **faisant 200 000 réfugiés israéliens à l’intérieur
de leur propre pays**.


**Soyons clair : ce qui se passe à Gaza est une catastrophe humanitaire sans précédent dans l’histoire du conflit**
=====================================================================================================================

Soyons clair : ce qui se passe à Gaza est une catastrophe humanitaire
sans précédent dans l’histoire du conflit.
**Fassin a raison de le rappeler**.

**Le spectacle des Gazaouis face à leurs maisons détruites, des milliers
de blessés et de morts est insoutenable**.

Ces images vont hanter les Palestiniens et les Israéliens pendant longtemps.


**Mais ce désastre humanitaire est un effet catastrophique de la guerre et non un génocide. La différence est cruciale**
==============================================================================================================================

**Mais ce désastre humanitaire est un effet catastrophique de la guerre et
non un génocide**.

La différence est cruciale.

Une riposte militaire, même féroce, contre un ennemi qui a enfreint les
frontières et le droit international, et qui met en œuvre beaucoup de
moyens pour éviter des pertes civiles, n’est pas un génocide.

Il est possible que les actions militaires israéliennes constituent des
crimes de guerre.
Nous y verrons plus clair à la fin de la guerre.
**Mais même des crimes de guerre ne constituent pas un génocide**.

Est-il besoin de rappeler que Bachar el-Assad a gazé sa propre population
et provoqué la mort de 300 000 personnes ; ou bien l’ethnicide des
Ouïghours par les Chinois ; ou bien encore les massacres génocidaires
contre les Rohingyas (déclarés par l’ONU l’un des peuples les plus
persécutés au monde) par le Myanmar.

Chacun de ces trois évènements est un bien meilleur candidat au titre
de premier génocide du XXIe siècle – dans la chronologie et dans l’intention.

**Créer des similitudes là où il n’y en a pas peut aussi être préoccupant**.

**Dans la période tourmentée que nous vivons, choisir les mots justes est
un devoir moral et intellectuel**.
