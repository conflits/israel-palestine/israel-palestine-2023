

L'offensive israélienne sur Gaza ne ressemble en rien au génocide des Herero
=================================================================================

Documents sauvegardés Vendredi 24 novembre 2023 à 13 h 33 1 document Par
PARIS10T_1 Ce document est réservé à l'usage exclusif de l’utilisateur
désigné par UNIVERSITE- PARIS-OUEST-NANTERRE-LA-DEFENSE et ne peut faire
l'objet d'aucune autre utilisation ou diffusion auprès de tiers. • Tous
droits réservés • Service fourni par CEDROM-SNi Inc.  Sommaire Documents
sauvegardés • 1 document L'Express (site web) 22 novembre 2023 Joël Kotek :
"L'offensive israélienne sur Gaza ne ressemble en rien au génocide des Herero"
... AOC. L'Express publie aujourd'hui le point de vue de l'historien belge Joël
Kotek, professeur à l'Université libre de Bruxelles et à l'IEP de Paris,
spécialiste des génocides ...  3 Vendredi 24 novembre 2023 à 13 h 33Documents
sauvegardés par PARIS10T_1 2Ce document est réservé à l'usage exclusif de
l’utilisateur désigné par UNIVERSITE-PARIS-OUEST-NANTERRE-LA- DEFENSE et ne
peut faire l'objet d'aucune autre utilisation ou diffusion auprès de tiers. •
Tous droits réservés • Service fourni par CEDROM-SNi Inc.  Joël Kotek :
"L'offensive israélienne sur Gaza ne ressemble en rien au génocide des Herero"
L'historien belge répond à l'anthropologue Didier Fassin, qui a établi un
parallèle entre Gaza et le massacre des Herero perpétré par les colonisateurs
allemands au tout début du XX? siècle...  C'est un parallèle historique qui
fait polémique. L'an- thropologue Didier Fassin, professeur au Collège de
France, a, dansune tribune publiée par le journal en ligne AOCet intitulée
"Le spectre d'un génocide à Gaza", décelé de "préoccupantes similarités"
entre la riposte israélienne à Gaza et le géno- cide des Herero perpétré
par les colonisateurs allemands à partir de 1904 dans le Sud-Ouest africain
(l'actuelle Namibie). Des historiens, philosophes et sociologues (dont Bruno
Karsenti, Danny Trom ou Luc Boltans- ki)ont réponduà Didier Fassin sur AOC.
La sociologue Eva Illouz aelle aussi cri- tiqué cette comparaisondans la revueK.
et Philosophie magazine. Didier Fassins'est défendu sur AOC. L'Express publie
aujourd'hui le point de vue de l'historien belge Joël Kotek, professeur
à l'Université libre de Bruxelles et à l'IEP de Paris, spécialiste des
géno- cides, notamment de celui des Herero.  Si seulement les spécialistes
de sciences sociales, lorsqu'ils interviennent dans le débat public sur des
sujets d'actualité, ne s'ingéniaient qu'à intervenir dans leur champ de
compétences, il y aurait as- surément moins de polémiques. J'en veux pour
preuve la triste comparaison de Didier Fassin entre le génocide des Herero
et la guerre menée par Israël contre le (seul) Hamas, développée dans
une tribune intitulée "Le spectre d'un génocide à Gaza".  Le fait est que
je suis, sans doute, le seul historien francophone à avoir tra- vaillé sur
le génocide des Herero, étant spécialisé depuis près de trente ans dans
l'étude des génocides. C'est bien ce savoir tout particulier qui m'oblige à
recadrer Didier Fassin, dont je ne nie nullement l'intérêt qu'il porte au
premier génocide du XX? siècle. Qu'on la souti- enne ou qu'on la condamne,
l'offensive militaire israélienne sur Gaza ne ressem- ble en rien au génocide
des Herero, et ce à plus d'un titre. Pourquoi?  Certes, le premier génocide
du XX? siè- cle débute par un massacre. Le 12 jan- vier 1904, les Herero,
alors majoritaires dans la colonie du Deutsch-Südwestafri- ka (Sud-Ouest
africain allemand), la fu- ture Namibie, se rebellent. Quelque 120 soldats et
colons allemands sont mas- sacrés, à 97 % de sexe masculin. Fait notable,
en effet, le chef Herero Samuel Maharero avait imposé de ne pas touch- er aux
femmes, aux enfants, ainsi qu'aux Britanniques et aux prêtres. Mais là n'est
pas l'essentiel, sauf bien entendu pour les prêtres, les Britanniques, les
femmes et les enfants épargnés. L'essentiel est que le génocide qui suivit,
au cours duquel périrent 80 % des membres de l'ethnie Herero, ne surgit pas
subreptice- ment à la suite d'une quelconque séquence événementielle. Il
fut pro- grammatique.  Intention et décision Ce que semble ignorer Didier
Fassin, c'est la nature même du concept de génocide. Le génocide n'est
pas une vi- olence de masse comme les autres. Un génocide, ce n'est pas
"beaucoup de morts", à l'instar de la Syrie (500 000 morts), du Yémen (200
000), de la guerre civile algérienne (200 000) et de Gaza (11 000). C'est
un acte criminel pensé, volontaire, prémédité, qui vise à assassiner
dans sa totalité une popula- tion cible, à l'exemple des Herero, mais
aussi des Arméniens, des juifs, des Tutsi ou des yézidis. Une parenthèse
m'oblige à constater que Didier Fassin semble ig- © 2023 L'Express. Tous
droits réservés.  Le présent document est protégé par les lois et
conventions internationales sur le droit d'auteur et son utilisation est
régie par ces lois et conventions.  Certificat émis le 24 novembre 2023
à UNIVERSITE-PARIS-OUEST-NANTERRE-LA- DEFENSE à des fins de visualisation
personnelle et temporaire.  news·20231122·EWL·ojpbqqnatndyzejlslr2qe2hny
Nom de la source L'Express (site web) Type de source Presse • Presse Web
Périodicité En continu Couverture géographique Nationale Provenance Paris,
Ile-de-France, France Mercredi 22 novembre 2023 L'Express (site web) • 1721
mots Vendredi 24 novembre 2023 à 13 h 33Documents sauvegardés par PARIS10T_1
Documents sauvegardés 3Ce document est réservé à l'usage exclusif de
l’utilisateur désigné par UNIVERSITE-PARIS-OUEST-NANTERRE-LA- DEFENSE
et ne peut faire l'objet d'aucune autre utilisation ou diffusion auprès
de tiers. • Tous droits réservés • Service fourni par CEDROM-SNi Inc.
norer dans sa démonstration que le XXI? siècle a déjà connu son premier
génocide : celui des yézidis. Il est vrai qu'il s'agit d'un génocide commis
par des "racisés". Cela ne le rend pas moins significatif. Le génocide
est pensé; d'où l'importance de la notion d'"intention", explicitée dans
la Convention pour la prévention et la répression du crime de génocide,
adoptée par l'ONU en décem- bre 1948. L'intention est de faire dis- paraître
tout le groupe cible, physique- ment, sans échappatoire ni fuite.  Evidemment,
l'intention radicale de faire disparaître un peuple de trop sur terre ne suffit
pas, celle-ci doit néces- sairement être sanctionnée par une dé- cision que
l'on peut toujours dater avec une certaine précision. Dans tout géno- cide,
il y a toujours un moment zéro, c'est-à-dire une décision qui conduit non pas
à causer la mort de 1 % de la pop- ulation cible, mais d'en éliminer rapide-
ment la part essentielle (80 %), c'est- à-dire tous les hommes et surtout les
femmes et les enfants. Le génocide des Tutsi, ce sont 10 000 morts par jour
du- rant cent jours. La Shoah, ce sont 5 500 morts par jour pendant quatre ans
et de- mi. On connaît les conséquences du génocide arménien. Il y avait
plus de 2 millions d'Arméniens en 1914 au sein de l'Empire ottoman, il en
reste tout au plus 60 000 aujourd'hui en Turquie. Dans le cas de la Shoah,
la décision date de juil- let 1941 pour les juifs soviétiques et d'octobre
1941 pour l'ensemble de la ju- daïcité européenne. On en connaît aussi
les terribles effets. Un seul exemple : il y avait 3,3 millions de juifs en
Pologne en 1939, seuls 300 000 survécurent, la plupart après avoir rejoint
l'URSS, où ils subirent bien d'autres tourments. Il reste aujourd'hui en
Pologne tout au plus 12 000 juifs.  Et dans le cas des Herero? Contraire-
ment à ce que pense Didier Fassin, l'ex- termination des Herero n'est pas
due à un engrenage fatal, mais à la décision mûrement réfléchie, mieux
encore, proclamée, du général en chef du corps expéditionnaire allemand,
Lothar von Trotha, d'en terminer une fois pour toutes avec le peuple Herero. La
guerre coloniale prit, dès les premiers jours, une forme génocidaire, où
l'intention proclamée était non pas de soumettre l'ennemi, mais de l'éradiquer
purement et simplement. C'est dans cette logique génocidaire que von Trotha,
fort de l'ap- pui du gouvernement allemand, décida, lors de la bataille de
Waterberg, le 11 août 1904, d'exterminer non seulement les quelque 5 500
combattants qui étaient venus à sa rencontre, mais aussi la majorité des
civils, hommes, femmes et enfants, qui, par milliers, les accom- pagnaient. Le
2 octobre 1904, un ordre d'extermination (Vernichtungsbefehl)en bonne et due
forme viendra compléter cette séquence génocidaire. Ce texte, rédigé en
"petit nègre" est des plus clairs quant aux desseins génocidaires alle-
mands : "Moi, le général des troupes allemandes, adresse cette lettre au pe-
uple Herero. Les Herero ne sont plus dorénavant des sujets allemands. [...]
Tout Herero découvert dans les limites du territoire allemand, armé comme
désarmé, avec ou sans bétail, sera abat- tu. Je n'accepte aucune femme ou
en- fant. Ils doivent partir ou mourir. Telle est ma décision pour le peuple
Herero."  Israël n'est pas né du colonialisme Où trouver l'équivalent
israélien de l'or- dre d'extermination allemand? Com- ment nier les efforts,
peut-être purement tactiques, voire cyniques, du comman- dement militaire
israélien d'épargner au maximum les femmes, les enfants et les malades
palestiniens (envois de SMS, mises en place de couloirs humanitaires, etc.) Et
ce, à l'instar des Herero, mais pas des soldats du corps expéditionnaire
allemand ni, faut-il le marteler, des ter- roristes du Hamas.  Car il ne fait
aucun doute que les mas- sacres du 7 octobre constituent, pour l'historien des
violences extrêmes que je suis, une séquence génocidaire; les terroristes
du Hamas exterminant jusqu'aux femmes enceintes. Question à se poser : ces
massacres ne trouvent-ils pas leur source dans la charte originelle du Hamas,
qui appelle à la destruction d'Israël, selon la belle expression désor- mais
consacrée "de la rivière à la mer"?  Car tout génocide s'inscrit dans un ter-
reau idéologique qui ne laisse aucune place au doute ou à la pitié. On se
sou- viendra des ouvrages du pasteur protes- tant Paul Rohrbach, qui théorisa,
dans les années 1900, l'extermination des populations africaines hostiles à
la colonisation. Ses écrits serviront de jus- tification à l'anéantissement
des Herero et des Nama. En 1912, huit ans après le génocide, il trouvera
encore les mots pour le justifier dans son best-seller La Pensée allemande
dans le monde: "Qu'il s'agisse de peuples ou d'individus, des êtres qui ne
produisent rien d'important ne peuvent émettre aucune revendica- tion au droit
à l'existence. Nulle philan- thropie ou théorie raciale ne peut con- vaincre
des gens raisonnables que la préservation d'une tribu de Cafres de l'Afrique
du Sud [...] est plus importante pour l'avenir de l'humanité que l'expan- sion
des grandes nations européennes et de la race blanche en général. [...] C'est
seulement quand l'indigène a appris à produire quelque chose de valeur au ser-
vice de la race supérieure, c'est-à-dire au service du progrès de celle-ci
et du sien propre, qu'il obtient un droit moral à ex- Vendredi 24 novembre
2023 à 13 h 33Documents sauvegardés par PARIS10T_1 Documents sauvegardés
4Ce document est réservé à l'usage exclusif de l’utilisateur désigné par
UNIVERSITE-PARIS-OUEST-NANTERRE-LA- DEFENSE et ne peut faire l'objet d'aucune
autre utilisation ou diffusion auprès de tiers. • Tous droits réservés
• Service fourni par CEDROM-SNi Inc.  ister." Du génocide comme instrument
colonial et impérialiste!  Suggérer, enfin, un lien entre le seul Etat juif
de la planète et une entreprise colo- nialiste est tout aussi absurde. Israël
n'est pas né du colonialisme, mais, à l'in- star de la Pologne, de l'Arabie
saoudite de la Syrie ou du Liban, des ruines des empires centraux. Les juifs
sont des nat- ifs, des indigènes. Il suffit d'ouvrir le Nouveau Testament
pour constater que des juifs vécurent, à l'instar de Jésus et des apôtres,
en Galilée et en Judée, des territoires qui ne s'appelaient pas encore la
Palestine. Il suffit encore d'ouvrir le Coran pour découvrir qu'il y avait
des juifs au VII? siècle dans le Hedjaz.  Comment comprendre autrement le sur-
gissement de l'islam, cette foi si proche du judaïsme? Surtout, comment oublier
que la moitié des Israéliens sont orig- inaires du bassin méditerranéen,
du Maroc, de Libye, d'Egypte, de Syrie, de... Palestine. Avant 1948, les juifs
étaient aussi des Palestiniens. Conclu- ons avec Jacques Prévert : "Il ne faut
pas laisser les intellectuels jouer avec les allumettes." La même formule s'ap-
plique aux savants spécialisés, qui, mus par autre chose que le désir de com-
prendre, s'aventurent hors de leur champ et s'emparent de ce qu'ils peuvent
pour le but qu'ils se sont donné. Suggérer la possibilité d'un génocide à
Gaza, c'est en effet jouer avec le feu dans le contexte d'une rue arabe qui
n'attend qu'une étin- celle pour exploser. Contre les juifs.  * L'historien
Joël Kotek, professeur à l'Université libre de Bruxelles et à l'IEP de
Paris, a notamment publié "Le géno- cide des Herero, symptôme d'un Son-
derweg allemand?" (Revue d'histoire de la Shoah, vol. 189, n° 2, 2008, pp.
177-197); "Afrique : le génocide oublié des Herero" (L'Histoire, n° 261,
janvier 2002); "Colonialisme et racisme comme matrice de la Shoah. Le cas du
génocide des Herero" (inDu génocide des Ar- méniens à la Shoah. Typologie
des mas- sacres du XX? siècle, sous la direction de Gérard Dédéyan et Carol
Iancu, Pri- vat, Histoire, 2015, 640 p., pp.  431-439).  Note(s) : Mise à jour
: 2023-11-22 16:00 Vendredi 24 novembre 2023 à 13 h 33Documents sauvegardés
par PARIS10T_1 Documents sauvegardés 5Ce document est réservé à l'usage
exclusif de l’utilisateur désigné par UNIVERSITE-PARIS-OUEST-NANTERRE-LA-
DEFENSE et ne peut faire l'objet d'aucune autre utilisation ou diffusion auprès
de tiers. • Tous droits réservés • Service fourni par CEDROM-SNi Inc.
