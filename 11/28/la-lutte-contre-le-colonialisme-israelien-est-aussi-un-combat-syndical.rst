
.. _angeli_2023_11_28:

============================================================================================================
2023-11-28 **La lutte contre le colonialisme israélien est aussi un combat syndical** par Verveine Angeli
============================================================================================================


- https://www.contretemps.eu/syndicalisme-palestine-anticolonialisme-israel-guerre-gaza/


On oublie parfois que l’une des dimensions de l’occupation et de la
colonisation israélienne, c’est la surexploitation des travailleurs·ses
palestinien·nes et leur extrême marginalisation économique (marquée notamment
par un sous-emploi massif), en particulier à Gaza soumis à un blocus inhumain
depuis plus de quinze ans. Le combat anticolonialiste a donc nécessairement
une dimension syndicale, à la fois en Palestine-Israël mais aussi dans le
mouvement international de solidarité avec la lutte des Palestinien·nes.

Cet article de Verveine Angeli est un point de vue qui ne saurait être
exhaustif sur la question syndicale et la Palestine. Il est le produit des
réflexions et des actions de militant·es de l’Union syndicale Solidaires
actif·ves dans le groupe Palestine du syndicat.

----

Il est encore plus difficile dans ces temps de guerre de faire un point
précis sur la situation des syndicats palestiniens et leur place dans le
contexte. Néanmoins il est remarquable que ceux-ci aient produit un appel
international commun « Stop arming Israël, End all complicity » appelant
à des prises de position et à l’action pour arrêter d’armer Israël, ce
qui n’est pas arrivé depuis longtemps. Cette déclaration est significative
à deux titre : elle regroupe des syndicats de Cisjordanie et de la bande
de Gaza ; et d’autre part elle regroupe des syndicats officiels et des
syndicats indépendants.

Elle est le signe d’une volonté commune d’agir dans une situation dramatique
pour le peuple palestinien que ce soit à Gaza sous les bombardements et
en Cisjordanie avec les violences des colons et des forces de répression
israéliennes qui tuent et emprisonnent, violences qui redoublent depuis le
7 octobre. Elle est un appel aux syndicats du monde… Un syndicalisme ancré
dans la réalité de la colonisation

Travailler en Palestine, être syndicaliste en Palestine, c’est être en
permanence confronté à une double contrainte, celle de la lutte quotidienne
pour un salaire, pour un emploi, c’est aussi le faire dans des conditions
très spécifiques, celles de l’occupation et de la colonisation.

Les conditions inhumaines relayées par la presse dans lesquelles les
travailleurs de Gaza employés en Israël ont été renvoyés à Gaza
bombardée ou expulsés vers la Cisjordanie sont l’expression de la violence
de l’Etat d’Israël dans le contexte actuel. Mais ces événements ne font
que refléter ce que ceux et celles, Palestinien·nes qui travaillent avec (ou
sans permis) dans les territoires de 48 vivent de façon quotidienne : passages
de check-points en pleine nuit, massé·es dans des couloirs grillagés comme
des cages, menaces permanentes de suppression des permis de travail si on a des
traces de produit chimique (un engrais par exemple) ou un membre de la famille
ou du village qui a été arrêté, fermeture des check-points au moindre
incident… ce qui veut dire l’absence de travail et l’absence de ressource.

Pour ceux et celles qui travaillent de façon illégale dans les colonies de
Cisjordanie c’est une précarité encore plus grande sans salaire minimum,
sans convention, sans garanties pour des travaux dangereux comme sont ceux du
bâtiment. En Cisjordanie, ce sont des taux de chômage élevés, des emplois
très précaires tant l’économie est bridée par la situation coloniale :
développement des télécommunication entravé (ce qu’avait dénoncé le
rapport demandant le désinvestissement d’Orange), interdictions d’installer
des panneaux solaires, services postaux non reconnus internationalement,
courrier et colis bloqués parfois pendant des mois voire des années…

En Cisjordanie le taux de chômage est était de 18% en 2018 et de 52% à
Gaza et globalement de 44% chez les jeunes, l’emploi de fonctionnaires est
soumis aux subsides que l’Autorité palestinienne reçoit et transmet ou non
à Gaza. Et dans la situation actuelle, les salaires ne sont pas versés, les
ressources étant bloquées par Israël. La lutte pour le droit à un salaire, à
un emploi, pour l’égalité des droits prend un sens évidemment particulier.

Il faut citer la situation des travailleur·euses palestinien·nes d’Israël
soumis·es aux discriminations, aux interdits professionnels qui s’ajoutent
à des conditions d’existence contrôlées, de logement limitées parce que
toute parcelle de territoire supplémentaire est impossible à obtenir pour
les Palestinien·nes d’Israël1.  Un syndicalisme marqué par le virage
néolibéral et répressif lié aux accords d’Oslo

Les accords d’Oslo sont connus pour avoir porté la perspective de
la construction de deux Etats, perspective qui s’éloigne entre autres
à cause de l’installation de colons sans cesse plus nombreux·euses en
Cisjordanie. La mise en place de toute une série de mesures économiques et
financières néo-libérales a accompagné ces accords. Elles pèsent sur le
monde du travail d’autant plus qu’elles s’appliquent dans le contexte
colonial : c’est le cas des prêts immobiliers alors que l’espace est
grignoté par les colonies illégales et de la mise en place de réformes
inspirées par le Fond monétaire international.

Une des grandes mobilisations syndicales des années 2018-2020 a été
la lutte contre la mise en place d’une sécurité sociale sur un mode
néo-libéral dans lesquels les travailleur·euses ne pouvaient avoir aucune
confiance : Un des enjeux était la récupération des cotisations sociales
des travailleur·euses employé·es en Israël, projet perçu comme l’objet
d’un véritable chantage. Bref, un espace sans Etat et sans démocratie,
soumis aux diktats d’Israël et au bon vouloir des pays occidentaux et des
organismes internationaux qui versent l’argent sous condition.

Oslo, c’est aussi la mise en place des permis pour travailler dans les
territoires de 48 quand les travailleur·euses viennent de Cisjordanie et
plus récemment de Gaza (ce qui a été présenté comme une des ouvertures de
Netanyahou), alors qu’ils n’étaient pas nécessaires avant Oslo. Ces permis
constituent un chantage permanent et ont tous été supprimés s’agissant
de Gaza aujourd’hui et aucun·e travailleur·euse de Cisjordanie ne peut
venir travailler. L’appel d’Israël à l’émigration en provenance des
pays asiatiques vise à remplacer la main d’œuvre palestinienne toujours
suspectée.

C’est aussi une situation où les fonctionnaires payés par l’Autorité
palestinienne (avec l’argent donné par l’occident…) se serrent la
ceinture, y compris après des grèves ayant conduit à des accords (cela a
été le cas amenant les enseignants à une grève générale en 2016). Et
où la répression des mouvements syndicaux est sévère conduisant à des
emprisonnements, des licenciements… Un mouvement syndical émietté et corseté

Autre conséquence d’Oslo, les cotisations syndicales que paient les
travailleur·euses palestinien·nes en Israël (elles sont obligatoires)
sont normalement reversées au syndicat palestinien officiel, la PGFTU. La
Histadrout, est le syndicat israélien qui reçoit de façon automatique les
cotisations. Elle a été créée en 1920 comme Fédération des Travailleurs
hébreux en Terre d’Israël et a été un élément essentiel de la
colonisation.

Ce reversement est vécu par de très nombreux·ses travailleur·euses et
les syndicats indépendants comme le signe d’une collaboration de fait
avec l’Etat d’Israël et l’occupation. C’est un moyen, quand ces
cotisations sont effectivement versées, ce qui n’est pas toujours le cas,
de financement du syndicalisme officiel. L’Autorité palestinienne défend
cette pratique et en fait un moyen de pression sur l’ensemble du mouvement
syndical, en réprimant et supprimant tout moyen d’existence aux syndicats
indépendants. Cela ne l’empêche pas de tenter de contrôler, notamment
en nommant les dirigeants syndicaux, certains secteurs de la PGFTU qui
deviennent trop remuant (comme cela a été le cas pendant la grande grève
des enseignant·es de 2016).

Dans les faits les syndicats indépendants sont organisés en une multitude de
secteurs professionnels. Ce fonctionnement éclaté est lié tant à la volonté
de contrôle des travailleur·euses de l’action sur leur champ professionnel,
qu’aux difficultés d’un fonctionnement démocratique d’une organisation
syndicale dans un contexte répressif et de faibles moyens, sans oublier les
positionnements et liens avec des courants politiques qui peuvent exister2.Un
syndicalisme interprofessionnel a de fait des difficultés à exister hormis dans
sa forme officielle. Il faut noter aussi l’existence d’organisations de base
de chômeurs, de femmes, liées à la santé qui ont une action sur les enjeux
du travail sans être formellement des syndicats, ou la constitution lors de
mobilisations comme celle des enseignant·es de structures d’auto-organisation
en lieu et place de la fédération de la PGFTU sous contrôle de l’AP.

On peut dire que les syndicats indépendants en Cisjordanie participent
d’un mouvement social multiforme qui prend sa place dans des mobilisations
récurrentes contre l’occupation et la colonisation et de contestation de
la politique de l’Autorité palestinienne. Mais il n’est pas en situation
d’être de premier plan y compris lors de l’évènement décisif qu’a
été la grève générale de 2018 qui concernait la totalité des territoires
et populations palestiniennes. A Gaza il semble que le Hamas ait tenté d’avoir
lui aussi la mainmise sur le mouvement syndical à travers la PGFTU locale.

Dans les territoire de 48, le petit syndicat des travailleurs arabes «
Arab workers union » installé dans la ville palestinienne de Nazareth est
actif à défendre les travailleur·euses palestinien·nes et à relayer des
informations. Nombreux sont les syndicats qui ont appelé à la solidarité et
dénoncé les conditions de la guerre actuelle contre le peuple palestinien3.
Les syndicats en Occident et le soutien à la Palestine

Les organisations syndicales internationales Confédération syndicale
internationale (CSI), la Confédération européenne des syndicats (CES), les
branches syndicales internationales pratiquent un équilibrisme qui exprime
l’absence de volonté de prendre position sur la situation en Palestine en
assumant des relations avec la PGFTU palestinienne et la Histadrout. Une des
demandes traditionnelle des syndicats palestiniens indépendants est la rupture
des liens avec la Histadrout. Cette exigence a porté ses fruits dans certaines
occasions par exemple lors du congrès de l’European public services union
(EPSU) en Irlande en 2019 où la décision de rompre les liens a été prise.

Un réseau syndical européen de solidarité avec la Palestine (ETUN) porte
entre autres ces batailles. Ce réseau est constitué pour l’essentiel de
syndicats norvégiens, irlandais, anglais, belges et de l’Etat espagnol, ainsi
que de Solidaires, tous très engagés dans la solidarité avec la Palestine
par l’organisation de campagnes, de délégations, de soutien direct aux
syndicats sur place. Des syndicats ont ainsi décidé de répondre à l’appel
intersyndical à l’action venu de Palestine contre le commerce des armes4.

Il faut citer en France l’initiative à laquelle ont participé pour
plusieurs campagnes la CGT et Solidaires, rejointes par la CFDT, au côté
d’associations de solidarité (en particulier l’AFPS), de l’organisation
palestinienne Al Haq (devenue organisation terroriste selon Israël) et d’ONG
(notamment le CCFD, la FIDH, la LDH…) pour exiger le désinvestissement de
certains projets dans lesquels sont présentes des entreprises françaises
en complicité avec la colonisation : cela a été le cas avec une victoire
pour Orange, une victoire partielle pour le tramway de Jérusalem dans lequel
étaient présentes deux filiales de la SNCF et de la RATP, une campagne sur
les banques qui ont des participations dans les banques israéliennes et/ou
projets d’investissement dans les colonies.

Il faut citer encore la campagne Boycott, désinvestissement, sanctions (BDS)
à laquelle de nombreux syndicats palestiniens nous invitent à participer
étant eux-mêmes partie prenante de la Boycott national campaign en Palestine
(notamment contre AXA, Puma, HP, Carrefour…). Il faut souligner pour toutes
ces actions le rôle décisif de ceux et celles qui documentent la complicité
des entreprises avec l’occupation et la colonisation. C’est le cas de Who
profits, centre de recherche basé en Israël.

Evidemment, les liens directs sont entre syndicats sont décisifs, et encore
mieux entre syndicats des mêmes secteurs professionnels parce qu’ils
concrétisent la solidarité. Cette nécessité étant renforcée par
l’éclatement des organisations en Palestine.

L’ensemble de ce travail effectué depuis des années a permis que
s’expriment des prises de positions syndicales dans la guerre actuelle
notamment sur l’arrêt des livraisons d’armes à Israël et que le
positionnement de Solidaires dans ce contexte de forte pression sur les forces
militantes puisse s’appuyer sur une activité et des engagements existants. Il
nous faut néanmoins constater qu’au regard de l’aggravation de la situation
sur place pour la population palestinienne, quel que soit l’endroit où
elle se trouve, il serait nécessaire de renforcer encore l’action syndicale
dans notre pays aujourd’hui pour un cessez-le-feu immédiat et demain pour
l’arrêt de l’occupation et de la colonisation.  Lire hors-ligne :

références  
=================

Voir le film Contrefeux
qui présente cette situation lors d’une délégation
syndicale en 2019 https://vimeo.com/345343417 

Voir
la revue internationale Palestine de l’Union syndicale Solidaires :
https://solidaires.org/sinformer-et-agir/brochures/international/revue-internationale-n14-palestine-fragments-luttes-et-analyses/

3	Syndicat des professeur.e.s
et des employé.e.s de l’Université de Birzeit :
https://agencemediapalestine.fr/blog/2023/10/13/nous-sommes-tous-tes-des-palestinien-ne-s-le-communique-du-syndicat-des-professeur-e-s-et-des-employe-e-s-de-luniversite-de-birzeit/
et
https://agencemediapalestine.fr/blog/2023/11/10/nous-sommes-toutes-et-tous-le-sud/.

Syndicat des journalistes palestiniens, 16 octobre 2023 :
https://agencemediapalestine.fr/blog/2023/10/16/syndicat-des-journalistes-palestiniens-a-gaza-des-crimes-contre-les-journalistes/
et
https://agencemediapalestine.fr/blog/2023/11/03/le-journalisme-nest-pas-un-crime-lettre-du-syndicat-des-journalistes-palestinien-ne-s/

Syndicat des Travailleurs Palestiniens des Services postaux sur
l’occupation sioniste et la guerre contre les Palestiniens :
https://agencemediapalestine.fr/blog/2023/10/15/communique-du-syndicat-des-travailleurs-palestiniens-des-services-postaux-sur-loccupation-sioniste-et-la-guerre-contre-les-palestiniens/

Appel des étudiants palestiniens aux
étudiants du monde entier : Stop au génocide et
fin de la complicité avec l’apartheid israélien, 23 octobre 2023 :
https://www.bdsfrance.org/appel-des-etudiants-palestiniens-aux-etudiants-du-monde-entier-stop-au-genocide-et-fin-de-la-complicite-avec-lapartheid-israelien/
⇧4	En Belgique :
http://www.etun-palestine.org/site/2023/10/31/belgian-transport-unions-refuse-to-load-and-unload-weapons-going-to-israel-and-call-for-an-immediate-ceasefire/

En France :
https://sudindustrie.org/wp-content/uploads/2023/11/Communique-secteur-armement-SUD-Industrie.pdf

En Catalogne : Déclaration dockers Barcelone
