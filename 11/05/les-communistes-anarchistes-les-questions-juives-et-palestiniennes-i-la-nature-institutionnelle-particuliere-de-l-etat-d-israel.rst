
.. _anar_it_israel_2023_11_05:

====================================================================================================================================================
2023-11-05 **Les communistes anarchistes, les questions juives et palestiniennes I. - La nature institutionnelle particulière de l'État d'Israël**
====================================================================================================================================================

- https://www.ucadi.org/2023/11/05/i-comunisti-anarchici-la-questione-ebraica-e-quella-palestinese/

Pour comprendre la dynamique politique interne d'Israël, il est extrêmement
important de prendre en compte les effets de l'immigration sur la croissance
démographique. 

Au 31 mars 2019 (date du dernier recensement), la population
israélienne était composée de 9 millions de personnes; depuis 1948, il
a augmenté de 1 700 %. 

Cela est du non seulement au taux de fécondité
qui se situe dans la moyenne mondiale, mais à une immigration constante et
régulée qui est passée de 28 600 en 2021 à 70 000 en 2022 en raison de
l'afflux d'Ukraine, de guerre et d'autres pays d'Europe de l'est.[11]

Cependant, au cours des vingt années précédentes, la plupart des olim (c'est ainsi
qu'on appelle les nouveaux immigrants) venaient des pays d'Afrique du Nord,
d'Irak, d'Iran, des pays d'Amérique latine; cette grande mutation dans
la composition des classes sociales de la population israélienne a eu une
influence significative sur l'équilibre politique, **augmentant le poids des
partis religieux** qui doivent également leur augmentation du consensus à la
nécessité de s'installer sur le territoire des nouveaux arrivants, désireux
de s'installer sur des terres volées aux Palestiniens. 

Cela signifie que la politique de Netanyahou, qui a débuté de manière organique en 2009,
consistant à faire d'Israël un pays «normal», en modifiant sa structure
institutionnelle et de gestion, en réduisant les autonomies, en verticalisant
les processus de prise de décision, avec pour résultat de faire perdre au pays
cette caractéristique d'expérimentation sociale continue qui l'a distingué
par ses origines atypiques en tant que forme d'État et de gouvernement qui,
comme nous l'avons vu, se forme sur un projet institutionnel à forte empreinte
pan-syndicaliste. 

Cela signifie que le pays ne s'est pas doté, comme ceux
de l'Europe continentale, d'une structure centralisée caractérisée par la
présence de la garantie d'une Cour constitutionnelle, mais a plutôt procédé
à la construction du pays en se dotant progressivement d'une sédimentation
de principes fondamentaux. des lois, résultat de la prédominance absolue
du Parlement, de la Knesset, qui a assumé le rôle anormal d'une assemblée
constituante permanente et en même temps celui d'un Parlement normal qui
n'a donc pas besoin d'établir des règles contenues dans un texte écrit
unitaire, une fois pour toutes, mais renvoie cette tâche à l'avenir, se
contentant de procéder étape par étape, pour élaborer au fil du temps
une «constitution par étapes» qui peut être façonnée pour s'adapter
à une société dynamique, un changement continu. 

Comme le Royaume-Uni, Israël a choisi d'avoir des règles constitutionnelles 
réparties dans de nombreuses «lois fondamentales», traitant de différents aspects. 

De cette manière, Israël veut maintenir ouverte la dialectique entre ses différentes
composantes laïques et religieuses, créant une synthèse fonctionnelle entre
les apports culturels des différentes origines culturelles de sa population,
bien que dans le cadre du choix commun d'appartenance à Israël. 

Il s'ensuit qu'Israël a adopté plus de 10 lois fondamentales, même si cet ensemble
n'est pas contenu dans un seul texte; elle a une «Constitution en cours»
qui dépend de l'évolution des équilibres politiques internes et n'a aucune
valeur prescriptive. 

La Knesset et les partis qui y sont représentés ont la
souveraineté politique du pays mais le pacte social prévoit que les règles
établies dans les lois fondamentales peuvent toujours être remises en question
par la politique parlementaire. 

et les procédures aggravées de modification
des règles fondamentales, même si elles sont prévues, peuvent facilement
être surmontées par la Knesset. 

À cet égard, la démocratie israélienne
s'apparente à celle de l'Angleterre, où la suprématie parlementaire reste
un dogme indiscutable, que le pouvoir judiciaire respecte, tout en exerçant
sa fonction de contrôle. 

Aujourd'hui, certaines lois fondamentales, tant
sur la forme du gouvernement que sur l'élection directe du Premier ministre,
visant à renforcer le gouvernement en limitant le pouvoir des petits partis,
se sont révélées inefficaces. 

Aucune règle ne garantit au premier ministre
élu une certaine majorité à la Knesset qui reste l'arbitre des coalitions
gouvernementales et souverain même à l'égard du premier ministre choisi
par le peuple que l'élection directe aurait du renforcer. 

En analysant ici,
quoique brièvement, le système institutionnel et gouvernemental d'Israël,
nous notons qu'en ce qui concerne la relation entre l'État et les citoyens,
deux lois fondamentales ont introduit la Déclaration des droits: un catalogue
de droits individuels envers l'État. 

La conséquence notable consiste à avoir
rendu possible un système judiciaire de contrôle de la constitutionnalité
des lois. 

Le premier arrêt de la Cour suprême qui l'a rendu effectif en
1996 a donné naissance à une démocratie libérale au sens constitutionnel,
avec pour conséquence que la Knesset a été soumise au contrôle de juges
indépendants, garants des libertés individuelles et des minorités, y
compris non-gouvernementales. Juifs, limitant les arbitres du pouvoir. 

Se croyant éternel, Netanyahu a voulu forcer la main et opérer un retournement
constitutionnel, en proposant une loi fondamentale qui permet à l'exécutif de
contrôler le pouvoir judiciaire, limitant les pouvoirs de la Cour suprême. 

Cela équivaudrait à restaurer la plénitude du principe de suprématie parlementaire
de la Knesset face à des juges non élus, donc dépourvus de légitimité
démocratique. 

Et pourtant, une fois établie la primauté des droits sur le
pouvoir, leur défense relève du processus démocratique et, accessoirement, du
pouvoir judiciaire de la Cour constitutionnelle. 

En Israël, la loi fondamentale
de 2018, sur «l'État national du peuple juif», voulue par Netanyahu pour
«garantir qu'Israël» reste «un État juif pour les générations futures»,
a creusé le fossé entre religieux et laïcs, et a exacerbé la question
de la protection des minorités qui reste ouverte. 

La possibilité future de
transformer, quoique lentement, Israël en un État non confessionnel, ouvert
et pluraliste repose sur cet affrontement, comme le prédisaient certains à
l'origine. 

Une évolution en ce sens pourrait être facilitée par l'absence de
compromis sur les valeurs fondatrices du pays, codifiées dans une Constitution
écrite, qui constituerait un catalogue fermé, face à la présence de lois
de rang constitutionnel, mais qui peut être facilement modifié et intégré
dans le temps, résultat d'une dialectique constante entre les différentes
composantes de la société: il s'agit en réalité de concevoir un espace
institutionnel pour les minorités, dans le cadre d'une société ouverte et
consociative qui fait de la médiation un élément de force et stabilité.

[11]En 1960, il y avait 2 114 020 résidents israéliens et, dix ans plus tard,
ils dépassaient les 3 millions. 

Entre 1970 et 1992, la croissance démographique
a ralenti. Mais depuis 1991, la population a recommencé à augmenter au rythme
d'environ 200 000 individus par an. La plupart des immigrants de 2022 sont des
jeunes: 27 % ont entre 18 et 35 ans, ce qui, selon l'Agence juive, «comprend
des professionnels dans des domaines tels que la médecine, l'ingénierie et
l'éducation». 

24 % des olim ont moins de 18 ans, 22 % ont entre 36 et 50 ans,
14 % ont entre 51 et 64 ans et 13 % ont plus de 65 ans.


Alya, olim, yordim
======================

- https://fr.wikipedia.org/wiki/Alya

Alya, Alyah ou Aliyah hébreu (עֲלִיָּה ou עלייה, pluriel alyoth, signifiant littéralement 
« ascension » ou « élévation spirituelle ») est un terme désignant l'acte 
d'immigration en Terre d'Israël, puis en Israël par un Juif.

Les immigrants juifs sont appelés olim. 

Au contraire, le fait pour un Juif d'émigrer en dehors d'Israël est appelé 
yéridah (יְרִידָה, « descente ») et les émigrants juifs sont appelés yordim. 

