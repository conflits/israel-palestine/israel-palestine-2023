

================================================================================================================================================
Tal Bruttmann, historien : « Le Hamas a conçu, en amont, une politique de terreur visuelle destinée à être diffusée dans le monde entier »
================================================================================================================================================

- https://www.lemonde.fr/idees/article/2023/11/29/tal-bruttmann-historien-le-hamas-a-concu-en-amont-une-politique-de-terreur-visuelle-destinee-a-etre-diffusee-dans-le-monde-entier_6202898_3232.html

Tal Bruttmann, historien : « Le Hamas a conçu, en amont, une politique de
terreur visuelle destinée à être diffusée dans le monde entier »


Par Anne Chemin

Le spécialiste de la Shoah estime, dans un entretien au « Monde », que
l’attaque perpétrée par le Hamas le 7 octobre contre Israël n’est ni
un pogrom ni un génocide mais un massacre de masse, et il met en garde contre
les analogies avec le nazisme.

L’historien Tal Bruttmann, spécialiste de la Shoah et de l’antisémitisme,
est notamment l’auteur de La Logique des bourreaux (Hachette, 2003), et, avec
Stefan Hördler et Christoph Kreutzmüller, d’Un album d’Auschwitz. Comment
les nazis ont photographié leurs crimes (Le Seuil, 304 pages, 49 euros).

Pour qualifier les attaques du Hamas, les hommes politiques, les historiens
et les éditorialistes ont parlé de massacre, d’attentat, de pogrom, voire
de génocide. En tant qu’historien, comment qualifieriez-vous cet événement ?

Le mot qui est revenu le plus souvent est « pogrom », mais les attaques du
Hamas ne relèvent pas, à mon sens, d’une telle qualification. Ce terme russe
désigne non pas les crimes de masse contre les juifs, mais la destruction
des biens qui sont en leur possession, accompagnée de violences contre les
personnes. Ce qui caractérise le pogrom, c’est le fait qu’une majorité,
excitée, voire incitée, par le pouvoir en place, s’attaque violemment à
une minorité qui vit en son sein.

Au XIXe et au début du XXe siècle, il y a eu, en Europe, beaucoup de pogroms
antijuifs, notamment en Russie ou en Roumanie, mais ce terme ne convient pas aux
attaques du Hamas. D’abord, parce qu’elles visaient non pas à détruire les
biens des Israéliens, mais à tuer des juifs ; ensuite, parce que les juifs,
en Israël, ne forment pas une minorité, mais une majorité ; enfin, parce que
le Hamas n’est pas un peuple, mais une organisation terroriste. Pour moi,
ces attaques sont des massacres de masse : le but était de tuer le plus de
juifs possible.  Certains ont utilisé le terme de génocide. Est-il, selon
vous, pertinent ?

Dans l’imaginaire occidental, le génocide est devenu l’alpha et l’oméga
du crime, alors qu’il n’est pas plus grave, en droit international,
que le crime de guerre ou le crime contre l’humanité. Personnellement,
en tant qu’historien, je n’utilise pas cette qualification juridique dont
la définition est d’une immense complexité : je la laisse aux magistrats
et aux tribunaux. C’est à eux d’établir, au terme d’une enquête,
si les massacres qui leur sont soumis sont, ou non, des génocides.

Le Hamas est évidemment profondément antisémite : sa charte initiale, qui
fait explicitement référence aux Protocoles des sages de Sion [un faux qui
date du début du XXe siècle], affirme que les juifs sont à l’origine de
la Révolution française, de la révolution bolchevique et de la première
guerre mondiale. Il faut cependant prendre le Hamas pour ce qu’il est :
un mouvement islamiste nationaliste qui n’est pas plus nazi qu’Al-Qaida,
l’Iran ou Marine Le Pen.  La Shoah apparaît comme une référence dans
nombre de discours sur les massacres du 7 octobre. Est-ce pertinent ?

La Shoah est incontestablement le pire épisode de l’histoire de
l’antisémitisme, mais cela n’en fait pas la clé à partir de laquelle on
peut comprendre toutes les violences antijuives. Parfois, elle nous empêche
même de saisir la singularité des événements : à force d’associer
l’antisémitisme à la Shoah, on oublie que cette haine a pris, au cours de
l’histoire, des formes très différentes.

L’antisémitisme du régime de Vichy n’est pas semblable à celui des nazis,
car il n’a pas pour vocation de tuer les juifs, mais de leur retirer leurs
droits de citoyens, ce qui n’est pas la même chose. Et cet antisémitisme de
Vichy n’a pas grand-chose à voir avec celui de Raymond Barre, qui s’était
indigné, en 1980, que l’attentat contre la synagogue de la rue Copernic
ait tué des « Français innocents », pas plus qu’il ne ressemble à celui
de Jean-Luc Mélenchon, qui a déclaré, en 2020, que Jésus avait été mis
sur la croix par « ses propres compatriotes ».  Quelle est, à vos yeux,
la spécificité du massacre commis par le Hamas le 7 octobre ?

La particularité de ce massacre, ce n’est pas qu’il vise des juifs –
c’est malheureusement traditionnel –, ni que les victimes ont été mutilées
et profanées par leurs bourreaux – cela se fait depuis l’Antiquité
–, mais qu’il a eu lieu en terre israélienne, dans un pays qui devait
initialement constituer un havre protecteur pour les juifs du monde entier. La
création de cet Etat, en 1948, devait leur permettre d’échapper aux violences
antisémites, mais cette espérance a volé en éclats le 7 octobre. C’est là
que se situe le traumatisme majeur chez les Israéliens et dans la communauté
juive mondiale.  Les combattants du Hamas ont tenu à filmer les attaques
qu’ils étaient en train de commettre. Comment comprendre leur geste ?

Ce qui est nouveau, à mon sens, c’est que l’opération du Hamas a été
précisément conçue pour générer des images, comme le montre le nombre de
combattants porteurs de caméras embarquées et de caméras-piétons. Parce que
les assaillants n’ont pas commis leurs massacres sur un territoire qu’ils
contrôlaient, ils ne pouvaient pas terroriser la population en exposant
leurs crimes au vu et au su de tous, comme le faisaient les Romains avec les
crucifixions ou les Allemands dans les territoires conquis – ils avaient
exhibé place Bellecour, à Lyon, les corps de cinq résistants exécutés en
juillet 1944.

Le Hamas a donc conçu, en amont, une politique de terreur visuelle destinée à
être diffusée dans le monde entier. Il a « réalisé » un événement dans
tous les sens du terme : il l’a à la fois commis et mis en scène. Certains
comparent ces images à celles du 11-Septembre, qui ont été suivies en direct
sur toute la planète, mais ce n’est pas Ben Laden qui a filmé les tours
en train de s’effondrer.  Ces films ont été intégrés dans un montage
israélien destiné à montrer les horreurs commises par le Hamas. Comment
comprendre que les mêmes images soient utilisées pour soutenir la cause du
Hamas, puis celle d’Israël ?

Ce double rapport à l’image était déjà présent dans la Shoah. La
quasi-totalité des images représentant des fusillades de juifs étaient
des photos « trophées » prises par les tueurs afin de les exhiber devant
leurs amis : pourtant, depuis 1945, ces mêmes photos servent à dénoncer le
nazisme. Sous l’Empire, les colons, en Afrique du Nord ou en Afrique noire,
prenaient, pour leur plaisir, des photos d’humiliations de femmes : depuis
les indépendances, elles ont, elles aussi, servi à dénoncer les humiliations
subies par les populations autochtones.

Ces images du Hamas placent Israël dans une situation à la fois complexe
et pernicieuse. L’armée israélienne veut, avec ce film, dénoncer la
violence des assaillants, mais elle doit également rester fidèle à la culture
israélienne, qui s’oppose, au nom de la dignité, à l’exhibition des corps
des victimes – les morts sont sacrés. En diffusant les images du Hamas,
même dans un cercle restreint, Israël renie donc une part de sa culture et
accepte le jeu d’un mouvement terroriste qui voulait justement exhiber les
corps des victimes israéliennes.  Si un procès sur les crimes de guerre du
Proche-Orient a lieu un jour, les images des assaillants du Hamas serviront
à nourrir leur dossier d’accusation. Est-ce que ce fut le cas pour la Shoah ?

Au procès de Nuremberg, très peu de photos prises par les Allemands ou par
ceux qui collaboraient avec eux ont été projetées : le système judiciaire de
l’époque ne considérait pas les images comme de véritables preuves. Ensuite,
les choses ont un peu changé : en 1961, les photos des nazis réunies dans
l’« album d’Auschwitz » ont été utilisées lors du procès d’Adolf
Eichmann à Jérusalem, en 1961, et, en 1963, ces mêmes photos ont permis
d’identifier un SS jugé à Francfort. En 1994, lors du procès de Paul
Touvier, une photo montrant les juifs tués à Rillieux-la-Pape a également
permis de nourrir le dossier d’accusation du milicien.  Une même photo
peut-elle donc raconter tour à tour la fierté du criminel, puis son
inhumanité ?

Depuis l’invention de la photographie, il y a près de deux siècles, nous
sommes persuadés que les images sont simples à comprendre, mais c’est faux
: elles sont souvent trompeuses. Observons, par exemple, les malentendus qui
peuvent naître de la lecture de l’« album d’Auschwitz », qui réunit
des photos prises par les nazis, au printemps 1944, pendant l’assassinat de
centaines de milliers de juifs hongrois.

Le travail que nous avons mené pendant cinq ans, avec les historiens Stefan
Hördler et Christoph Kreutzmüller, sur ces 197 photos montre d’abord que
ces images constituent une mise en scène. Elles ont été prises à un moment
chaotique de l’histoire d’Auschwitz – les Allemands étaient débordés
et les déportés se rebellaient –, mais, sur les photos, les juifs hongrois
marchent tranquillement vers la mort. Cette perception fausse a d’ailleurs
modelé une représentation commune de la Shoah.

La discordance entre notre regard et celui du photographe vient également du
fait que figurent, sur ces images, des choses que nous jugeons importantes,
mais auxquelles les nazis ne prêtaient aucune attention – la présence de
cannes par exemple. Pendant le processus de sélection, les SS les utilisaient
pour se reposer, donner des coups ou indiquer une direction, mais, à nos yeux,
ces cannes sont les traces tangibles des assassinats : elles avaient été
confisquées à des juifs tués dans les chambres à gaz.

Enfin, puisque les juifs sont les victimes et les Allemands les bourreaux,
nous sommes d’emblée, devant ces images, en empathie avec les juifs. Pour
les nazis, les déportés ne sont pourtant pas des victimes, mais des juifs
au sens nazi du terme. Le photographe veut donc montrer à ses supérieurs non
pas une tuerie, mais l’efficacité d’un processus d’extermination. Parce
qu’il ne parle pas le même langage que nous, ce qu’il raconte, au fond,
nous échappe.  Le dessinateur Joann Sfar a établi un parallèle entre une photo
de la Shoah et une photo du 7 octobre montrant toutes deux un soldat mettant
en joue une femme protégeant son enfant. Que pensez-vous de cette comparaison ?

Dans ces deux images, la gestuelle du bourreau et de la victime est la
même, mais le pourquoi et le comment diffèrent : ce n’est pas du tout
le même événement. Le Hamas est un mouvement terroriste profondément
antisémite, mais son programme ne repose pas, à ce jour, sur une politique
d’assassinat systématique des juifs à travers le monde, comme celui du
régime nazi. Finalement, la seule chose que montre cette comparaison entre la
photo de 1942 et celle du 7 octobre, c’est que l’imaginaire occidental est
centré sur la Shoah – au point que les images de cet événement modèlent
notre pensée.

Anne Chemin
