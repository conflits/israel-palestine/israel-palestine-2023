.. index::
   ! Pogroms

.. _pogroms:

=======================================================================================
**Pogroms des 7 et 8 octobre 2023**
=======================================================================================

- https://oct7map.com/
- https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Attentat_du_Hamas
- https://www.liberation.fr/societe/religions/attaque-du-hamas-en-israel-on-navait-jamais-vu-la-notion-de-pogrom-ressortir-avec-une-telle-force-20231027_LWTAMHPW5JGVBD2LXISXG32SDE/
- https://www.youtube.com/watch?v=XE3gzRB7q_E&t=5s&ab_channel=AkademTV (Israël : peut-on parler de pogrom ? Par Marie Moutier-Bitan)


.. toctree::
   :maxdepth: 3

   beeri/beeri
   kfar_azza/kfar_azza
   reim/reim
   ressources/ressources
