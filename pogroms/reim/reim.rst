.. index::
   pair: Pogrom; Re'im

.. _pogrom_reim:

=======================================================================================
**Pogrom dans le kibboutz de Re'im**
=======================================================================================

- https://fr.wikipedia.org/wiki/Massacre_du_festival_de_musique_de_R%C3%A9%C3%AFm


.. figure:: images/reim.png

   Re'im (Lien sur la carte satellite : https://urlr.me/CvP4m)

Attaque
===========

Le massacre du festival de musique de Réïm se déroule le 7 octobre 2023, au début de l'invasion d'Israël par des terroristes du Hamas, dans le cadre de l'opération Déluge d'al-Aqsa. Ceux-ci, après avoir pénétré en Israël à partir de la bande de Gaza, commettent un massacre de civils réunis lors d'un festival de musique près du kibboutz de Réïm, dans le désert du Néguev, au sud d'Israël. Deux cent soixante personnes au moins sont tuées et de nombreuses autres blessées. Les terroristes prennent des otages.



**Maya Puder, 25 ans, actrice en herbe, aimée de tous**
====================================================================================

- https://fr.timesofisrael.com/maya-puder-25-ans-actrice-en-herbe-aimee-de-tous/

.. figure:: images/maya.png


Maya Puder, 25 ans, a été assassinée alors qu’elle participait au festival de musique Supernova, non loin du kibboutz Reim, le 7 octobre.

Maya, originaire de Zichron Yaakov, avait étudié le théâtre à l’école d’art dramatique Yoram Loewenstein de Tel Aviv.

« Nous sommes peinés et choqués par la mort prématurée de Maya Puder, qui a été assassinée par des terroristes du Hamas », a déclaré l’école dans un communiqué. « Maya était une femme belle et magique, agréable et jolie, avec un sourire captivant et une personnalité unique. Elle était aimée de ses professeurs et de ses camarades de classe. »

En 2012, Maya a été campeuse au Tamarack Camps’ Ruach Village dans le Michigan, dans le cadre du programme Partnership2Gether de Détroit. « Nos cœurs sont brisés par cette perte incommensurable », ont déclaré les organisateurs dans un communiqué.

Rotem Alima, une amie de Maya, a déclaré à la Treizième chaîne que son amie « était comme de la colle ».

« Elle pouvait se connecter à tout le monde, elle attirait tellement de gens vers elle, elle était la colle qui nous gardait ensemble. Elle était vraiment spéciale, elle savait profiter de la vie (…) elle était si talentueuse. »

Shira Pinkas, qui a travaillé avec Maya dans un restaurant de Tel Aviv, l’a décrite comme « fine, intelligente et drôle ».

« Elle était une véritablde lumière pour nous tous. Elle nous a appris à ne pas prendre les choses trop à cœur, à les prendre avec légèreté – elle avait beaucoup d’humour et d’autodérision (…). C’était le genre de fille que tout le monde considérait comme sa meilleure amie. »

