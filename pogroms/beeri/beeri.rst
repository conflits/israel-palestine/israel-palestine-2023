.. index::
   pair: Pogrom; Beeri

.. _pogrom_beeri:

=======================================================================================
**Pogrom dans le kibboutz de Beeri**
=======================================================================================

- https://fr.timesofisrael.com/quinze-jours-apres-lassaut-du-hamas-des-benevoles-fouillent-toujours-les-cendres-a-beeri/?utm_source=A+La+Une&utm_campaign=a-la-une-2023-10-24
- https://urlr.me/CvP4m
- :ref:`vivian_silver_2023_11_14`

