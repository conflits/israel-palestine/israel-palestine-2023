.. index::
   pair: Pogrom; Kfar Azza

.. _pogrom_kfar_azza:

=======================================================================================
**Pogrom dans le kibboutz de Kfar Azza**
=======================================================================================

- https://fr.wikipedia.org/wiki/Massacre_de_Kfar_Aza


.. figure:: images/kfar_azza.png

   Kfar Azza (Lien sur la carte satellite : https://urlr.me/CvP4m)

Attaque
===========

Le kibboutz de Kfar Aza est l'une des premières cibles de l'attaque
surprise contre Israël, lancée par le Hamas au petit matin du 7 octobre 20232.

Le kibboutz a été repris par Israël le 9 octobre 2023, après plus de
36 heures de combats.

Selon l'armée israélienne, 70 membres du Hamas ont envahi Kfar Aza.


**Niv Raviv, 27 ans, Nirel Zini, 31 ans, des funérailles au lieu de fiançailles**
====================================================================================

- https://fr.timesofisrael.com/niv-raviv-27-ans-nirel-zini-31-ans-des-funerailles-au-lieu-de-fiancailles/

Niv Raviv, 27 ans, et Nirel Zini, 31 ans, un jeune couple qui prévoyait
de se fiancer, ont été assassinés lorsque des terroristes palestiniens
du Hamas ont fait irruption à Kfar Azza le 7 octobre.


.. figure:: images/niv_et_nirel.png

Le couple, qui s’était rencontré alors qu’il était dans l’armée, s’était
installé dans le kibboutz au début de l’année, à la recherche d’une vie
simple et tranquille.

Raviv un master en psychologie tandis que Zini espérait étudier le droit.

Zini avait prévu de faire sa demande en mariage, a déclaré leur famille.

Raviv voulait se fiancer le 10 octobre et se marier un an plus tard, le
10 octobre 2024. Au lieu de cela, ils ont tous deux été assassinés trois
jours seulement avant la date prévue de la demande en mariage.
Leurs familles ont attendu une semaine avant d’apprendre que leurs corps
avaient été retrouvés, et ils ont finalement été enterrés côte à côte
à Netanya.

« Ils avaient déménagé il y a six mois pour vivre dans le kibboutz
Kfar Azza. Ils voulaient vivre dans un kibboutz et mener une vie simple,
ils disaient que c’était le paradis là-bas », a déclaré Shachar, la
belle-sœur de Raviv.

« Ils aimaient le kibboutz, la communauté, les gens. Ils étaient si
heureux là-bas (…). Ils étaient toujours actifs – ils aidaient, faisaient
du bénévolat, des dons. C’étaient des gens tellement bons. »

Noam Zini, le frère de Nirel, a promis de parler à ses enfants de
l’héritage de son oncle et de sa tante.

« Je me souviendrai de vous deux pour toujours, vous êtes gravés dans mon
cœur – mon cœur qui est brisé en morceaux », a écrit Noam.

« Je ne sais pas comment continuer à partir d’ici, comment recoller
les morceaux, comment continuer à faire comme si tout était normal
alors que rien ne redeviendra jamais comme avant. »

« Vous avez été enterrés l’un à côté de l’autre, et c’est ainsi que vous
resterez pour toujours ! Niv, je n’oublierai jamais ton sourire et ta
force aux côtés de Nirel – je sais que tu étais tout pour lui, et je
crois qu’il a tout fait pour toi. Nous vous aimons tous les deux infiniment,
et nous ne vous oublierons jamais. »
