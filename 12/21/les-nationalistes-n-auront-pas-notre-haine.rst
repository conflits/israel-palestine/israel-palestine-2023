
.. _cnt_ait_2023_12_21:

=======================================================================================================
2023-12-21 **Anarchosyndicalisme! n°184 (CNT-AIT) - Les nationalistes n'auront pas notre haine**
=======================================================================================================

- http://cntaittoulouse.lautre.net/spip.php?article1375
- http://cntaittoulouse.lautre.net/spip.php?article1368 (Anarchosyndicalisme ! n°184)
- https://www.foreignaffairs.com/israel/what-palestinians-really-think-hamas

**Ilan Shalif est un compagnon anarchiste qui vit en Israël**. 

Agé de 86 ans, il est le vétéran du mouvement anarchiste dans la région, 
ayant évolué d'une position marxiste-léniniste sioniste dans sa jeunesse 
vers une position anarchiste en 1966, après avoir compris et vu concrètement 
l'impasse du colonialisme. 

Infatigable propagandiste de l'anarchisme organisé (il a traduit en hébreu 
la Plateforme d'Archinov), engagé activement dans la lutte contre la colonisation, 
il a participé activement au mouvement des anarchistes contre le mur et 
aux marches pacifiques du village de Bil'in (voir par exemple: http://cnt-ait.info/2020/02/12/bilin/). 

Voici un message qu'il a publié sur sa page facebook pour remettre les choses 
à leur place, au-delà de toutes les propagandes nationalistes, qu'elles 
soient du gouvernement israélien ou du Hamas:

Qu'est-ce que les palestiniens pensent réellement du hamas?
=====================================================================

Peu importe les efforts déployés par la machine de propagande sioniste pour
noircir tous les habitants de Gaza, elle n'y parviendra pas.

Le gouvernement israélien a nourri le Hamas - en le laissant s'organiser,
mener ses activités humanitaires et obtenir de l'argent de toutes les
sources. 

De plus, il était probablement au courant de l'attaque à venir:
il a permis au Hamas de prendre la bande de Gaza, il lui a permis de rester
au pouvoir et de former ses combattants. Le gouvernement israélien est donc
aussi responsable du massacre du 7 octobre.

Quelques données intéressantes sur l'opinion publique à Gaza à la veille
du déclenchement de la guerre, tirées d'un article publié le 25 octobre
2023 par la revue internationale de référence» FOREIGN AFFAIRS».
(https://www.foreignaffairs.com/israel/what-palestinians-really-think-hamas)

Il vaut la peine de noter deux choses avant:

1. Le sondage de Gaza a été réalisé le 6 octobre.

2. Dans le passé, la sympathie pour le Hamas a augmenté à mesure que les
affrontements avec les forces israéliennes se sont intensifiés, donc si le
passé est un indicateur du présent et de l'avenir, il est probable que ces
chiffres ont changé un peu, mais restent intéressants.

Voici un résumé de quelques données intéressantes de cet article
====================================================================

73 % des habitants de Gaza ont déclaré qu'ils préféraient la paix avec
Israël à la poursuite du conflit et qu'ils ne partageaient pas l'idéologie
du Hamas visant à détruire l'État d'Israël. La majorité (54 %) choisirait
une solution à deux États, 10 % choisiraient un modèle de fédération
israélo-palestinienne, où chaque partie serait indépendante, mais des liens
étroits seraient maintenus entre les parties, et 9 % préféreraient un État
commun pour les Arabes et les juifs.

La veille du déclenchement de la guerre, seulement 29 % des Gazaouis
exprimaient leur confiance dans le gouvernement du Hamas; 72 % des Gazaouis
pensaient que le régime du Hamas était corrompu et qu'il ne répondait pas
à leurs besoins fondamentaux.

20 % des habitants de Gaza préféraient une solution militaire, l'écrasante
majorité d'entre eux étant des partisans du Hamas, soit environ 15 % de
la population.

