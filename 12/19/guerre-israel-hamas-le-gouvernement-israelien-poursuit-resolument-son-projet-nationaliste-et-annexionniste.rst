

.. _perelman_2023_12_19:

====================================================================================================================================================
2023-12-19 Guerre Israël-Hamas : « Le gouvernement israélien poursuit résolument son projet nationaliste et annexionniste » par Nitzan Perelman
====================================================================================================================================================

- https://www.lemonde.fr/idees/article/2023/12/19/guerre-israel-hamas-le-gouvernement-israelien-poursuit-resolument-son-projet-nationaliste-et-annexionniste_6206627_3232.html


Commentaires de membres du  RAAR (2023-12-20)
=================================================

Commentaire 1
----------------

Drôle de choix en vérité. Elle émane d’une personne vivant en France avec 
un prisme particulier https://www.france-palestine.org/Nitzan-Perelman-et-Thomas-Vescovi-en-conference-a-Nancy-sur-la-montee-de-l

Mais surtout les éléments non sourcés apportés par cette doctorante en 
sociologie sont contredits par toutes les études existant sur le terrain … 
l’annexion de Gaza, rejetée par une majorité d’israéliens, n’est pas à 
l’ordre du jour. 

Elle ne figure qu’au programme des fantasmes de l’extrême droite….

Par ailleurs **parler du 7 octobre comme d’une bonne occasion relève 
de l’obscénité**. 

Soi dit en passant, la France qui se dit "menacée" n’a pas eu besoin 
pourtant du 7 octobre pour proposer sa loi pourrie sur l'immigration…

Commentaire 2
----------------

Je connais cette « étudiante «qui est tout sauf sociologue et qui fait 
partie avec son pseudo-collègue Vascovi d’une génération de pseudo-étudiant-es 
introduisant de la confusion dans l’usage des sciences sociales et de la 
science dans l’espace public et militant.

Rien n’est sourcé, validé, rigoureux et on peut opposer à cette tribune 
tous les sondages qui sortent en Israel, majoritairement contre une annexion.

Il est regrettable que le Monde n’effectue pas le travail de vérification 
des faits ou des données avant publication d’une tribune, même si elle 
n’engage que son autrice.

Autrice Nitzan Perelman Sociologue
======================================

Nitzan Perelman Sociologue

Nitzan Perelman est doctorante en sociologie à l’université Paris Cité. 
Ses travaux portent notamment sur la société israélienne.  

Tribune
=======

Non sans cynisme, le gouvernement de Benyamin Nétanyahou s’est saisi de
l’attaque terroriste du 7 octobre pour poursuivre ses objectifs d’expansion
territoriale et d’élargissement de la présence juive « de la mer au Jourdain
», estime la sociologue Nitzan Perelman dans une tribune au « Monde ».

Alors que les regards sont tournés vers Gaza où,
après la libération épuisante des otages, l’attaque israélienne a repris,
occasionnant plus de 15 000 morts selon l’OCHA (Coordination des affaires
humanitaires des Nations unies), le gouvernement poursuit résolument son
projet nationaliste et annexionniste. Depuis sa nomination en décembre 2022,
son gouvernement, le plus à droite et le plus suprémaciste qu’Israël
n’ait jamais connu, a mis en place d’importantes réformes concernant la
fonction publique, le pouvoir judiciaire et la colonisation.

Au lendemain de l’attaque du Hamas le 7 octobre, il a cherché à «
saisir l’opportunité » pour faire progresser ses objectifs d’expansion
territoriale et d’élargissement de la présence juive « de la mer au
Jourdain » [de la mer Méditerranée au fleuve Jourdain].

Dans ce contexte, le discours sur le « retour à Gaza » revêt une légitimité
sans précédent. En 2005, sous le gouvernement d’Ariel Sharon, est mis en
place un plan controversé de « désengagement ». Bien qu’il ait été un
des principaux alliés du mouvement des colons, Sharon ordonne la destruction
du bloc de colonies Gush Katif dans la bande de Gaza ainsi que quatre autres
colonies dans le nord de la Cisjordanie. Le « désengagement » constitue
un profond traumatisme au sein du camp nationaliste israélien. Il est perçu
comme une grande trahison du premier ministre et une erreur à corriger.

Depuis le 26 octobre, l’opération terrestre israélienne à Gaza paraît
en offrir l’opportunité. Alors que plusieurs ministres du gouvernement
appellent à « profiter de l’occasion » pour conquérir et occuper la zone,
tout en y érigeant de nouvelles colonies, une grande partie de la société
israélienne semble également encline à cette idée : selon un sondage de
la chaîne Canal 12, 44 % des Israéliens sont favorables à la reconstruction
des colonies à Gaza après la guerre, tandis que 39 % y sont opposés.

« Zones de sécurité dépourvues d’Arabes » Beaucoup l’ignorent, mais il y
a quelques mois, le Parlement en offrait la possibilité juridique. Le 21 mars,
les députés ont voté une loi mettant fin au plan de désengagement, ouvrant
ainsi la voie à la reconstruction des colonies dans les zones concernées : la
bande de Gaza et les quatre colonies en Cisjordanie. Alors que l’autorisation
de « retourner à Gaza » paraissait purement symbolique, Orit Strock,
ministre des missions nationales, déclara le même jour à un média de la
droite radicale : « Le retour dans la bande de Gaza impliquera de nombreuses
victimes, malheureusement (…), mais il ne fait aucun doute qu’en fin de
compte elle fait partie de la terre d’Israël et qu’un jour viendra où
nous y reviendrons. » Ses propos semblent plus que jamais d’actualité.

L’expansion de la colonisation ne se limite pas à la bande de Gaza, mais
concerne également la Cisjordanie. Bezalel Smotrich, ministre des finances,
a appelé à « tirer des leçons des événements du 7 octobre » et à les
appliquer en Cisjordanie en créant des « zones de sécurité dépourvues
d’Arabes » autour de chaque colonie. Autrement dit, à étendre leur
territoire. Bien que sa demande ne soit pas encore mise en œuvre, les colons
et l’armée l’appliquent en menaçant les Palestiniens avec des armes,
en les contraignent à quitter leurs foyers, causant la mort de 243 personnes
selon l’OCHA.

D’après l’organisation israélienne des droits de l’homme B’Tselem,
entre le 7 octobre et le 30 novembre, 1 009 Palestiniens ont été expulsés
de leurs maisons en Cisjordanie, affectant seize communautés. Rappelons
que le gouvernement de Benyamin Nétanyahou a battu des records en matière
d’autorisation de construction dans les colonies, avec 13 000 accordées
en sept mois (le record précédent étant de 12 000 pour toute l’année
2020), ainsi que la légalisation de 22 avant-postes, selon l’organisation
La Paix maintenant.

L’oppression s’accroît A l’intérieur du territoire israélien, d’autres
processus importants se déroulent depuis le 7 octobre. Le ministre de la
sécurité nationale, Itamar Ben Gvir, tire parti de l’anxiété ressentie par
les Israéliens pour concrétiser plusieurs projets, notamment la distribution
massive d’armes aux citoyens israéliens. Lorsqu’il a pris ses fonctions,
il promettait d’introduire 30 000 nouvelles armes dans les rues.

Depuis le 7 octobre, cet objectif a été largement dépassé avec 255 000
nouvelles demandes d’acquisition d’armes en seulement cinq semaines,
selon le quotidien Haaretz. Pour cela, Ben Gvir a modifié les critères
d’obtention, de sorte que les nouveaux demandeurs ne sont plus tenus de
passer un entretien et que, pour certains, notamment ceux ayant effectué le
service militaire obligatoire, aucun entretien n’est nécessaire (50 % selon
le journal Calcalist). De plus, il organise chaque semaine des distributions
d’armes dans de nombreuses villes du pays, encourageant les gens à faire
de nouvelles demandes de permis.

Outre ces distributions, Ben Gvir prévoit la création de 700 « unités
prêtes », composées de citoyens possédant des armes et prêts à réagir
en cas d’urgence. Cette initiative suscite de vives inquiétudes auprès
de certains membres de la police, qui trouvent les citoyens recrutés « trop
motivés » ou sont préoccupés par leurs positions politiques, en particulier
de leur tendance raciste envers les Palestiniens citoyens de l’Etat israélien.

Ici, il faut préciser que depuis le 7 octobre, cette même police surveille de
près les réseaux sociaux des Palestiniens citoyens d’Israël et procède à
un grand nombre d’arrestations pour chaque partage, publication ou même un
simple like exprimant sa solidarité avec les Gazaouis ou sa critique envers
la politique du gouvernement. Dans ce contexte, l’oppression envers eux ne
cesse de s’accroître, alors que toute expression de solidarité avec les
Gazaouis est considérée comme une trahison contre l’Etat.

Les actions entreprises par le gouvernement depuis le 7 octobre, en plus de
l’attaque à Gaza, nécessitent une analyse minutieuse. Il semble presque
cynique que ce dernier cherche à tirer profit du chaos et de la peur pour
faire avancer des projets planifiés de longue date. Ils méritent d’être
mis en lumière, car ils auront des conséquences majeures sur l’avenir,
de plus en plus incertain, de la question israélo-palestinienne.


