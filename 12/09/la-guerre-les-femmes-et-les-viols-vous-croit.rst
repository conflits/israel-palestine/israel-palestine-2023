.. index::
   pair: Fabienne Messica ; 2023-12-09

.. _messica_2023_12_09:

=====================================================================
2023-12-09 **La guerre, les femmes et les viols : on vous croit**
=====================================================================

- https://blogs.mediapart.fr/messicafabienne/blog/091223/la-guerre-les-femmes-et-les-viols-vous-croit
- :ref:`fabienne_messica`

Préambule
==============

La lutte contre le sexisme, les violences faites aux femmes, l’antisémitisme
et les racismes ne souffre aucune exception, aucune exemption, y compris au
sein de la gauche antiraciste et féministe. 

N'oublions pas ce que les corps des femmes exhibés, malmenés, torturés, 
violés ou anéantis sous les bombes et ceux des enfants, nous disent de 
cette guerre ente Israël et le Hamas.

A l’heure où cette tribune est publiée, de nombreux textes ont abordé la
question des viols le 7 octobre 2023 sous des angles d’ailleurs différents. 

Il m’aura fallu trois semaines pour parvenir à une version suffisamment
rassembleuse tant le sujet est lourd de conséquences et psychologiquement
chargé. 
Il est important de comprendre qu’à l’heure où la Cour Pénale Internationale 
a été saisie pour crimes de guerres, crimes contre l’humanité et crimes 
de génocide à l’encontre de l’État d’Israël, les crimes de viols commis 
en Israël le 7 octobre 2023 – et possiblement à l’encontre des
otages - sont constitutifs de crimes de génocide. 

Ceci fait suite à plusieurs jugements prononcés à partir de 1998 par le 
Tribunal Pénal International pour l’ex-Yougoslavie ainsi que le jugement 
prononcé en 1998 par le Tribunal International pour le Rwanda à l’encontre 
de Jean-Paul Akayesu.

Malgré les appels nombreux à un cessez-le-feu et à la libération des otages,
l’offensive israélienne à Gaza se poursuit et des familles entières,
dont de nombreuses femmes et enfants sont anéanties.

**La justice internationale est saisie**. 

Ce texte n’a pas prétention à rendre à sa place un jugement mais à dénoncer 
le caractère patriarcal de cette guerre.

Fabienne Messica.

Tribune 
============

**Femmes victimes de viols dans des actes de guerre : qui que vous soyez,
israéliennes, palestiniennes ou de tout autre pays : on vous croit !**

La lutte contre le sexisme, les violences faites aux femmes, **l’antisémitisme
et les racismes** ne souffre aucune exception, aucune exemption, y compris au
sein de la gauche antiraciste et féministe.

**Rien ne justifie les horreurs contre les populations civiles**. 

Confondre la solidarité qui est aussi la nôtre avec les Palestinien-es 
pour leurs droits **avec le déni des souffrances endurées le 7 octobre 2023 
par les civils israéliens et en particulier les femmes, c’est se tromper de combat**.

Nous tenons à l’affirmer bien que nous ne partagions pas l’accusation trop
globale et sans nuance à l’égard des mouvements féministes à qui l’on
reproche le retard ou parfois le déni dans la dénonciation des violences
subies le 7 octobre 2023 par des femmes israéliennes. 

Nous savons aussi que les extrêmes-droites se saisissent de ces tensions 
dans le but d’invalider les luttes des femmes pour leurs droits, pour 
l’égalité et contre les violences sexistes et sexuelles. 
Mais si cette tentative de délégitimer le féminisme est inacceptable, la 
critique est permise.

La question des viols du 7 octobre 2023 est cruciale pour la propagande 
autour du Hamas qui, dès les premières heures, s’est attachée à nier 
l’extrême cruauté des actes commis (malgré les scènes filmées par les assaillants
eux-mêmes et envoyées aux familles des victimes) pour que ces actes soient
nimbés du qualificatif de résistance. 

**Cette violence spécifique peut aussi faire l’objet d’une instrumentalisation 
par le gouvernement israélien pour justifier une riposte toujours plus 
meurtrière que nous condamnons fermement**.

Mais ce n’est pas un motif pour invisibiliser ces crimes car la condition
des femmes et les violences spécifiques qui s’exercent à leur égard ne
sont jamais secondaires aux intérêts des uns et des autres !

La parole féministe ne peut être confisquée, embrigadée, paralysée au
prétexte d’instrumentalisations possibles. 

Or le silence sur les viols et violences sexistes et sexuelles du 7 octobre 2023 
dans de nombreux pays et même à l’ONU Femmes[1], en est un exemple. 
**Si nous le disons, ce n’est pas pour attaquer le féminisme, mais pour 
réaffirmer ses valeurs qui devraient primer**.

En tant que féministes, condamner, quels qu’en soient leurs auteurs, 
les viols et violences sexistes et sexuelles qui sont des crimes contre 
l’humanité, **relève de nos fondamentaux**. 

Notre critique du patriarcat, de la glorification de la guerre, de la 
violence, de la puissance, de l’esprit militaire et du nationalisme et 
du colonialisme qui font système, reste entière. 
Nous savons que la guerre joue un rôle essentiel pour perpétuer les innombrables
dominations tandis que les femmes occupent une place spécifique dans les
stratégies guerrières : faire des enfants, les garçons seront des soldats
et les filles feront des enfants… 

De plus, l’atteinte aux corps des femmes est considérée par des belligérants 
comme l’atteinte à la virilité de"l’ennemi" dans une conception et une 
pratique, fondamentalement virilistes.

C’est avec **les féministes pacifistes** qui dans ce conflit ont œuvré dans
la solitude, supportant les insultes et les menaces des hommes nationalistes,
masculinistes, fanatiques religieux qui les considèrent comme des traitres
et "des putains de l’ennemi" que nous pouvons défaire ces logiques;

c’est avec **nos sœurs israéliennes et palestiniennes** qui tentent depuis 
des décennies de tracer une autre voix et sont renvoyées à leur appartenance
nationale, israéliennes ou palestiniennes, à leur qualité de juives,
de musulmanes ou de chrétiennes dans une perspective qui les assigne à la
solidarité avec les bellicistes de leur pays ou de leur religion, alors même
que la ligne de fracture ne se situe pas seulement entre deux peuples mais à
l’intérieur de chacun de ces peuples.

Tel est le message du `mouvement les Guerrières de la paix <guerrieres:paix>`, 
un mouvement féministe mixte, créé il y a un an en France qui milite avec 
les sociétés civiles israéliennes et palestiniennes dans la continuité 
des Femmes en noir, mouvement israélo palestinien créée en 1988 pour 
mettre fin à la colonisation. 

Ni l’ethnicité, ni la religion ne sont la cause de la guerre mais bien 
plus des idéologies nationalistes et xénophobes. 

**Le lien entre le patriarcat et le nationalisme est largement démontré**. 

**Défaire le patriarcat, c’est défaire le nationalisme belliqueux et xénophobe. 
Défaire le patriarcat, c’est défaire la guerre ; et les autocrates le 
savent bien qui verrouillent la liberté et les droits des femmes**.

Nous ne pouvons dédouaner ni la société israélienne, ni la société
palestinienne et les pouvoirs politiques qui les dirigent de tout
patriarcat. 

L’expérience des femmes qui ont participé à des mouvements de libération 
nationaux devrait à cet égard nous enseigner que les moyens utilisés dans 
des guerres de libération, parmi lesquels l’éviction de tout groupe 
concurrent et souvent l’oppression du peuple qu’on prétend libérer, ne 
sont pas neutres. 

Ce n‘est donc pas n’importe quel acte dit de résistance ni n’importe quel 
pouvoir politique qu’il convient de soutenir.

Dans son livre, "Ne suis-je pas une femme [2]",  bell hooks, célèbre
afro-féministe, décrit avec finesse la double exclusion des femmes noires. 
Elle montre que des oppressions peuvent se combiner, dans la domination 
des femmes noires, à savoir **le racisme d’une part** et d’autre part, **le 
sexisme dans son propre camp de lutte contre le racisme**.

Alors quand nous lisons, dans une tribune, publiée le 21 novembre 2023 dans 
"le media", par un collectif de féministes militantes, chercheuses et artistes,
que ce dernier, non seulement nie l’augmentation des actes antisémites dans
le monde mais encore, réduit les évènements du 7 octobre 2023 à de la simple
propagande, **c’est pour nous un détournement du féminisme**. 

Écrire que parler de ces viols et des violences sexistes et sexuelles et 
condamner les actions des combattants du Hamas, c’est"réitérer la vision 
d’un monde musulman barbare contre une population israélienne féminisée 
et ainsi lavée et blanchie de tout soupçon. 

La condamnation sans appel des combattants du Hamas s’arrime en effet à 
la construction d’un Orient monstrueux, nécessairement coupable des pires 
atrocités contre les femmes, permettant ainsi une fois de plus d’annuler 
toute perspective historique quant à la violence intrinsèque à la colonisation" 
est une **forme alambiquée de deshumanisation des victimes et d’héroïsation 
d’actes que nous avons tout de même coutume de condamner sans réserve**.

Certes, **le discours qui fait de cette guerre un conflit de civilisation
est islamophobe et nous le rejetons fermement**. 

**Mais un discours qui nie les atrocités commises le 7 octobre 2023 au nom 
de la lutte contre l’islamophobie n’est pas entendable non plus**.

A nous féministes de nous montrer dans ce domaine à la hauteur des enjeux. 

A nous, engagées dans la lutte contre le sexisme et tous les racismes et 
pour une paix juste et durable entre Israël et la Palestine de dénoncer 
sans réserve et sans exception, les virilismes, la militarisation des 
conflits et le patriarcat.

Premiers signataires
========================

- Fabienne Messica, Ligue des Droits de l’Homme, Golem
- Pierre Tartakowsky, président d’honneur de la Ligue des Droits de l’Homme,
- Nora Tenenbaum, militante féministe,
- Jonas Pardo, militant et formateur contre l'antisémitisme, golem
- Laura Fedida, militante antiraciste
- Mélanie Jaoul, féministe intersectionnelle, Présidente de AATDS
- Illana Weizman, essayiste
- Sandro Munari architecte-urbaniste
- Deborah de Robertis, artiste
- Sarite Rosen Artiste plasticienne
- Fiona Schmit, autrice et militante féministe
- Eva Vocz, miltante féministe, chargée de plaidoyer à Act Up,

[1] Qui réagit tardivement par un tweet lunaire le 25 octobre 2023.

[2] C’est Sojourner Truth, militante noire américaine qui prononça cette
phrase en 1851 dans un discours à une Convention des femmes à Akron en Ohio
