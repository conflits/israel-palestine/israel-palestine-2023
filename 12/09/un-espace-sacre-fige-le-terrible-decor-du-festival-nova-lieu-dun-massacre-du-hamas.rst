
.. _nova_2023_12_08:

===============================================================================================================================
2023-12-08 **Un « espace sacré » fige le terrible décor du festival Nova, lieu d’un massacre du Hamas** 
===============================================================================================================================

- https://fr.timesofisrael.com/un-espace-sacre-fige-le-terrible-decor-du-festival-nova-lieu-dun-massacre-du-hamas/

Nous allons réunir de nouveau 3 000 personnes pour danser, car nous sommes 
une communauté, une tribu qui continue d'exister", affirme Omri Sassi, 
l'un des fondateurs de Tribe of Nova


Tentes de camping, bar sur lequel traînent bouteilles vides et cendriers 
pleins, étals divers et même l’imposante sono : près de Tel-Aviv a été 
installé le décor du festival de musique Nova, figé au moment où, le 7 octobre, 
la fête a laissé place au massacre.

Deux mois, jour pour jour, après l’attaque sans précédent menée dans le 
sud d’Israël par le Hamas depuis la bande de Gaza, a ouvert jeudi au 
public une exposition temporaire consacrée à la mémoire des 364 personnes, 
venues pour danser et méthodiquement abattues par des terroristes de la 
branche palestinienne des Frères musulmans.

Mercredi soir, le lieu accueillait les proches des victimes. Silencieuses, 
ils pénètrent lentement dans l’immense salle plongée dans la pénombre, 
accueillis par un écran lumineux affichant noms et photos de ces 364 morts, 
parmi lesquels l’un des leurs.

Non loin sont disposés des effets et objets non réclamés. Dans l’espoir
d’y trouver un souvenir d’un être cher, les proches de victimes scrutent
attentivement chaussures et casquettes alignées sur une table, lunettes,
parfums ou porte-clés empilés, et des dizaines d’habits suspendus sur des
cintres à des portants, rappelant un funeste magasin.

Ceux qui trouveraient quelque chose pourront demander à le récupérer,
photos à l’appui.

En larmes, s’épaulant les uns les autres, ils parcourent ensuite l’immense
salle, placés au cœur du décor même où leurs proches ont vécu leurs
derniers instants.

Parmi ces parents endeuillés, Amit Zender, 63 ans, vêtu d’un t-shirt
affichant la photo de sa fille Noa et les dates « 2000-2023 ».

« Je suis venu voir à quoi ressemble ce festival où ma fille est morte
», confie-t-il, réclamant un « musée permanent » racontant la terrible
journée du 7 octobre, durant laquelle quelque 1 200 personnes au total,
majoritairement des civils, ont péri, selon les autorités israéliennes.

Ce jour-là, plus de 3 000 personnes participaient depuis la veille au festival
Nova « pour la paix », dans un champ du Néguev, à cinq kilomètres de la
bande de Gaza.

A 06H29 – titre de cette exposition créée par les organisateurs du festival
– les sirènes d’alerte ont retenti.

Rapidement, des hommes armés ont surgi en voiture, mais aussi en paramoteurs,
et ont immédiatement ouvert le feu sur la foule, avant de lancer la chasse
aux festivaliers s’enfuyant à pied, tentant de s’échapper en voiture,
ou essayant de se terrer dans des abris improvisés.

Des heures durant, les terroristes du Hamas abattent des festivaliers désarmés,
puis prennent une quarantaine d’otages qu’ils emmènent de force dans la
bande de Gaza, où la plupart restent détenus.

Les morts et blessés évacués, restent sur le site durant plusieurs jours
les installations, des dizaines de véhicules incendiés et, éparpillés sur
des centaines de mètres, des tentes, sacs de couchages, habits, chaussures
ou glacières, abandonnés à la hâte.

Les organisateurs ont réinstallé à Tel-Aviv ce décor d’après-massacre :
sous des arbres reliés par des guirlandes lumineuses, un campement de tentes
vides, souvent ouvertes, devant lesquelles gisent des chaises renversées.

Plus loin, sur le comptoir du bar du festival subsistent les restes de ce qui
était encore une fête. Ailleurs, des carcasses de voitures carbonisées,
mais aussi, des toilettes mobiles, où certains crurent trouver une cachette
illusoire, portant des impacts de balles.

En fond sonore, de la musique techno créée pour l’exposition, mais «
dans l’esprit du festival », selon les responsables de « Tribe of Nova »,
organisateurs du festival.

Pour Nitzan Schlesinger, 27 ans, cette visite est « un moyen de rester en
contact avec d’autres familles qui ont vécu la même chose ». Son père
Assaf Schlesinger, un secouriste, a « soigné des blessés » sur le site du
festival, avant de succomber aux balles des terroristes, dit-elle.  « Mon frère
Idan Dor, 25 ans, a été assassiné dans cette fête et il a fallu huit jours
pour qu’on nous annonce sa mort », confie de son côté Daniela Dor-Levin,
« il aimait danser, il avait juste commencé sa vie, il voulait la paix ».

« Venir ici, ce soir, est peut-être le jour le plus difficile depuis sa
mort, car j’ai vu son nom à l’entrée, les carcasses de voitures (…)
ça se concrétise pour moi », explique la jeune femme, les larmes aux yeux.

Présent à l’inauguration, le président israélien Isaac Herzog a qualifié
l’exposition « d’espace sacré » et promis aux familles « de ne jamais
oublier la beauté et la bonté de nos chers disparus ».

A la sortie, un panneau en anglais proclame : « We will dance again » («
Nous danserons à nouveau »).

« Nous allons réunir de nouveau 3 000 personnes pour danser, car nous sommes
une communauté, une tribu qui continue d’exister », affirme Omri Sassi,
l’un des fondateurs de Tribe of Nova.

