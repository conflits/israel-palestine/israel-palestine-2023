
.. _cnt_ait_2023_12_09:

========================================================================================
2023-12-09 **Adresse aux déserteurs de toutes les nations** par CNT-AIT Toulouse
========================================================================================

- http://cntaittoulouse.lautre.net/spip.php?article1369

Dans un monde où la barbarie est banale, les attaques du Hamas le 7 octobre 2023
ont constitué un nouveau pallier dans l’horreur : 1400 morts israéliens dont
1100 civils (prolétaires, pour la plupart) – massacrés dans des conditions
atroces – et 300 militaires et policiers, ainsi que des centaines d’otages
emmenés dans la bande de Gaza. La double dimension – principalement criminelle
et pogromiste, mais également militaire – des attaques du Hamas permet aux
tenants du camp dit « pro-palestinien » de passer tactiquement sous silence
les crimes de masse contre des populations civiles. A contrario, le camp dit
« pro-israélien » (auquel s’identifie l’axe « républicain » qui
va du PS au RN en passant par LREM), « oubliant » la dimension militaire,
n’évoque que les massacres de civils – permettant ainsi de justifier
d’autres massacres de civils, avec les bombardements actuels sur la bande
de Gaza (qui ont déjà coûté la vie à plus de 8000 personnes).

À l’instar du ministre israélien de la Défense Yoav GALANT qualifiant
les Gazaouis d’« animaux », il n’y a plus aucune distinction opérée
entre civils et militaires : chaque peuple est considéré comme une grande
masse indistincte tout juste bonne à être massacrée et les deux « camps
» en présence participent à ce bal sanglant, nous sommant de choisir notre
barbarie. Au-delà de l’émotion générée par ces attaques, l’impasse
politique dans laquelle les dirigeants israéliens ont enfermé la population
de Gaza depuis trois décennies rendait pourtant inévitable tôt ou tard une
tentative de solution « militaire ».

En effet, Israël, tout en cherchant à briser son relatif isolement
diplomatique et économique en négociant des traités de paix avec les
États arabes voisins, laissait les Gazaouis croupir dans un territoire
pauvre, surpeuplé et régulièrement bombardé par Tsahal – le tout sans
aucune perspective de développement. Cette tentative de faire la paix avec
les États arabes (soutiens traditionnels – quoique très hypocrites – de
la cause palestinienne) tout en laissant en suspens la question palestinienne
revêtait un caractère illusoire qui s’est révélé de manière sanglante
à la société israélienne lors des attaques du 7 octobre 2023.

Mais, si la Bande de Gaza est une prison, le Hamas est son geôlier appointé
par Israël. En effet, afin d’endiguer les mouvements – laïcs – de
la résistance palestinienne (Fatah, FPLP) en vogue à l’époque, les
services de sécurité israéliens n’ont aucunement hésité à appuyer le
développement du Hamas en Palestine en n’autorisant que l’ouverture de
mosquées liées aux Frères musulmans (faisant doubler leur nombre entre 1967
et 1986) et en leur déléguant de fait les tâches habituellement dévolues au
Welfare State. Le pari israélien n’a qu’en partie réussi : il s’avère
à l’usage que l’islamisme se marie très bien avec le nationalisme mais,
à défaut de supprimer la cause palestinienne, la manœuvre israélienne a au
moins permis de l’« islamiser ». Depuis trente ans, le Hamas se révèle
l’allié objectif de la frange la plus réactionnaire du mouvement sioniste et
garantit, par ses nombreux attentats visant la population civile, qu’aucune
fraternisation entre prolétaires juifs et arabes ne sera possible. Cette
stratégie avait été explicitée par le chef du renseignement militaire
israélien Amos YADLIN en juin 2007 : « Israël serait heureux si le Hamas
s’emparait de Gaza parce que l’armée pourrait alors traiter Gaza comme
un État hostile. » Dénoncée par la jeunesse gazaouie dans un Manifeste de
2010 comme « une organisation tentaculaire qui s’est étendue à travers la
société, tel un cancer malveillant déterminé à détruire dans sa propagation
jusqu’à la dernière cellule vivante », le mouvement frériste palestinien
impose un conservatisme social brutal à toute une société et supprime toute
opposition en faisant régner la terreur.

Parallèlement, dans une société où la jeunesse israélienne se détachait de
plus en plus des valeurs militaristes et nationalistes, les exactions du Hamas
qui a opportunément ciblé cette même jeunesse et attaqué les endroits qui
étaient les plus hostiles à l’actuel gouvernement (le kibboutz de Beer’i,
une rave party pour la paix, etc.) sont venues ressouder la société autour
de ses piliers historiques : l’armée, la guerre, le racisme anti-arabe. 

Car, au-delà des éléments de langage distillés par les propagandistes zélés du
sionisme (« Faire fleurir le désert », « Une terre sans peuple pour un peuple
sans terre »), Israël est un État « bourgeois, militariste et rabbinique »
doublé d’une « économie artificielle » (Internationale Situationniste,
1967) totalement dépendant – pour sa survie – de l’aide diplomatique,
économique et militaire états-unienne.

Ce qui était initialement une utopie socialiste, notamment avec le mouvement
des kibboutz (villages collectivistes) a débouché sur un nettoyage ethnique
lors de la guerre israélo-arabe et de la création d’Israël en 1948,
pour devenir un enfer néolibéral et inégalitaire. Les conditions de la
création de l’État d’Israël n’ont cependant rien d’exceptionnel :
ce qui l’a particulièrement rendu possible, c’est le cadre moderne du
nationalisme inscrit dans un État-nation. 

Ce dernier implique nécessairement
que se superposent une terre, un peuple / une nation et une langue, voire –
en bonus – une religion. L’État turc moderne, par exemple, s’est bâti
sur le génocide de la population arménienne. 

La partition de l’Inde en
1947 s’est faite sur des bases ethno-religieuses qui ont donné lieu à des
transferts massifs de population et des massacres de grande ampleur (environ un
million de morts). D’autres pays, lors de leur création ou ultérieurement,
ont été livrés à des guerres entre gangs rivaux – prétendant agir au
nom de telle ou telle bannière identitaire et eux-mêmes instrumentalisés
par des impérialismes concurrents – pour le contrôle du pouvoir et des
ressources socio ! économiques : Rwanda (génocide des Tutsis en 1994),
Soudan du Sud (guerre civile de 2013 à 2020). 

La Yougoslavie – démantelée
par les puissances impérialistes au nom du « droit des peuples à disposer
d’eux-mêmes » – a cédé la place à une myriade d’États eux-mêmes
en proie à des micro-nationalismes : ainsi, la minorité kosovare, devenue
majoritaire dans « son » propre pays en 1999, discrimine une population serbe
devenue à son tour minoritaire. Le droit des peuples à disposer d’eux-mêmes
se transforme inévitablement en droit des États nouvellement créés à
disposer comme ils l’entendent de leurs minorités. 

À la lueur de tous ces
exemples, comment croire que la « solution à deux États » en Palestine
telle que prônée par la « communauté internationale » pourrait aboutir
à un règlement juste et durable du conflit ?  Dans un monde où règnent
les impérialismes rivaux, où les richesses font l’objet de conflits,
où les notions de « minorité » et de « majorité » conservent (hélas)
leur fonction opératoire, comment penser qu’une simple partition de la
Palestine sur des bases ethno-religieuses puisse être autre chose qu’une
étape supplémentaire dans la perpétuation du conflit ? 

Israël se prétendant
« État juif et démocratique », la conservation de son caractère « juif
» ne peut être garantie qu’en maintenant la population arabe dans un état
de minorité et en déversant le « trop-plein » chez ses voisins arabes. Et,
dans ces conditions, que deviendrait, de son côté, la minorité juive dans
l’État palestinien nouvellement créé ?

Quant à la solution alternative dite « à un État », celui de tous ses
citoyens, de quel État s’agirait-il ? D’un Israël élargi où tout
ou partie des Palestiniens des anciens territoires occupés seraient priés
d’aller s’exiler ailleurs pour ne pas remettre en cause la majorité juive
de l’État, tandis que les Arabes seraient maintenus au bas de l’échelle
sociale ? 

D’un État « arabe » qui réduirait les Juifs à l’état de
minorité et ne leur offrirait aucune garantie contre l’antisémitisme
? 

Comment imaginer que les deux peuples pourraient, sans une révolution
radicale, oublier du jour au lendemain tous leurs clivages et toutes leurs
haines et s’unir dans un État commun ? Dans sa sobre réalité, le nouvel
État ne serait, à l’instar d’Israël avant lui, « rien d’autre
qu’une vulgaire société de classes, où [se reconstitueraient] toutes les
anomalies des vieilles sociétés » avec ses « divisions hiérarchiques »,
son racisme et ses « oppositions ethniques » (Internationale Situationniste,
1967). 
La solution dite « réaliste » n’est donc qu’une utopie. La tâche
des révolutionnaires n’est pas de proposer des solutions pour mieux gérer
ce monde tel qu’il est, mais de le détruire.

Pour nous, le monde se divise en classes et non pas en clans ou en camps : nous
refusons de diaboliser des peuples et des pays et ne faisons allégeance à
aucun nationalisme sous quelque forme que ce soit. C’est sur la base de leur
condition sociale que les populations doivent s’unir et non pas en fonction
de leur identité. Ce vieux monde doit disparaître et il ne saurait être
question pour nous d’entretenir ces vieilleries mortifères que sont les
nations, les ethnies, les races et les religions. 

Il faut démanteler le cadre
de l’État-nation, avec ses minorités et ses majorités, pour lui substituer
partout des communes, des conseils, des soviets librement fédérés. 

L’État n’est pas la solution au problème du racisme et de l’antisémitisme : il
est le problème. 

Nationalisme arabe et panislamisme, d’un côté, sionisme,
de l’autre, sont les deux faces d’une même médaille. Leur affrontement
spectaculaire se fait sur le dos du prolétariat.

Que celui-ci se lève enfin et ils tomberont à terre aussitôt ! Le prolétariat
n’a pas de patrie ! Au Proche-Orient comme partout ailleurs, ni un ni deux
ni trois : zéro État ! Établissons-le communisme mondial et sans frontières !
