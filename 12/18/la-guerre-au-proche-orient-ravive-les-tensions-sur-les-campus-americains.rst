

=====================
2023-12-18
=====================

En ne parvenant pas à condamner une situation hypothétique où un étudiant
appellerait au génocide des juifs, les présidentes d’Harvard, de Penn
et du MIT, sont tombées dans le piège tendu par les élus conservateurs,
qui dénoncent depuis des années la "cancel culture" des campus. Mais
leur manque de clarté a aussi soulevé de nombreuses critiques à gauche.

New York (États-Unis).– Pendant plusieurs jours outre-Atlantique, trois
femmes, trois présidentes d’université parmi les institutions les plus
prestigieuses de la côte Est des États-Unis, ont fait les gros titres. Dans
un rare consensus, parmi les membres de la classe politique, elles ont fait
l’unanimité contre elles.

Dans la presse, il n’était plus question que de leurs auditions «
catastrophiques" au Congrès, devant la Chambre des représentants le 5
décembre dernier, du"backlash" (retour de bâton), de la "tempête de
feu" causés par les propos sur l’antisémitisme qu’elles sont toutes
les trois accusées d’avoir tolérés sur leurs campus depuis l’attaque
du Hamas le 7 octobre.

Les questions que les élus de la Chambre des représentants (à majorité
conservatrice) avaient à leur poser n’étaient pourtant pas, en apparence, des
questions pièges. Les règlements intérieurs d’Harvard, du MIT (Massachusetts
Institute of Technology) et de l’université de Pennsylvanie ("Penn")
autorisent-ils les appels au génocide du peuple juif ? a questionné Elise
Stefanik, une élue de New York, ancienne d’Harvard et soutien de Donald Trump.

Claudine Gay, présidente de l'université Harvard, Liz Magill, présidente
de l'université de Pennsylvanie, Pamela Nadell, professeure d'histoire et
d'études juives à l'université américaine, et Sally Kornbluth, présidente
du Massachusetts

Plusieurs incidents − des chants à la gloire de l’Intifada notamment, ou des
slogans appelant à un territoire palestinien qui s’étendrait depuis les rives
du Jourdain à la mer Méditerranée (From the River to the Sea) – ont créé
la polémique ces dernières semaines. Ces faits vus et entendus sur les campus
pouvant être considérés comme des appels à l’anéantissement d’Israël.

Contrairement à la France, où depuis 1972 des délits ont été spécifiquement
créés pour lutter contre la provocation à la haine, le 1er amendement de la
Constitution américaine sacralise aux États-Unis la liberté d’expression,
y compris les appels à la violence. D’un point de vue strictement légal,
les réponses des présidentes d’universités – à savoir que le discours
seul n’est pas condamnable – étaient donc conformes aux textes de loi.

Alors pourquoi une telle controverse ? Qu’est-ce qui a conduit les gros
donateurs à vouloir retirer des dizaines de millions de dollars de dons à ces
universités ? Qu’est-ce qui a amené les médias, y compris ceux situés
plutôt à gauche, à l’image du New York Times, à vilipender à travers
plusieurs tribunes "l’hypocrisie" ou encore le "double standard"
de ces présidentes ? Qu’est-ce qui, enfin, a poussé l’une d’elles,
Elizabeth Magill, jusqu’ici à la tête de l’université de Pennsylvanie,
à la démission ?

La réponse réside, en partie, dans le court mea culpa publié par Elizabeth
Magill à l’issue des auditions. "Je n’étais pas concentrée sur le
fait irréfutable, mais j’aurais dû l’être, qu’un appel au génocide
du peuple juif est un appel à certaines des violences les plus terribles que
les êtres humains puissent perpétrer", a-t-elle regretté. Autrement dit,
la question qui leur était posée était aussi une question morale.

La faute à la "cancel culture" Pour la droite américaine, en revanche,
les raisons de ce "manque de concentration" seraient tout autres :
tout serait de la faute à la cancel culture ("culture de l’effacement
»), présumée dangereuse, et aux dérives du "wokisme" au sein de ces
institutions progressistes. Sur les 171 villes identifiées aux États-Unis
comme villes universitaires, deux sur trois votent dorénavant à gauche.

Une nouvelle génération d’étudiants semble favoriser la mise en place
de restrictions de la liberté d’expression − ce que font déjà les
universités privées. 55 % des étudiants, selon les derniers sondages,
estiment que des orateurs ayant tenu des propos offensants ne devraient pas
être autorisés à être invités sur leur campus. En particulier des orateurs
ayant des vues conservatrices.

Le géophysicien Dorian Abbot, par exemple, a été canceled (banni) au MIT
pour avoir critiqué la discrimination positive. Carole Hooven, une biologiste,
a été mise à l’écart de Harvard, en raison de ses opinions sur ce qu’elle
considère être les deux sexes biologiques. Amy Wax, une professeure de Penn,
a, elle, été sanctionnée après des propos sur les résultats d’étudiants
non blancs.

D’où l’accusation d’"hypocrisie" et de "double standard". Alors
que certaines minorités ont été protégées de certaines vues conservatrices,
les juifs ne bénéficient pas d’une égalité de traitement. C’est en
tout cas ce que le conflit entre Israël et le Hamas est venu révéler,
pour ces détracteurs.

"Un grand nombre de groupes à gauche qui auraient pu être offensés par
certains types de discours [sur les questions raciales ou de genre – ndlr]
souhaitent désormais avoir une liberté d’expression qui permette de
soutenir la cause palestinienne", explique à Mediapart Jeffrey Kidder,
professeur de sociologie à l’université du Northern Illinois.

Seulement, considérer la gauche "comme un bloc" uniforme serait pour ce
professeur une erreur. Il existe notamment à gauche un mouvement non sioniste,
juif, très actif, qui milite en faveur de la cause palestinienne.

"Pour les juifs non sionistes, la question est de protéger les droits
d’une minorité [les Palestiniens – ndlr] contre un État déterminé à
l’éliminer […] Qu’y a-t-il de plus juif que ça ?", a par exemple
déclaré au New York Times Eva Borgwardt, 27 ans, la directrice d’IfNotNow,
un groupe de jeunes juifs non sionistes appelant à un cessez-le-feu.

Pour Vivian Bradford, professeur de communication à l’université Penn,
"il y a maintenant un mouvement" à droite, aux tactiques proches du
"maccarthysme […], qui tente de faire passer les universités pour des
endroits terribles. Comme s’il n’y avait pas de liberté d’expression
ni de diversité intellectuelle", explique-t-il.

Si vous regardez ce qui se passe dans les législatures des États contrôlés
par les Républicains à travers le pays, vous verrez une augmentation historique
de la censure dans les universités.

Ce type d’"incidents" - des professeurs ou des invités canceled - «
sont statistiquement peu signifiants" poursuit Bradford, mais rencontrent
une immense "résonance sur les réseaux sociaux" et "dans les médias
mainstream". "L’anecdote devient alors le récit dominant" quand bien
même "des milliers de cours sont suivis chaque jour sans incident".

"Lorsque nous faisions nos entretiens sous l’administration Trump, remarque
le professeur Kidder, de nombreux groupes conservateurs faisaient venir des
conférenciers sur le campus, en grande partie dans le but de provoquer les
étudiants progressistes [dits wokes – ndlr]. Ils faisaient venir des gens
comme [l’extrémiste – ndlr] Milo Yiannopoulos ou Charles Murray",
dénoncé pour ses thèses racistes.

La séquence sur les présidentes d’université taillée pour créer le
buzz s’inscrit par conséquent pour les deux professeurs dans cette même
stratégie de provocation. La controverse offrant au passage l’occasion
d’occulter l’antisémitisme qui sévit dans les propres rangs républicains.

Un antisémitisme encouragé en particulier par le champion du parti, Donald
Trump, candidat à sa réélection en 2024 et adepte de la théorie du grand
remplacement – aux États-Unis, selon cette théorie, les juifs seraient en
train de tirer les fils de ce grand remplacement.

Or, "si vous regardez ce qui se passe dans les législatures des États
contrôlés par les Républicains à travers le pays, vous verrez […] une
augmentation historique de la censure dans les universités" et dans les
écoles, en Floride en particulier, rappelle Vivian Bradford.

Après plusieurs jours de tensions, Claudine Gay, présidente d’Harvard,
et Sally Kornbluth, présidente du MIT, elle-même de confession juive,
ont été maintenues à leurs postes malgré la pression sur leurs conseils
d’administration, en particulier des donateurs milliardaires, parmi lesquels
des managers de hedge funds à Wall Street, tels que William Ackman pour Harvard.

La question de l’influence de ces financiers, et plus largement de l’argent,
s’est également posée ces derniers jours. À Penn, le milliardaire Mark
Rowan (50 millions de dons) a demandé et obtenu la tête de la présidente,
Elizabeth Magill.

Sur les campus, 73 % des étudiants juifs déclarent quant à eux avoir subi ou
avoir été témoins d’incidents antisémites cette année. 

Avant l’attaque du Hamas, environ 67 % d’entre eux déclaraient se sentir "très" ou
"extrêmement" en sécurité sur le campus. Depuis l’attaque, moins
de la moitié (46 %) partagent ce sentiment. Les réponses des présidentes
d’université lors des auditions ne permettront pas de les rassurer.
