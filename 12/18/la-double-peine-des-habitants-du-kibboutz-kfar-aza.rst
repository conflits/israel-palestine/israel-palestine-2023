

.. _minisini_2023_12_18:

===============================================================================================
2023-12-18 En Israël, la double peine des habitants du kibboutz Kfar Aza par Lucas Minisini
===============================================================================================

- https://www.lemonde.fr/international/article/2023/12/18/la-double-peine-des-habitants-du-kibboutz-kfar-aza_6206475_3210.html

En Israël, la double peine des habitants du kibboutz Kfar Aza

Par Lucas Minisini (Shefayim (Israël), envoyé spécial)

REPORTAGE
===========

La communauté a été particulièrement frappée par l’attaque du Hamas
le 7 octobre et deux de ses membres, otages à Gaza, viennent d’être tués
par des soldats israéliens. Les survivants, accueillis dans un kibboutz près
de Tel-Aviv, s’interrogent sur leur avenir.  La tragédie ne semble vouloir
laisser aucun répit au kibboutz Kfar Aza. Dimanche 17 décembre, la communauté,
dont plus de 60 membres ont été tués dans l’attaque terroriste du Hamas le
7 octobre, assiste une nouvelle fois à des funérailles. Dans l’après-midi,
une foule se presse en silence pour l’enterrement d’Alon Shamriz. Le jeune
homme de 26 ans, pris en otage comme 19 autres habitants de cette localité
située à quelques centaines de mètres de la bande de Gaza, a été tué par
l’armée israélienne, vendredi 15 décembre, dans le cadre des opérations
militaires que mène l’Etat hébreu dans l’enclave, en même temps que
Yotam Haim, un autre habitant de Kfar Aza, et Fouad Talalka. Les trois jeunes
captifs, torse nu et un drapeau blanc à la main, ont été considérés comme
une « menace » par les soldats.

L’enterrement d’Alon Shamriz, tué accidentellement par l’armée
israélienne alors qu’il était otage à Gaza. Dans le cimetière du kibboutz
Shefayim (Israël), le 17 décembre 2023.

Ces derniers sont aujourd’hui accusés de ne pas avoir respecté « les
règles d’engagement » militaire, selon leur hiérarchie. Vendredi, Avi,
le père d’Alon, avait estimé, dans un entretien à la radio de l’armée,
qu’il ne s’agissait pas « d’une erreur, mais d’une exécution. Même
s’il s’était agi d’un terroriste, pourquoi lui tirer dessus comme ça
? C’est contre les règles apprises à l’armée ».

« Ceux qui t’ont abandonné [allusion aux défaillances de l’armée lors de
l’attaque du 7 octobre] sont aussi ceux qui t’ont assassiné », déclare
Ido, le frère d’Alon, en larmes face aux centaines de personnes présentes,
dont Nir Barkat, le ministre de l’économie, et l’ancien chef d’état-major
de l’armée Dan Haloutz. En plus du choc, les habitants de Kfar Aza venus
partager cette « tristesse sans fin », comme la résume Dikla, la mère du
défunt, sont contraints de rendre hommage au jeune étudiant à plus de cent
kilomètres au nord de chez eux. Comme vingt autres membres de la communauté
avant lui, Alon Shamriz est enterré dans le cimetière du kibboutz Shefayim.

Cette riche communauté du nord de Tel-Aviv accueille aujourd’hui environ
700 des 900 survivants de Kfar Aza. Au lendemain du 7 octobre, en plein chaos,
ils s’étaient éparpillés dans le pays avant de converger vers ce lieu,
à quelques dizaines de mètres de la mer et loin des roquettes du Hamas. «
Même réfugiés, nous devions être dans un kibboutz, précise Ofer Winner,
dont le fils Yahav, un jeune réalisateur prenant Kfar Aza comme décor de
cinéma, a été tué par le Hamas. C’est le seul endroit où nous pouvons
imaginer aller mieux. »

« Deux kibboutz en un » La plupart des habitants frontaliers de Gaza se
sont installés dans le seul hôtel de la localité, un ancien centre de
conférences séparé en plusieurs bâtiments, avec 140 chambres doubles,
des balcons étroits et une moquette sombre. Sans avoir jamais mis les pieds
dans cette commune auparavant, les réfugiés du sud d’Israël partagent les
valeurs de solidarité du mouvement collectiviste des kibboutz. Grâce à ces
références communes, les familles de chaque communauté se sont « adoptées
mutuellement » pour travailler et vivre ensemble, raconte Lior Edod, le «
secrétaire » de Shefayim, équivalent du maire. Le quadragénaire sourit
: « Aujourd’hui, nous avons deux kibboutz en un. » Pour le remercier,
plusieurs familles de survivants ont installé une bannière, à l’entrée
de la ville, où s’affiche en grandes lettres : « Kfar Aza remercie Shefayim
pour son hospitalité. »

Le centre communautaire sert aujourd’hui de lieu de vie aux survivants de
Kfar Aza, au kibboutz Shefayim (Israël), le 17 décembre 2023.

Le centre des congrès du kibboutz transformé en maternelle. Les salles de
classe ont été renommées comme celles de Kfar Aza, au kibboutz Shefayim
(Israël), le 17 décembre 2023.  Le centre des congrès du kibboutz transformé
en maternelle. Les salles de classe ont été renommées comme celles de Kfar
Aza, au kibboutz Shefayim (Israël), le 17 décembre 2023.

Dans la commune au bord de la plage, tout est fait pour que leurs hôtes aient
l’impression de ne jamais être vraiment partis de chez eux. Les salles de
classe de l’école maternelle locale sont renommées « Soleil », « Ami »
ou « Eléphant », comme à Kfar Aza. L’atelier de réparation du kibboutz,
dont la gérante, Livnat Kutz, a été tuée avec son mari et ses trois enfants
le 7 octobre, a été reconstruit à l’identique dans un local prêté par
la commune.

Même chose pour le pub de Kfar Aza, en partie détruit pendant l’attaque du
Hamas : les décorations affichant quelques phrases comme « La vie est belle
», « La vie continue » et « Saisis l’instant » ont été récupérées
dans le bâtiment pour orner les murs du nouveau bar, installé dans le lobby de
l’hôtel. Et, depuis début décembre, l’ancien château d’eau du kibboutz
Shefayim, devenu une galerie d’art, propose une série de photographies
figurant Kfar Aza et sa frontière avec Gaza, auparavant affichées sur place.

Au cœur de Shefayim, les habitants ont aussi installé un centre de dons,
où récupérer des vêtements ou des jouets pour les enfants, et une nouvelle
cafétéria, gratuite pour les nouveaux venus. Pour accueillir plus de monde,
les dirigeants du kibboutz ont lancé la construction d’une dizaine de
« karavilla », mélange des mots hébreux « caravane » et « villa »
désignant des mobile homes faciles à installer, semblables à ceux que l’on
retrouve dans les colonies juives illégales de Cisjordanie.

Où reconstruire ?  Depuis le 7 octobre, Lior Edod dit avoir dépensé 80
millions de shekels (environ 20 millions d’euros) dans l’accueil de ces
réfugiés. Malgré des promesses de remboursement de tous ces frais, Shefayim
n’a encore « rien reçu » de l’Etat, affirme-t-il. Contactée, la Takuma,
l’agence gouvernementale chargée de la réhabilitation des territoires
attaqués par le Hamas, assure seulement, dans un communiqué, qu’elle procède
aux « ajustements nécessaires » pour un meilleur accueil des déplacés.

Malgré tous les efforts déployés, plusieurs dizaines de survivants de Kfar Aza
n’ont pas pu rejoindre le reste du groupe à Shefayim. Ils sont aujourd’hui
répartis entre deux hôtels de Tel-Aviv ou quelques maisons prêtées par des
propriétaires de la région. Puisque la reconstruction de Kfar Aza pourrait
prendre plusieurs années et que l’hôtel de Shefayim manque de place, toute
la communauté soutient la construction d’habitations temporaires. Mais
personne ne s’accorde sur le lieu : rester au centre d’Israël, loin des
roquettes, ou repartir dans le Sud, à Ruhama, un kibboutz voisin de Kfar Aza
qui a proposé de partager ses terres, malgré les risques et le traumatisme ?

Depuis vendredi, les survivants peuvent voter électroniquement pour une des
deux options. « Je ne veux pas bouger une nouvelle fois », témoigne Hanita
Baram, 62 ans, dont le fils, Aviv, a été tué le 7 octobre en tentant de
défendre Kfar Aza. La mère de famille, traumatisée par l’attaque du Hamas,
s’est résignée à repartir de zéro, sans son enfant ni sa maison. « Je
n’ai pas le choix. » La plupart des jeunes parents du kibboutz, effrayés
à l’idée d’élever des enfants à proximité de la bande de Gaza,
préfèrent cette alternative, eux aussi.

Cinq otages encore « Nous avons des discussions très intenses à ce sujet
», décrit Hannan Dan, 39 ans. Lui n’imagine pas faire sa vie ailleurs que
dans le Sud. Ce père de famille, installé à Kfar Aza en 2018 avec Eden,
sa femme, et leurs deux enfants en bas âge, se sait une exception parmi les
jeunes parents : il craint que tout le monde reste campé sur ses positions
et que cela marque la fin du kibboutz Kfar Aza, fondé en 1951, trois ans
après la création de l’Etat d’Israël. « Plusieurs amis proches m’ont
déjà dit qu’ils ne suivraient pas en cas de départ dans le Sud, assure le
trentenaire. Si la communauté disparaît, ça sera comme un second deuil pour
moi. » Lundi matin, les résultats du vote n’avaient pas été rendus public.

Hannan Dan, survivant de l’attaque du Hamas le 7 octobre sur le Kibboutz de
Kfar Aza, avec son fils aîné, dans le salon de la maison où sa famille est
hébergée non loin du kibboutz Shefayim (Israël), le 15 décembre 2023.


Certaines familles ont tout simplement décidé de ne pas participer à cette
consultation. « Comment peut-on déjà penser au futur ? », soupire Simona
Steinbrecher, volontaire au centre des donations de 63 ans. Sa vie s’est «
arrêtée » le 7 octobre : Doron, sa fille de 30 ans, a été kidnappée et
elle est toujours retenue en otage dans la bande de Gaza.

La sexagénaire tente aujourd’hui de monter une « coalition » pour demander
au gouvernement israélien une seconde trêve dans le conflit et de nouvelles
négociations pour la libération des otages. Elle manifeste régulièrement
à Tel-Aviv et parle « constamment » à d’autres familles sans nouvelles
de leurs proches. Quatre autres membres de Kfar Aza sont toujours retenus par
le Hamas. Pour que personne ne les oublie, sous des photographies accrochées
au plafond, leurs noms sont affichés sur un large panneau dans le hall de
l’hôtel de Shefayim.

Lucas Minisini (Shefayim (Israël), envoyé spécial)
