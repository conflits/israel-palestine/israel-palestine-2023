
.. _wieviorka_2023_12_15:

==============================================================================
2023-12-15 **Guerre Hamas-Israël : Camus, reviens ! par Michel Wieviorka**
==============================================================================

- https://www.liberation.fr/idees-et-debats/tribunes/guerre-hamas-israel-camus-reviens-par-michel-wieviorka-20231215_NQEAEMRAIFGNVIFPUDA4D4S5UQ/
- https://www.liberation.fr/culture/2010/01/02/camus-cet-etrange-ami_602169/


Auteur Michel Wieviorka, Sociologue
========================================

- Michel Wieviorka, Sociologue

Guerre Hamas-Israël : Camus, reviens ! par Michel Wieviorka
===============================================================

En référence à la guerre d’Algérie, Albert Camus se demandait comment
mettre fin aux «noces sanglantes du terrorisme et de la répression" ? Comment
renverser l’équation impossible qui oblige à choisir un camp ou l’autre
? Comment repenser l’un et l’autre ensemble, dans un même projet de
résolution des violences ? s’interroge le sociologue Michel Wieviorka.

Ma mère, avant la justice, disait Camus. 

Ni assimilation ni indépendance, disait-il aussi alors que la guerre 
d’Algérie faisait rage. 

Comment ne pas penser à lui aujourd’hui ? 

Non pas tant, ou pas seulement, en référence à l’Algérie actuelle et à 
ses relations avec la France. 
Mais parce que ce qui se joue actuellement au Proche-Orient mérite d’en appeler à
son mode de pensée. Cela vaut mieux que de se vautrer dans les polémiques
enflammées, les radicalisations outrancières, bien peu ou mal informées,
et les comparaisons douteuses – par exemple entre le génocide des Héréros
en 1904 et l’action d’Israël à Gaza.

Israël, une démocratie malade
=================================

D’un côté, Israël. Une démocratie malade, une société tolérant
d’être dirigée par un pouvoir corrompu conjuguant pour se maintenir
nationalisme débridé et impulsions théocratiques. Benyamin Nétanyahou
n’est pas un dictateur, il a été élu, sa politique repose sur des
soutiens populaires. Elle s’est profilée au lendemain des accords d’Oslo
: si Benyamin Nétanyahou n’a pas armé Yigal Amir, l’assassin israélien
ultranationaliste de Yitzhak Rabin en 1995, il a orchestré la campagne rendant
son geste désirable ou acceptable par une partie de la population.

Il est responsable des carences de son gouvernement en matière de sécurité ;
il fait tout pour miner depuis longtemps la démocratie de l’intérieur, par
exemple en tentant de brider la Cour suprême, principal rempart institutionnel
de l’Etat de droit. En Cisjordanie, il persévère dans sa stratégie de
colonisation meurtrière qui rend chaque jour plus difficile la création
d’un Etat palestinien.

La politique d’Israël a contribué aux tragédies d’aujourd’hui. La
société israélienne l’a acceptée, sinon voulue, faisant preuve
d’une insouciance arrogante, confiante en son avance technologique et
militaire. Comment ne pas comprendre la rage et la haine de ceux qui, derrière
un mur ou des grillages, vivent dans des conditions indignes de pauvreté et de
confinement ? Le ghetto qu’est Gaza contraste avec ce qu’ils ont sous leurs
yeux : une population israélienne libre de se déplacer et vivant aux standards
économiques et culturels de l’Occident.  Une nation palestinienne sans Etat

De l’autre côté, une nation palestinienne sans Etat, aux dirigeants souvent
corrompus, les uns velléitaires et dépassés, l’Autorité palestinienne,
les autres radicalisés et militarisés, le Hamas. L’attaque du 7 octobre
dernier a eu entre autres effets de remettre la question palestinienne au
cœur de l’actualité, alors que les pouvoirs arabo-musulmans de la région
l’ignoraient ou la bafouaient, et que les opinions publiques occidentales
s’en désintéressaient.

Elle a aussi entraîné l’horreur en déclenchant cyniquement, car hautement
prévisible, une réponse israélienne brutale. Toujours est-il que les
Palestiniens, dans la mesure où ils n’ont pas su se laisser incarner par
d’autres élites que celles qui mettent en œuvre, la violence à leurs
dépens, laissent s’exprimer un désir effréné de rejeter les Juifs à la
mer, et de détruire Israël.

Dès lors, la tentation d’un positionnement sans nuance apparaît de
toutes parts, et dans le monde entier. En France, les uns expriment un
soutien inconditionnel à Israël et, dans le contexte présent, à son
armée. Toute autre position est pour eux non seulement antisioniste, mais
aussi antisémite. Ils oublient qu’il est possible d’être l’un sans
l’autre, et imputent aux Palestiniens la responsabilité de leur malheur,
coupables qu’ils seraient, expliquent-ils, d’avoir refusé les efforts
d’Israël pour aboutir à la paix. Les autres minimisent le caractère criminel
de l’attaque du 7 octobre, parfois même la légitiment en faisant du Hamas
le champion de la résistance et du combat pour la justice et la liberté. Les
extrêmes ici frisent le négationnisme.

Dans chaque camp, les arguments contre l’autre ne manquent pas, mais pas
au service de la paix, sous la forme, éventuellement, d’une reconstitution
des commencements à prétention historique, pour la légitimité première,
ou au contraire la culpabilité fondatrice des uns ou des autres.  Un appel
à la trêve civile

En attendant, la rigueur intellectuelle et morale suggère de refuser les
outrances simplificatrices et radicales et de soutenir tout ce qui va dans
le sens d’une solution acceptable de part et d’autre. Cet horizon est
éloigné, et la situation est dès lors singulièrement difficile, comme
l’était celle de Camus en son temps. Comment n’être ni avec Jean-Luc
Mélenchon ni avec Alain Finkielkraut ? Ni pour la destruction de l’Etat
d’Israël, et la haine antisémite, sous couvert d’antisionisme, ni pour
les horreurs que vivent quotidiennement les Palestiniens à Gaza, sans oublier
l’injustice qui les prive d’un Etat ? Comment, pour parler comme Camus
dans un appel à la trêve civile (le 22 janvier 1956), mettre fin aux «noces
sanglantes du terrorisme et de la répression" ?

Albert Camus avait choisi ensuite le silence, purement et simplement,
pendant presque deux ans : "J’ai décidé de me taire en ce qui concerne
l’Algérie, afin de n’ajouter ni à son malheur ni aux bêtises qu’on
écrit à son propos" (lettre à Jean Sénac, 10 février 1957).

Car comment renverser l’équation impossible qui oblige à choisir un camp
ou l’autre, pour passer à une autre équation permettant de vouloir et
penser l’un et l’autre ensemble, dans un même projet de résolution des
violences ? En soutenant les efforts pour aller vers la paix, voire l’esquisser
concrètement par des échanges, ou dans des expériences concrètes suggérant
la possibilité de vivre ensemble ? Oui, bien sûr.

Mais les initiatives actuelles sont loin de laisser entrevoir un processus
de résolution juste, pacifique et durable du conflit, tant sont difficiles
à remplir les conditions qu’elle exige : changements politiques majeurs en
Israël ; émergence d’un leadership palestinien ouvert à la négociation
; pressions internationales efficaces pour aller dans ce sens, y compris
discrètement, voire secrètement… En attendant, comment, loin du terrain
des violences, ne pas avoir l’honnêteté sinon de se taire, du moins de
témoigner des déchirements qu’impose la situation ?
