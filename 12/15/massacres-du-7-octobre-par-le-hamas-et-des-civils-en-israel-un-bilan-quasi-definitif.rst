
.. _times_israel_2023_12_15:

======================================================================================================
2023-12-15 Massacres du 7 octobre en Israël par le Hamas et des civils : un bilan quasi définitif
======================================================================================================

- https://fr.timesofisrael.com/massacres-du-7-octobre-par-le-hamas-et-des-civils-en-israel-un-bilan-quasi-definitif/

Massacres du 7 octobre en Israël par le Hamas et des civils : un bilan quasi
définitif

Plus jeune victime du Hamas : la petite Mila Cohen, 10 mois, a été assassinée
par balles au kibboutz Beeri, où elle vivait

Un total de 1 139 morts incluant quelque 700 civils israéliens parmi lesquels
36 enfants et environ 70 étrangers : le bilan quasi définitif des attaques
du 7 octobre en Israël permet d’établir une radioscopie plus précise des
massacres et remet en cause certains témoignages initiaux.

En ce Shabbat marquant le dernier jour des fêtes juives de Souccot,
des centaines d’hommes armés du groupe terroriste palestinien du Hamas
s’infiltrent au petit matin dans le sud d’Israël à partir de la bande de
Gaza. Ils tuent à l’aveugle dans la rue, dans les maisons de plusieurs villes
et kibboutz (communautés rurales) ou en pleine rave-party. Il faudra plus de
trois jours de violents combats à l’armée israélienne pour reprendre le
contrôle des zones attaquées.

Le pays est frappé d’effroi par cette attaque contre des civils d’une
ampleur et d’une violence jamais vues depuis la création de l’État
d’Israël en 1948, et accompagnée de violences sexuelles dont une enquête
policière tente aujourd’hui d’évaluer toute la mesure.

Le 14 octobre, les autorités annoncent un bilan de « plus de 1 400 personnes
(…) tuées (…) par les terroristes du Hamas ». Le 10 novembre, le
ministère des Affaires étrangères publie une « estimation actualisée
» selon laquelle ces derniers « ont assassiné de sang-froid environ 1 200
personnes » en Israël, sans plus de détails.

De nombreux corps ayant été brûlés, dont plusieurs familles entières dans
leur maison, ou affreusement mutilés, il a fallu des semaines aux médecins
légistes pour tous les identifier. Au 13 décembre, il reste encore cinq
personnes portées disparues, parmi lesquelles quatre Israéliens, selon le
Bureau du Premier ministre (PMO).

Pour connaître l’identité et l’âge des victimes civiles, il faut
se plonger dans les données du Bitouah Léoumi, la Sécurité sociale
israélienne. Sur son site internet figure la liste de 695 civils israéliens
tués ce jour-là et dans les jours qui suivent, avec leur identité et les
circonstances de leur mort. Parmi eux, 36 mineurs, dont 20 de moins de 15 ans,
et 10 tués par des roquettes.

A LIRE – Yona, Ohad et Mila Cohen, 73 ans, 43 ans et 10 mois : trois
générations fauchées

Plus jeune victime du Hamas, la petite Mila Cohen, 10 mois, a été assassinée
par balles au kibboutz Beeri, où elle vivait. Trois autres enfants d’une
même famille, âgés de deux à six ans, ont été assassinés avec leurs
parents dans leur maison du kibboutz Nir Oz, deux autres frères de cinq et huit
ans et leurs parents ont été abattus dans leur voiture sur une route. Et un
autre garçon de cinq ans a été tué dans la rue par une roquette. Tous les
autres enfants tués ont plus de neuf ans, selon les données du Bitouah Léoumi.

Le festival Nova, 364 morts

Pour qui en douterait encore, les données de la Sécurité sociale israélienne
attestent des atrocités commises par le Hamas, et documentées par de nombreux
témoignages. Un nombre retient particulièrement l’attention : celui des
364 personnes abattues dans le massacre au festival de musique Nova, à Réïm.

Mais les statistiques invalident aussi définitivement certaines informations
relayées par les autorités israéliennes dans les jours suivant le 7 octobre.

Dans les jours ayant suivi le début de la guerre, un message publié sur le
compte X officiel de l’État d’Israël était devenu viral. Il évoquait
« 40 bébés assassinés » au kibboutz Kfar Aza, sur la base du reportage
d’une journaliste de la chaîne i24NEWS citant des soldats sur place.

Interrogé par l’AFP au lendemain de sa publication, le ministère des
Affaires étrangères israélien, responsable du compte, avait affirmé ne
pouvoir « confirmer aucun chiffre à ce stade ».

Selon le Bitouah Léoumi, 46 civils ont été tués à Kfar Aza. Le plus jeune
avait 14 ans.

Autre témoignage remis en cause, celui livré le 27 octobre par le colonel
Golan Vach, chef de l’unité de recherche et de secours de l’armée,
qui avait affirmé à un groupe de journalistes, dont un de l’AFP, avoir «
personnellement » transporté « un bébé décapité » retrouvé à Beeri,
selon lui, dans les bras de sa mère assassinée.

Selon le Bitah Léoumi, un seul bébé a été tué à Beeri : la petite Mila
Cohen, avec son père et sa grand-mère. Sa mère a miraculeusement réchappé
à la tuerie. Interrogés par l’AFP sur les déclarations du colonel Vach,
deux porte-parole de l’armée israélienne ont opposé une fin de non-recevoir

Neuf autres mineurs ont péri à Beeri, mais Yossi Landau, un volontaire
de l’ONG Zaka qui aide notamment à la collecte des corps des victimes
d’attentats ou d’accidents, avait déclaré à l’AFP le 11 octobre y avoir
vu « 20 enfants (…) les mains attachées dans le dos, abattus et brûlés ».

« Il y a des écarts entre la réalité et les témoignages de nos bénévoles
sur place sur ce qu’ils ont cru voir, pour plusieurs raisons », explique
à l’AFP Haïm Otmazgin, un des responsables de Zaka.

« Quand on découvre des corps brûlés ou en état de décomposition, on
peut facilement se tromper et penser qu’un corps est celui d’un enfant »
car la forme du corps change dans ces conditions, ajoute M. Otmazgin. « Nos
bénévoles ont été confrontés à des scènes traumatisantes et ont parfois
mal interprété ce qu’ils ont vu », reconnaît-il.

Aux civils israéliens tués dans les attaques du Hamas s’ajoutent 71
étrangers, selon la presse israélienne, en majorité des ouvriers thaïlandais,
58 policiers, 10 membres du Shin Beth, le service de sécurité intérieure
et 305 militaires tués le 7 et les trois jours qui ont suivi.

Tel qu’il est compilé par l’armée, le nombre de pertes militaires
inclut aussi bien plusieurs dizaines de soldates non armées affectées à la
surveillance de la frontière avec la bande de Gaza, tuées dans l’assaut
initial du Hamas, des soldats d’active ou des réservistes tombés au combat,
que des membres des groupes de défense des kibboutz morts l’arme à la main
ou des permissionnaires tués chez eux ou à la rave-party de Réïm par exemple.

Plusieurs inconnues

Les données du Bitah Léoumi ne permettent pas de distinguer le nombre des
victimes du Hamas de celui des civils tués par les forces israéliennes dans
les combats lors de la reprise des kibboutz et des villes attaquées, opération
pendant laquelle l’armée a eu recours à des tirs nourris d’obus et de
roquettes sur des zones habitées pour déloger les assaillants, selon des
témoignages recueillis par l’AFP et des médias israéliens.

Autre inconnue : le nombre de terroristes palestiniens tués sur le sol
israélien. L’armée avait indiqué le 10 octobre avoir retrouvé « environ
1 500 corps » d’assaillants. Plusieurs questions adressées par l’AFP à
l’armée et au ministère de la Défense pour en savoir plus sont restées
depuis lors sans réponses.

Dans une interview à la chaîne Al-Jazeera, le numéro deux du Hamas, Saleh
al-Arouri, a assuré de son côté « qu’environ 1 200 combattants » avaient
pris part aux attaques du 7 octobre mais la presse israélienne parle plutôt
de 3 000 terroristes.

Avec quelque 250 otages, dont 110 ont été libérés depuis, et huit
récupérés par Israël à l’état de cadavres, selon les autorités
israéliennes, pour qui le Hamas détient encore 132 otages dont 19 corps de
personnes enlevées le 7 octobre.
