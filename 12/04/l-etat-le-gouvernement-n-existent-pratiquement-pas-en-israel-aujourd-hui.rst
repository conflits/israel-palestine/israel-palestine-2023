
.. _etat_israel_2023_12_05:

===================================================================================================================
2023-12-05 **L’État, le gouvernement n’existent pratiquement pas en Israël aujourd’hui** par Gwenaelle Lenoir 
===================================================================================================================

- https://www.mediapart.fr/journal/international/041223/l-etat-le-gouvernement-n-existent-pratiquement-pas-en-israel-aujourd-hui
- https://www.mediapart.fr/biographie/gwenaelle-lenoir


« L’État, le gouvernement n’existent pratiquement pas en Israël
aujourd’hui »

4 décembre 2023 | Par Gwenaelle Lenoir

Menachem Klein est professeur de sciences politiques à l’université
de Bar-Ilan en Israël. Auteur de nombreux livres sur la question
israélo-palestinienne, il a participé à partir du milieu des années
1990 et jusqu’au milieu des années 2000 à de nombreuses négociations
israélo-palestiniennes informelles. Entretien.

Menachem Klein est professeur de sciences politiques à l’université
de Bar-Ilan en Israël, il vit à Jérusalem. Il a participé à partir du
milieu des années 1990 et jusqu’au milieu des années 2000 à de nombreuses
négociations israélo-palestiniennes informelles. Il a été une des chevilles
ouvrières de l’initiative de Genève, modèle unique d’un accord de paix
global rendu public le 1er décembre 2003.

Négocié par des personnalités politiques de premier plan israéliennes et
palestiniennes hors des voies officielles, cet accord a reçu le parrainage de
nombreuses personnalités et dirigeants internationaux. Mais l’initiative
de Genève n’est finalement pas assez soutenue pour connaître le début
d’une concrétisation.

Menachem Klein est par ailleurs l’auteur de nombreux livres, dont l’un,
biographique, sur les dirigeants palestiniens Yasser Arafat et Mahmoud Abbas
(Arafat and Abbas, Portraits of Leadership in a State Postponed, 2019, Hurst,
non traduit en français).

Mediapart : Sept semaines après les attaques du Hamas perpétrées le 7 octobre
et après le début de la guerre massive lancée par le gouvernement israélien,
comment va la société israélienne ?

Menachem Klein : Jusqu’à très récemment, les Israéliens ne recevaient
guère d’informations en provenance de Gaza concernant le nombre de
civils tués et les destructions, ou ne les regardaient pas. Ce n’est que
récemment, avec le cessez-le-feu, qu’ils ont commencé à s’informer. Des
photos de soldats des forces de défense israéliennes [FDI, nom officiel de
l’armée israélienne – ndlr] qui patrouillent dans la bande de Gaza ont
également été diffusées, mais l’attention du public israélien se porte
essentiellement sur les victimes de l’attaque barbare du Hamas, qui a eu lieu
le 7 octobre, ainsi que sur le retour des otages. Très peu d’informations
sont diffusées par les grands médias, les chaînes de télévision, les
journaux, sur les victimes civiles, les souffrances et la crise humanitaire
dans la bande de Gaza.

La société israélienne est profondément traumatisée, et les gens vivent
dans la peur. Il y a beaucoup de gens dans les rues avec des armes. Des soldats
de l’armée qui sont en permission de courte durée passent de l’uniforme
militaire à la tenue civile, mais sortent avec leur arme dans la rue, dans
les cafés, les supermarchés, les synagogues le samedi. Ils ont peur qu’un
terroriste vienne les attaquer, n’importe où, au coin d’une rue, ou tire
depuis une voiture.

Il y a donc une grande peur, un traumatisme et une méfiance à l’égard
des dirigeants. Cette méfiance existait déjà avant la guerre. Elle a encore
grandi avec la guerre, avec un gouvernement dysfonctionnel qui ne fournit pas de
services à la population dans le sud d’Israël, à côté de la bande de Gaza,
ni à celle qui vit dans le Nord, près de la frontière libanaise. L’État,
le gouvernement n’existent pratiquement pas. Il n’y a donc pas de figure
paternelle. Il n’y a pas de grand-père qui s’occupe de la société
effrayée et traumatisée et qui la calme.

Vous voulez dire que sept semaines après l’attaque du Hamas, le gouvernement
ne remplit toujours pas son rôle ?

C’est incroyable de voir à quel point les services de l’État fonctionnent
mal. Ce sont les volontaires de la société civile qui fournissent des
compléments alimentaires aux soldats, leur achètent des vêtements chauds,
des chaussettes, des sous-vêtements. Ce sont des particuliers, des restaurants,
des cafés qui envoient des repas chauds aux personnes déplacées évacuées
de la zone proche de la bande de Gaza et du Nord, près de la frontière avec
le Liban, qui sont forcées de vivre dans des hôtels et dans d’autres
lieux. Il y a des groupes qui rassemblent des experts en haute technologie
pour découvrir où se trouvent les otages dans la bande de Gaza. La société
civile aide donc le gouvernement.

Détruire le Hamas, c’est irréalisable, et c’est aussi ce que pensent
les Américains.

C’est un gouvernement dysfonctionnel et Nétanyahou n’est pas digne
de confiance. Il a formé une coalition de médiocres. Pire encore, les
membres des ministères sont des activistes politiques mis en place par le
ministre. Leur seule qualification est d’être des apparatchiks politiques
loyaux au ministre. C’est le cas dans les transports, dans le tourisme. C’est
aussi le cas dans les services du premier ministre. Le directeur du bureau du
premier ministre est une personne loyale à Benyamin Nétanyahou, mais qui
n’a aucune compétence pour gérer l’ensemble des fonctions du cabinet.
Nous manquons donc d’une personne sur laquelle le public, les personnes
effrayées et la société traumatisée peuvent compter. Il n’y a pas de
Roosevelt, de Churchill ou de Ben Gourion aujourd’hui.

Le gouvernement sait-il où il va ?

À mon avis, il pense le savoir, mais il ne le sait pas. Ses membres
sont motivés par la vengeance. Ils veulent se venger et reconstruire leur
réputation après le choc et la surprise de l’attaque du Hamas. Ils pensent
que c’est possible grâce à la puissance militaire. Ils ont annoncé
des objectifs de guerre très ambitieux, très radicaux et, à mon avis,
irréalisables. Détruire le Hamas, c’est irréalisable, et c’est aussi
ce que pensent les Américains. Les Américains ont essayé de convaincre
le gouvernement israélien de changer ses objectifs de guerre, sans succès
jusqu’à présent.

La prochaine étape ne dépend donc pas de la perspective israélienne, mais de
la volonté de la communauté internationale d’imposer à Israël d’arrêter.

Il y a deux jours, Nétanyahou a parlé de la « dénazification » de la bande
de Gaza. Il s’agit donc de rééduquer les Palestiniens de la bande de Gaza,
non seulement pour détruire le Hamas, mais aussi pour construire une autre
réalité, pour rééduquer les habitants de la bande de Gaza afin qu’ils
ne haïssent pas Israël. Les dirigeants israéliens sont déconnectés du
monde, de ce qui se passe dans leur environnement régional et de ce qui se
passe dans les principales villes européennes, mais aussi dans les villes
américaines, où se déroulent de grandes manifestations. Ils sont enfermés
dans leur chagrin, leur frustration, leur colère. Et ils ne pensent pas au
long terme. Ils ne se demandent pas comment changer la réalité. Ils veulent
changer l’esprit des gens, pas l’environnement politique.

Il y a aussi les pressions des religieux nationalistes, pour reconstruire
les colonies israéliennes de la bande de Gaza évacuées en 2005 et
même au-delà. Certains appellent à la reconstruction de trois villes de
colonisation dans la bande de Gaza, sur les ruines de la ville de Gaza. Ils
ont même présenté un plan d’ingénierie, où figure le nombre exact
d’unités de logement.

Ce n’est pas très optimiste pour les jours et les semaines qui viennent…

Le plan israélien est de reprendre la guerre à grande échelle, soit pour
achever ce qu’ils ont commencé au nord de la bande de Gaza, car il y a
des zones au nord de la bande de Gaza qu’Israël n’a pas détruites et
dont il n’a pas encore pris le contrôle, soit de faire la même chose dans
le sud de la bande de Gaza, tout en expulsant les Palestiniens, les civils,
vers l’ouest de la bande de Gaza près du bord de mer. Ou de faire les deux.
Est-il possible de déplacer 2 millions de personnes, de civils du sud de la
bande de Gaza, sans créer une nouvelle catastrophe humanitaire ? À mon avis,
c’est impossible. La prochaine étape ne dépend donc pas de la perspective
israélienne, mais de la volonté de la communauté internationale d’imposer
à Israël d’arrêter.

Selon vous, la communauté internationale a-t-elle cette volonté, et si elle
l’a, en a-t-elle les moyens ?

Je pense que oui car, premièrement, il y a de plus en plus de manifestations
de masse dans les capitales. Deuxièmement, le nombre de civils palestiniens
innocents tués est sans précédent. C’est plus que dans n’importe quel
autre conflit de ce siècle. Et plus d’enfants ont été tués. Save the
Children a publié des statistiques montrant que plus d’enfants ont été
tués en quelques semaines à Gaza par Israël que pendant toute l’année
dans tous les autres conflits dans le monde.  Lors d’un rassemblement des
proches des otages de la famille Bibas à Tel-aviv le 28 novembre 2023. ©
Photo Ilia Yefimovich / DPA / Abaca C’est donc cette ampleur sans précédent
que la communauté internationale ne peut tolérer. Et certaines voix au
sein de la Maison-Blanche et du Département d’État, relayées par des
médias américains, le New York Times et le Washington Post, s’élèvent
pour mettre en cause Biden et Blinken pour leur trop grande mansuétude à
l’égard d’Israël. Des sondages indiquent que Biden est en passe de perdre
les élections parce que les progressistes ne se rendront pas aux urnes.

La trêve a pris fin vendredi matin, le 1er décembre. Comment réagissez-vous
aujourd’hui ?

J’ai parlé aujourd’hui et hier avec quelques Israéliens. Lorsque j’ai
dit que nous devions mettre fin à ce bain de sang par un accord politique,
ils ont répondu : « Mais les Palestiniens ne veulent pas de nous dans la
région. » Un autre a dit : « Nous devons tuer tous les membres du Hamas
comme cela a été fait en Allemagne et au Japon pendant la Seconde Guerre
mondiale. » Si nous prenons les chiffres de cette guerre, Israël a expulsé
plus de Palestiniens qu’en 1948 et a tué plus de civils. En 1948, les crimes
de guerre israéliens étaient commis du nord au sud, alors qu’aujourd’hui,
ils ne concernent qu’une petite zone. Nous sommes dans un jeu à somme nulle :
les Israéliens et les Palestiniens considèrent la guerre comme existentielle.
Vous avez été impliqué dans les négociations israélo-palestiniennes et
notamment celles qui ont abouti au plan de paix dit « initiative de Genève
», le dernier plan sérieux sur la table. En tant qu’ancien négociateur,
que pensez-vous possible aujourd’hui ?

La communauté internationale devrait exiger de Nétanyahou l’arrêt de la
guerre. Elle devrait en parallèle présenter les principes d’une solution à
deux États. Le processus doit être différent de celui d’Oslo, qui était
un processus ouvert. J’attends de la communauté internationale qu’elle
dise : « Voici les principes du règlement final. Nous travaillerons, avec
les parties, pour parvenir à la solution à deux États. Il ne s’agit
pas d’une solution ouverte. Et nous serons profondément impliqués, et le
Hamas y participera. » Il est impossible de détruire le Hamas. Mais le Hamas
politique et son aile militaire qui accepte la solution de deux États et un
accord avec Israël peut faire partie de l’accord.

Vous pensez que le Hamas peut accepter la solution à deux États et qu’il
est possible de l’intégrer dans des négociations ?

Le président de l’Autorité palestinienne, Mahmoud Abbas, voulait le
lui faire accepter, mais Israël, les États-Unis et aussi malheureusement
l’Europe l’en ont empêché. En 2021, Abbas a conclu un accord avec le
Hamas selon lequel ce dernier participerait aux élections et accepterait de
facto la politique de l’OLP. Cet accord a été possible car en 2017 le
Hamas a changé de politique et de doctrine. Mais la décision d’Abbas a
finalement été rejetée par la communauté internationale.

La partie israélienne n’acceptera jamais cela…

Ce que le gouvernement israélien veut faire dans la bande de Gaza, c’est
trouver un collaborateur dans les rues de Gaza, quelqu’un qui ressemble à une
organisation comme l’armée du Sud-Liban ou l’Autorité palestinienne sous
Abbas, qu’Israël peut manœuvrer, manipuler, contrôler. Il veut faire la
même chose qu’au Sud-Liban en son temps ou qu’actuellement en Cisjordanie
Israël refuse de reconnaître que cette politique de gestion des conflits avec
des régimes collaborateurs s’est totalement effondrée. Et elle ne tient
pas, même aujourd’hui, en Cisjordanie. La Cisjordanie brûle. De nombreux
Palestiniens ont été tués, des colons ont attaqué des Palestiniens et
de petites communautés palestiniennes ont été déplacées par les colons
et l’armée.

L’ordre imposé par Israël depuis 2006 s’est totalement effondré. Ce dont
nous avons besoin, c’est d’un nouvel ordre, qui devrait être proposé
par la communauté internationale.  Un autre principe s’est effondré le 7
octobre : celui selon lequel, si nous disposons d’une technologie avancée
et d’un mur bien construit sur le sol et sous la surface, nous sommes en
sécurité. La technologie et le mur massif n’assurent pas la sécurité. Si
nous regardons nos relations avec l’Égypte et la Jordanie, elles sont bien
meilleures, seule une barrière sans mur a été construite récemment dans
le Néguev avec l’Égypte. Un accord politique et un projet de loi, ainsi
qu’un travail visant à changer la réalité sur le terrain et les relations
entre les gens, peuvent nous apporter une bien meilleure sécurité.

Pensez-vous que l’initiative de Genève pourrait constituer une base de
négociation ?

Grosso modo, l’initiative de Genève est un modèle. L’idée des deux
États est pertinente. Mais elle doit être mise à jour. Tout d’abord, à
Jérusalem : l’ouverture maximale des frontières devrait être le principe
directeur de la nouvelle réalité de Jérusalem. Si nous construisons une
frontière dure à Jérusalem, cela détruira les deux capitales : la capitale
palestinienne et la capitale israélienne. Une approche très différente
doit donc être mise en œuvre. Nous avons travaillé dans cette direction
à Genève.  Autre point qui n’avait pas été réglé à Genève et qui
doit l’être : la question des réfugiés. En ce qui concerne les colonies,
nous devons voir si nous pouvons garder des colonies pour un certain nombre
d’années, disons sur le modèle de l’accord entre la Grande-Bretagne
et la Chine concernant Hong Kong, mais pour une période plus courte. Ainsi,
certaines des principales grandes colonies à l’intérieur de la Palestine
seront une terre palestinienne qu’Israël pourrait louer et gérer pour,
disons, cinq à dix ans.  Ce type d’idées créatives devrait être inclus dans
l’accord. Nous pouvons y parvenir. Mais avec une intervention internationale
massive et une médiation entre les parties. Et, avant tout, les Palestiniens
et les Israéliens doivent organiser des élections.

En effet, la question qui se pose est aussi celle-ci : qui pour négocier,
qui pour faire admettre les concessions à son peuple ?

Nous devons organiser des élections le plus rapidement possible, car en
Israël, aucun gouvernement ne jouit de la confiance du public. Nombreux sont
ceux qui souhaitent le départ de Nétanyahou, mais ce dernier refuse. Il
n’y a donc pas d’autre solution que de procéder à des élections.
En Palestine aussi, des élections sont nécessaires, car toute administration,
toute force internationale qui entre à Gaza aux côtés d’Israël ou qui
remplace Israël dans la bande de Gaza, qui prend en charge la reconstruction
de Gaza, doit jouir de la légitimité publique palestinienne.

Dans le cas contraire, elle sera considérée comme un collaborateur de
l’occupant israélien. Il se passera à Gaza la même chose qu’au Liban
lorsque les Français et les Américains ont envoyé des soldats dans les
années 1980, à la suite de la guerre israélienne au Liban de 1982, avec des
attentats terroristes. Il y en a eu contre les Marines, et contre les Français
[attentats de l’aéroport de Beyrouth et du Drakkar du 23 octobre 1983 –
ndlr].  Ce qu’il faut, c’est légitimer l’Autorité palestinienne. Si
l’Autorité palestinienne entre à Gaza, elle ne peut pas y entrer en tant
que collaboratrice d’Israël. Elle doit y entrer sur la base de sa propre
légitimité. Et le seul moyen d’obtenir cette légitimité est de procéder
à des élections.

Ne craignez-vous pas une nouvelle poussée de l’extrême droite en Israël
si des élections sont organisées dans un proche avenir ?

Certains éléments indiquent qu’elle obtiendra moins de voix que lors des
dernières élections. Les études penchent pour une victoire du centre-droit,
incarné aujourd’hui par Benny Gantz. Il est moins dogmatique que Nétanyahou
et il est ouvert aux critiques et aux conseils des Américains. Nétanyahou
semble, lui, très fermé d’esprit.

Côté palestinien, on parle de plus en plus de Marwan Barghouti. Est-ce à
vos yeux une personnalité qui pourrait mener le processus, côté palestinien ?

Dans tout scénario d’échange complet « otages contre prisonniers », il
devrait être inclus. C’est dans l’intérêt d’Israël d’inclure Marwan
Barghouti, car il peut assurer la stabilité dans l’arène palestinienne. Son
nom est accepté par le Hamas, il est accepté et admiré par de nombreux
Palestiniens, je pense donc que c’est une option qu’Israël doit prendre
en compte.

