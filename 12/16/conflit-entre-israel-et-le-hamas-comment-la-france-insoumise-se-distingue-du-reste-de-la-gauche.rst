

.. _lfo_2023_12_16:

=================================================================================================================
2023-12-16 Conflit entre Israël et le Hamas : comment La France insoumise se distingue du reste de la gauche
=================================================================================================================

- https://www.lemonde.fr/politique/article/2023/12/16/conflit-entre-israel-et-le-hamas-comment-la-france-insoumise-se-distingue-du-reste-de-la-gauche_6206135_823448.html

Conflit entre Israël et le Hamas : comment La France insoumise se distingue
du reste de la gauche ANALYSE Brice Teinturier Directeur général délégué
d’Ipsos

La deuxième vague de l’enquête sur les élections européennes réalisée
par Ipsos-Sopra Steria pour « Le Monde » et le Cevipof montre que les
sympathisants LFI soutiennent davantage les Palestiniens que les Israéliens,
une exception. En revanche, elle montre la désapprobation massive des sondés,
même à gauche, envers les prises de position de Jean-Luc Mélenchon sur
la guerre au Proche-Orient.  Ce que nous mesurons comme intentions de vote
à six mois du scrutin européen est bien entendu très volatil et ne tient
pas compte des effets que la campagne électorale produira. Pour autant,
l’intérêt réside dans l’évolution des résultats depuis juin et dans
leur croisement avec la perception du conflit au Proche-Orient. De ce point
de vue, le faible niveau de la liste de La France insoumise (LFI) (7,5 %,
– 1 point), la dynamique du Rassemblement national (RN) (28 %, + 4 points)
et la désapprobation massive des prises de position de Jean-Luc Mélenchon
constituent un fait notable et interpellent, même si le conflit n’explique
évidemment pas tout.

L’enquête fait ainsi apparaître la très grande singularité des électeurs
de LFI, y compris au sein de l’espace des gauches, et combien les événements
du 7 octobre, date de l’attaque du Hamas contre Israël, ont ajouté une
fracture supplémentaire au sein d’une Nouvelle Union populaire écologique
et sociale (Nupes) déjà amplement fragilisée. La spécificité des prises
de position des différentes familles politiques apparaît d’emblée
sur un indicateur simple, celui de la sympathie, de l’antipathie ou de
l’indifférence à l’égard des Israéliens, des Palestiniens, du Hamas et
du gouvernement de Benyamin Nétanyahou. Chez les Français, la balance penche
en faveur des Israéliens : 53 % de sympathie, 18 % d’antipathie, contre 45 %
et 25 % à propos des Palestiniens. Dans toutes les catégories politiques,
la sympathie à l’égard des Israéliens est majoritaire… sauf chez les
sympathisants de La France insoumise, où elle tombe à 41 % seulement.

En revanche, 72 % de ces mêmes sympathisants éprouvent de la sympathie
pour les Palestiniens, chiffre qu’on ne retrouve dans aucune autre famille
politique. Certes, le clivage gauche-droite fonctionne sur cet indicateur, les
sympathisants écologistes et socialistes éprouvant un peu plus de sympathie
à l’égard des Palestiniens que des Israéliens. Mais ce qui frappe, c’est
bien que chez eux, le différentiel de sympathie entre ces deux peuples n’est
que de quelques points alors qu’il est massif au sein de La France insoumise
(31 points d’écart). Rappel utile : nous parlons là de la perception des
peuples, pas du gouvernement israélien. Il y a donc incontestablement une
hostilité à l’égard des Israéliens bien plus grande chez les sympathisants
de LFI que dans la population tout entière et qu’au sein des gauches.

Inversion de la hiérarchie des responsabilités Le deuxième point à relever
est que le Hamas génère 6 % de sympathie chez les Français (soit tout de
même un peu plus de 3 millions de personnes…) et 72 % d’antipathie mais
que ce taux monte à 14 % chez les sympathisants de La France insoumise – et
13 % chez ceux du Parti communiste (PCF). Le refus de le taxer d’organisation
terroriste et la volonté de le présenter comme un mouvement de résistance
produit à l’évidence ses effets. De fait, à la question « Le Hamas est-il
une organisation terroriste ? », 78 % des Français répondent oui et 6 % non.

Chez les sympathisants de La France insoumise, ce chiffre est également
majoritaire – donnée manifestement non prise en compte par les responsables
de cette formation… –, mais il est de 19 points en retrait de la moyenne des
Français puisqu’il s’établit à 59 %. Surtout, 23 %, soit près d’un
sympathisant LFI sur quatre, estime que le Hamas n’est pas un mouvement
terroriste. Un résultat que l’on ne retrouve ni à droite, ni au PCF (9 %),
ni chez les écologistes (3 %) ou au Parti socialiste (PS, 7 %). Là encore,
singularité… Interrogés plus spécifiquement sur le fait de savoir si le
Hamas et Israël sont ou ne sont pas « responsables de la situation actuelle
de la population civile palestinienne à Gaza », indicateur qui permet de
mesurer les effets dans l’opinion des bombardements Israéliens, 88 % des
Français considèrent que le Hamas est responsable de la situation et 65 %
que c’est Israël. Un écart de 23 points qui reste significatif, même si
la responsabilité perçue est forte chez les deux protagonistes. Pour autant,
là encore, les sympathisants de LFI se singularisent puisqu’ils sont la
seule famille politique à inverser la hiérarchie des responsabilités :
chez eux 84 % estiment que la responsabilité est du côté israélien, 72 %
du côté du Hamas.

Pompiers incendiaires Reste à explorer la perception spécifique de Jean-Luc
Mélenchon et de ses prises de position à propos du conflit entre Israël et le
Hamas : 9 % seulement des Français l’approuvent et 57 % le désapprouvent,
soit le plus fort niveau de rejet de toutes les personnalités testées. Un
résultat d’autant plus important que 50 % seulement des sympathisants LFI
l’approuve, près d’un sur quatre (23 %) étant dans la désapprobation
– le reste des sondés estimant n’avoir pas assez d’éléments pour
juger. Surtout, cette désapprobation atteint 62 % chez les sympathisants
du PCF, 54 % chez ceux d’Europe Ecologie-Les Verts et 59 % chez ceux du
PS. Une fracture aussi réelle que profonde à gauche.  Enfin, et ce n’est
pas le moindre, les propos de Jean-Luc Mélenchon et de certains responsables
de La France insoumise sont perçus par 46 % des Français comme des propos
qui « vont au-delà de la critique d’Israël, sont antisémites et attisent
l’antisémitisme au sein de la population », 35 % estimant qu’ils « ne sont
pas directement antisémites mais contribuent, dans les arguments utilisés pour
critiquer Israël, à attiser l’antisémitisme au sein de la population » et
18 % seulement qu’ils « ne sont pas antisémites et expriment simplement une
critique de l’action d’Israël ». Antisémites ou pompiers incendiaires,
tel est le jugement des Français. Mais il est aussi partagé par 33 % des
sympathisants LFI, 72 % des sympathisants PCF, 78 % des écologistes et 81 %
des sympathisants socialistes.

En résumé, une stratégie mortifère pour La France insoumise et la gauche
tout entière, dont on verra si elle s’atténue dans le temps ou pas.  Sondage
Ipsos-Sopra Steria, réalisé pour Sciences Po et Le Monde du 29 novembre au
12 décembre, sur un échantillon de 11 691 personnes, représentatif de la
population française inscrite sur les listes électorales, âgée de 18 ans
et plus (méthode des quotas).

Brice Teinturier (Directeur général délégué d’Ipsos)
