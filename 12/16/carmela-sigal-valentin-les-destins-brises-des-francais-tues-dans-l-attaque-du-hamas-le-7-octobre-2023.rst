

.. _monde_2023_12_16:

======================================================================================================================
2023-12-16 Carmela, Sigal, Valentin… Les destins brisés des Français tués dans l’attaque du Hamas le 7 octobre
======================================================================================================================

- https://www.lemonde.fr/international/article/2023/12/16/carmela-sigal-valentin-les-destins-brises-des-francais-tues-dans-l-attaque-du-hamas-le-7-octobre_6206138_3210.html

Carmela, Sigal, Valentin… Les destins brisés des Français tués dans
l’attaque du Hamas le 7 octobre Par Marie-Béatrice Baudet et Annick Cojean


RÉCIT Dans l’attente de l’hommage national promis par Emmanuel Macron, la
liste nominative des victimes françaises de l’attaque terroriste du Hamas
reste inaccessible. Peu à peu, malgré tout, certaines familles acceptent
d’évoquer leurs proches disparus.  Quarante et un Français sont morts
pendant ou après l’attaque menée le 7 octobre par le Hamas en Israël,
qui restera dans l’histoire comme le plus important massacre de juifs
depuis la Shoah. Quarante et un, sur quelque 1 140 victimes, soit le plus gros
contingent d’étrangers tués, avant les Thaïlandais (essentiellement des
ouvriers employés dans des communautés agricoles), les Américains et les
Ukrainiens. Jamais la France n’a été aussi touchée par un attentat à
l’étranger depuis l’explosion du DC-10 d’UTA au-dessus du Niger, en
1989. Qui étaient ces quarante et un morts ? Quelle était leur histoire ? La
raison de leur présence en Israël ? Leur lien avec la France ? Avaient-ils
la double nationalité ? Etait-ce un choix ou l’héritage d’une histoire
familiale ? Les questions sont multiples. Impossible, hélas, d’y répondre
précisément, la liste demeurant secrète.

« C’est assez incroyable, s’étonnait mi-novembre Yonathan Arfi, le
président du Conseil représentatif des institutions juives de France. Quelques
noms, ici et là, nous parviennent. Mais nous sommes très loin de les
connaître tous. » L’Elysée et le Quai d’Orsay se refusent à divulguer
cette liste. A l’ambassade d’Israël en France, on nous renvoie à celle de
France en Israël, laquelle n’est guère plus loquace. Tout juste perçoit-on
l’extrême sensibilité des familles à l’idée d’une éventuelle
publication du nom d’un proche et la réticence des autorités françaises à
communiquer à leur place. L’hommage national relancera ce sujet délicat.
La date de cette cérémonie à la mémoire des victimes françaises, prévue
devant le monument dédié aux victimes du terrorisme derrière les Invalides,
n’est pas encore fixée. Annoncé dès le 26 octobre par Emmanuel Macron
et réclamé par les principales organisations juives, plusieurs responsables
politiques ou encore l’ex-chef de l’Etat François Hollande, le principe
d’un hommage fait consensus. Il n’y a « ni hésitation ni tâtonnement
», a affirmé le président de la République le 7 décembre, juste de la
prudence liée à l’« inquiétude » sur les Français dont on reste sans
nouvelles. Une raison de plus de ne pas rendre publique une liste de victimes
présentée comme définitive. L’annonce, vendredi 15 décembre, de la mort
d’Eliya Toledano, enlevé par le Hamas et dont le corps a été retrouvé
dans la bande de Gaza, justifie ces précautions. Le décompte macabre n’est
peut-être pas terminé.

Le Parquet national antiterroriste est dans la même expectative. Le
12 octobre, il a ouvert une enquête préliminaire sur les chefs d’«
assassinats et tentatives d’assassinats en relation avec une entreprise
terroriste » et d’« enlèvements et séquestrations de personnes en bande
organisée et en relation avec une entreprise terroriste ». Déjà saisi de
vingt et une plaintes, déposées par plusieurs avocats représentant des
familles de victimes, il s’attend à en recevoir d’autres. En Israël,
l’identification des corps mutilés et brûlés se poursuit, désormais
confiée, entre autres experts, à des archéologues chargés d’examiner
des dents et des fragments d’os.  Les dossiers du parquet se répartissent
en trois groupes : les quarante et un morts, les huit blessés ou impliqués
(visés par des tirs) et les personnes enlevées ou considérées comme
disparues. Sur les huit Français concernés au début des événements,
quatre ont été libérés, un a été tué – Eliya Toledano –, mais le
sort d’Ohad Yahalomi, 49 ans, d’Ofer Kalderon, 53 ans et du Franco-Mexicain
Orion Hernandez Radoux reste incertain.

La Cour pénale internationale (CPI) pourrait aussi avoir son mot à dire dans
les investigations menées. Plusieurs avocats français ont déposé plainte
pour crime contre l’humanité. « Il est fondamental de reconnaître que ces
événements ne concernent pas que les proches des victimes, mais l’ensemble
de l’humanité, affirme Me François Zimeray, reçu le 17 novembre à la
CPI avec plusieurs familles. Inscrire ces faits dans la mémoire collective
est essentiel. Les grands procès racontent notre histoire. »

Concernant la liste des quarante et un morts, une révélation est concédée au
Monde : on y compte trente civils et onze militaires. Cette information soulève,
pour les magistrats, une question de droit : ces onze militaires entrent-ils
dans le périmètre de leur enquête ? Des soldats abattus les armes à la main
peuvent-ils être considérés comme des victimes du terrorisme ? La doctrine
en vigueur, notamment pour les opérations au Sahel, répond par la négative.
Les binationaux semblent largement majoritaires. Est-ce un reflet de la société
israélienne (9 millions d’habitants), dans laquelle la communauté française,
estimée entre 150 000 et 180 000 personnes, est particulièrement importante
? « Restons prudents, recommande l’historien Yann Scioldo-Zürcher, coauteur
du livre Partir pour Israël (Presses universitaires François-Rabelais,
192 pages, 25 euros), car il est impossible de connaître avec précision
le nombre de binationaux en Israël. Se manifester auprès de son consulat
à l’étranger relève d’une démarche purement individuelle. Selon les
chiffres officiels de l’Etat hébreu, environ 36 % de sa population a une
origine européenne ou nord-américaine. Mais cela ne fait pas pour autant
36 % de binationaux potentiels ! Dans les années 1950-1960, il est arrivé
que certains pays, comme la Roumanie, retirent leur nationalité à leurs
ressortissants qui émigraient vers Israël… »

Les enterrements se sont succédé depuis le 7 octobre. L’ambassadeur
de France s’est rendu à plusieurs cérémonies en Israël. Des familles
françaises ont fait le voyage pour inhumer des proches et participer aux
rituels de deuil. En France, la presse régionale s’est parfois fait
l’écho de familles concernées. Et les réseaux sociaux n’en finissent
plus d’entretenir la mémoire de jeunes disparus. Pourtant, il semble
que nombre de familles trop exposées devant les caméras le regrettent
aujourd’hui. Certains proches, en grande détresse psychologique, n’ont
plus la force de s’exprimer. D’autres, pétrifiés par des menaces et des
insultes antisémites, ont désormais peur de se manifester. Dans l’attente de
l’hommage national qui permettra d’associer des visages et des parcours de
vie aux victimes, une poignée de personnes ont accepté d’évoquer auprès
du Monde quelques destins brisés.

Un dimanche glacé du mois de décembre, huit semaines après le massacre
commis le 7 octobre par le Hamas dans les villages et kibboutz répartis
autour de la bande de Gaza, plusieurs dizaines de personnes se sont réunies
dans un immeuble discret, au cœur de Paris. Il n’y avait pas eu d’annonce
publique, pas de convocations sur les réseaux sociaux, mais des invitations
particulières transmises par le bouche-à-oreille avec la simple demande de
s’inscrire pour « l’hommage à Carmela ». Carmela, du nom de cette dame
de 80 ans, au sourire très doux, assassinée avec sa petite-fille autiste de
12 ans, Noya, dans le kibboutz de Nir Oz, qu’elle avait contribué à créer
avec son mari, Uri Dan-Jaoui, né de père tunisien et de mère française,
mort il y a six ans. Carmela, qui aimait les enfants, la musique, les fleurs,
et avait milité toute sa vie pour la paix. Carmela, dont se souvenaient avec
tendresse les cousins et petits-cousins français du couple. Mais pas seulement
: toute une génération de jeunes juifs formés par l’association Hachomer
Hatzaïr, ce mouvement de jeunesse laïque, « empreint des idéaux du sionisme
et du socialisme », que Carmela et Uri, quittant quelques années Israël,
avaient animé à Marseille dans les années 1980, puis à Paris au début
des années 1990.

Sur un piano était posé un dessin représentant Noya, blottie contre sa
grand-mère. Et sur scène, devant un parterre d’enfants, de parents et de
proches de la famille Dan-Jaoui, plusieurs intervenants se sont succédé pour
évoquer la personnalité de Carmela. En quelques anecdotes, est alors apparue
l’image d’un couple fusionnel qui, toute sa vie, a poursuivi l’idéal de
gauche des kibboutz et a partagé la conviction qu’Israël et la Palestine
pouvaient vivre en paix. Un couple qui avait vibré à l’annonce des accords
d’Oslo, en 1993 ; n’avait eu de cesse de tendre la main aux voisins arabes
et palestiniens ; allait aider aux récoltes d’olives dans les villages arabes
des environs ; encourageait la prise en charge de malades de Gaza par des membres
du kibboutz pour les transporter vers les hôpitaux israéliens ; exécrait la
colonisation des territoires, viscéralement hostile à sa violence et à son
idéologie.  Touché par l’appel du muezzin qu’il entendait à Nir Oz depuis
Gaza et persuadé que les notes qui s’affranchissaient des clôtures étaient
une amorce de dialogue, Uri, encouragé par Carmela, avait imaginé – et fait
voter par le conseil du kibboutz – l’installation d’un jeu de miroirs
immenses sur la tour d’une usine qui, selon la position du soleil, envoyait
des « signaux d’amitié » aux Gazaouis. De même qu’il avait conçu un
panneau « Faisons la paix » pour remplacer les barrières à l’entrée du
kibboutz. « L’utopie pacifiste de ce couple était sans limite, a raconté
un cousin. Un jour, Uri m’a même dit être prêt à céder aux Gazaouis,
s’il le fallait, le territoire du kibboutz. “Tu es fou, lui ai-je dit ! Tu
abandonnerais la tombe de tes parents enterrés à Nir Oz ?” Il m’avait
répondu : “Nous accueillons bien des tombes musulmanes dans nos cimetières
!” Eh bien, c’était ça, Uri et Carmela ! »

Les témoignages ont continué. « En ce moment, le plus difficile est de garder
espoir, a dit un jeune homme. Mais en pensant à Carmela, il nous faut résister
aux sentiments de haine et de vengeance ! » Un autre a appelé à défendre
l’idéal de Nir Oz, ce kibboutz fondé en 1955, à l’opposé de l’état
d’esprit de l’actuel premier ministre, Benyamin Nétanyahou. « Nous avons
une dette envers Uri et Carmela. Il importe que leur souffle demeure ! » Et
d’appeler volontaires et bienfaiteurs à la reconstruction de Nir Oz. Enfin,
une enfant de 12 ans s’est adressée à la petite-fille de Carmela. « Nous te
faisons le serment, Noya, de tout faire pour un monde en paix. » Galit, la fille
de Carmela et la mère de Noya, pour laquelle « chaque jour est un 7 octobre »,
n’est pas encore parvenue à savoir ce qui leur était réellement arrivé. Et
cela la ronge. Le 6 octobre, elle avait convié sa mère à venir chez elle,
dans un kibboutz voisin de Nir Oz, partager la soirée du shabbat. Après le
dîner, la petite Noya s’en était allée dormir chez sa grand-mère, avec
qui elle avait un lien très privilégié. A l’aube du 7 octobre, tandis que
des roquettes s’abattaient sur toute la région et qu’on entendait des
tirs d’armes automatiques, Galit a eu brièvement Carmela au téléphone,
puis sa fille : « Il y a du bruit dehors, maman. J’ai peur. » Puis plus
rien. Pendant dix jours, elle a retenu son souffle et son sommeil, imaginant la
grand-mère et sa petite-fille prises en otage à Gaza puisque aucune trace ne
subsistait au kibboutz. Et puis les autorités l’ont prévenue que les corps
avaient été identifiés grâce aux analyses ADN. Où ? Comment ? Dans quelles
circonstances ? Elle n’a reçu aucune explication. Elle sait simplement que
la maison si soignée de Carmela, celle qui avait accueilli tant d’amis,
de parents, de jeunes venus de France, n’est plus qu’un tas de cendres.

Quand nous les rencontrons à Montpellier, Geneviève Molina et Michel Ghnassia
chuchotent plus qu’ils ne parlent. Parfois, les larmes les submergent et ils
pleurent en silence. Si les anciens époux acceptent de se confier, c’est parce
qu’ils sont animés par la volonté d’honorer la mémoire de leur fils,
Valentin, tué le 7 octobre ; « un shabbat maudit », murmure Geneviève. Le
jeune Français, engagé volontaire dans l’armée israélienne, a perdu
la vie en combattant les terroristes du Hamas qui attaquaient le kibboutz
de Beeri. Alors, oui, si hommage national aux Invalides il y a, ils s’y
rendront, accompagnés de leur fille aînée, Chloé, 25 ans, inconsolable
depuis la mort de son frère. Geneviève Molina et Michel Ghnassia, séparés
depuis plusieurs années, vivent à Montpellier, où Valentin est né le 28
octobre 2000. « Tout petit déjà, c’était une pile électrique », sourit
tristement Michel. « Solaire », « généreux », « drôle », « curieux
de tout », « débrouillard » et « passionné » sont les mots choisis pour
le décrire.  Valentin grandit au sein d’un foyer respectueux des traditions
juives et découvre Israël pour la première fois à l’âge de 9 ans. Il
s’éprend de cette terre où vit une partie de sa famille. Partir là-bas,
l’idée l’obsède, surtout après qu’il s’est fait traiter de « sale
juif » au collège. « Notre fils a souffert de l’antisémitisme et en avait
assez de se cacher pour porter la kippa », raconte Geneviève, épuisée par
des nuits sans sommeil. Mais il reste en France, passe son bac, intègre la
faculté de droit de Montpellier, où il étudie pendant quatre ans. A lire
dans Le Midi libre l’hommage qui lui a été rendu le 23 octobre, son passage
a marqué les esprits. En témoigne cet extrait du discours prononcé par le
doyen, Guylain Clamour : « Valentin était magnifique d’intelligence, de
vivacité et avait de grandes qualités oratoires. Sa mort est une terrible
déchirure pour notre communauté, celle des chercheurs qui éveillent les
consciences et forgent l’esprit critique. »

En février 2022, le jeune homme renonce à devenir avocat et s’envole
pour Israël, où il apprend l’hébreu et suit une formation militaire. «
Il voulait combattre, sauver des vies, protéger les gens », insiste son
père. C’est ce qu’il fera le 7 octobre, quand son unité parachutiste,
le bataillon 890, est envoyée à quelques kilomètres du kibboutz de Beeri,
pris d’assaut par le Hamas. Son escadron vient au secours de nombreux
civils. Après plusieurs heures de combat, le sergent Ghnassia est tué
quasiment à bout portant par un assaillant réfugié dans une des maisons du
kibboutz. « Son commandant nous a dit qu’il s’était comporté en héros
», ajoute Michel. Le 12 octobre, l’enfant de Montpellier a été inhumé sur
les hauteurs de Jérusalem, au cimetière militaire du mont Herzl. « S’il
lui arrivait quelque chose, il m’avait confié vouloir reposer dans ce pays
qu’il aimait tant », assure sa mère.  Valentin Ghnassia devait achever son
service militaire fin octobre puis demander la nationalité israélienne. Il
n’en a pas eu le temps. Mais le 15 novembre, le jeune Français a été
fait citoyen d’honneur d’Israël en vertu d’une loi adoptée en urgence
par la Knesset, le Parlement national, et qui porte son nom, « Eli Valentin
». Désormais, comme lui, les étrangers morts au combat « pour la patrie
» pourront obtenir la nationalité israélienne.

Sigal Levy, 31 ans, benjamine d’une famille de quatre enfants, a toujours
eu le goût des autres. Depuis toute petite, explique sa mère, Annie,
originaire de Cherbourg (Manche) et installée en Israël depuis le début
des années 1970, elle avait juré qu’elle s’occuperait des plus démunis,
des paumés, de ceux qui peinaient à trouver un sens à leur vie, convaincue
de pouvoir leur fournir de l’espérance et les remettre d’aplomb. Elle
était donc devenue assistante sociale et s’en trouvait heureuse. « Ses
amis lui disaient : “Ce sera dur et mal payé”, se souvient sa maman,
jointe par téléphone à Netanya. Mais Sigal s’en fichait. C’était sa
vocation. » Et il est bien possible que l’exemple de son père, originaire
du Maroc et aide-soignant auprès de handicapés, y soit pour quelque chose.
Volontaire pendant ses études au sein de l’association Elem et de son projet
Good People visant à secourir les jeunes en détresse, soit dans les villes en
les accueillant dans des antennes mobiles, soit dans des fêtes et festivals,
elle en était devenue salariée, dirigeant l’antenne, réputée difficile,
de l’agglomération de Ramla-Lod, à la population juive et arabe. Puis,
elle avait pris le poste d’assistante sociale dans une école de la banlieue
de Tel-Aviv qui était un peu celle de la dernière chance pour des ados
délinquants ou déscolarisés. Cela ne l’empêchait pas, pendant ses
loisirs, de reprendre bénévolement du service auprès d’Elem, toujours
pour s’occuper des jeunes à qui elle offrait une écoute affectueuse et
sans jugement. C’est ainsi qu’elle s’est retrouvée au festival Tribe
of Nova, dans la nuit du 6 au 7 octobre, et face aux terroristes du Hamas, au
petit matin. L’épouvante ressentie par l’équipe a été racontée à sa
mère par quelques rescapés. La fuite éperdue à bord d’une camionnette de
l’association, le chaos de la circulation, les tirs tous azimuts provenant de
terroristes surgissant de toutes parts, la panique de milliers de jeunes gens…

« On a localisé son téléphone, raconte la mère. Il ne bougeait ni ne
répondait pas. Alors on a pensé qu’il était tombé et on a recherché
Sigal dans tous les hôpitaux. » En vain. Le portable a finalement été
restitué à la famille au bout de quatre jours d’angoisse, ainsi que la
bague de fiançailles de Sigal. « L’or avait changé de couleur, poursuit
Annie Levy, mais je préfère ne pas avoir de détails. Son corps a été
identifié par la dentition dont l’armée conserve les données depuis le
service militaire. » La jeune femme se réjouissait de venir en France en
famille au mois de mars : « Elle devait se marier le 18 janvier. Nous avions
acheté ensemble la robe blanche qu’elle prévoyait d’offrir ensuite à une
association de femmes victimes de violences. Je m’en suis chargée. Sigal,
mon bébé aux yeux turquoise, ne la portera pas. Elle a été enterrée le
11 octobre, entourée d’une multitude de jeunes, de cousins, de copains,
de collègues dont elle était follement aimée. »

Céline Ben David Nagar avait envie d’aller danser, ce samedi 7 octobre. Cette
Franco-Israélienne de 32 ans, « joyeuse et lumineuse » comme la décrit
Mahaiane, l’une de ses amies, voulait fêter la fin de son congé
maternité et s’amuser avant de reprendre son travail à Tel-Aviv dans
le cabinet d’avocats où elle était responsable du secrétariat. Elle y
avait rencontré son futur mari, Ido Nagar, et de leur union était née la
petite Ellie, âgée aujourd’hui de 8 mois.  Céline, accompagnée d’un
couple d’amis, quitte vers 5 heures la ville d’Holon, où ils résident,
et roule vers le sud pour rejoindre le festival de musique Tribe of Nova. Ils
n’y arriveront jamais. Sur la route, une pluie de roquettes, tirées à 6
h 30 de la bande de Gaza, les oblige à foncer dans un abri. Céline appelle
son mari, qui propose de venir la chercher. A 7 h 11, elle le rassure par un
SMS : « Ne t’inquiète pas, les soldats sont là. » Ce seront ses derniers
mots. Les terroristes du Hamas, vêtus de faux uniformes israéliens, tirent
à tout va et lancent des grenades dans le refuge en béton. Une vidéo de
télésurveillance du kibboutz de Mefalsim, situé à proximité, filme la
scène que la famille découvrira plus tard avec horreur.

Sans nouvelles de sa femme, Ido Nagar la croit entre les mains du Hamas. Epaulé
par Yaelle Krief, porte-parole du comité de soutien créé afin de demander la
libération de Céline, il participe, le 12 octobre, à une conférence de presse
consacrée aux familles des Français enlevés ou portés disparus. L’avocat
a choisi de s’exprimer devant les caméras, la petite Ellie assise sur ses
genoux. A ses côtés, Samuel, le frère de Céline, appelle Paris à l’aide
: « Nous avons grandi en France ; dans notre cœur, on est français à 100 %
; on chante des comptines à Ellie en français… » Les images font le tour
du monde. Quatre jours plus tard, le 16 octobre, des militaires annoncent à
Ido Nagar la mort de son épouse, laquelle sera enterrée dès le lendemain au
cimetière de Holon.  Céline est née en Israël d’une mère française et
d’un père israélien, mais elle a grandi à Lyon où sa famille a décidé
de s’installer. Inquiets des insultes et des menaces physiques proférées
à l’encontre de leurs enfants parce que juifs, les parents décident,
en 2006, de repartir en Israël. L’adolescente n’en pouvait plus de ces
agressions antisémites. Elle devient alors interne au lycée français de Guivat
Washington, près de la ville d’Ashdod, et partage notamment sa chambre avec
Maihaiane, originaire de Rouen. « J’entends encore son rire si communicatif,
mais elle savait aussi défendre son point de vue », témoigne la jeune femme,
qui a fait son alya en 2008.

Après le lycée, Céline Ben David choisit de ne pas intégrer l’armée,
préférant accomplir un service national qui l’amène à s’occuper
d’enfants en difficulté. Puis elle enchaîne les petits boulots, vit en
colocation à Eilat puis à Tel-Aviv, croque la vie à pleines dents. «
Elle était super attachante », témoigne Maihaiane qui, en son souvenir,
a organisé, le 11 décembre, une fête de Hanouka pour Ellie, le bébé de
Céline. La première, sans sa mère, mais une cagnotte à laquelle beaucoup
d’amis ont participé a permis de faire une razzia dans les magasins de jouets.

Shiraz Brodash, Franco-Israélienne de 23 ans, était une athlète
complète. Elle courait, sautait, dansait, et les habitants du village agricole
de Ramot Meir, où s’étaient installés ses grands-parents français
après leur alya à la fin des années 1960, avaient l’habitude de croiser
sur les sentiers sa fine silhouette de joggeuse. Adolescente, elle s’était
fait remarquer en gymnastique artistique et acrobatique et avait pris part à
de nombreuses compétitions d’athlétisme. Cela aurait pu la dispenser de
service militaire, en le remplaçant par des entraînements sportifs. Mais non :
la jeune fille avait tenu à faire son devoir dans le pays qu’avait choisi de
rejoindre sa famille. Après les deux ans réglementaires, elle avait préparé
un diplôme de coach sportive et avait créé, dans une partie du jardin de ses
parents, une petite salle de sport qu’elle équipait peu à peu, et où elle
donnait notamment des cours de pilates.  Entreprenante, elle avait pour ambition
de compléter sa formation par des études de diététique. « Un tourbillon
de joie », nous dit sa grand-tante, Chantal Méttoudi, ancienne inspectrice
de l’éducation nationale en France, qui voyait la jeune fille plusieurs
fois par an. Sur les réseaux sociaux, ses amies évoquent son optimisme et sa
capacité à entrer en relation avec quiconque. Chantal Méttoudi confirme :
« Trois mots d’hébreu, deux mots d’anglais, un mot de français, et
hop ! Shiraz, dans un grand éclat de rire, emportait l’adhésion de tout
le monde. On riait tant ensemble. Elle était si gentille ! » Il y a un an,
alors que sa mère, professeure de littérature, affrontait un grave problème
de santé, Shiraz avait pris son relais, s’occupant de sa sœur et de son
frère plus jeunes, soutenant sa mère qu’elle appelait son « héroïne ».

Le 6 octobre, elle était allée passer la soirée de shabbat dans la ville de
Netivot, où habitait la famille de son amoureux, membre des forces spéciales
de la police. A 7 heures, le lendemain, celui-ci a reçu l’ordre de rejoindre
en urgence son unité d’élite, sans plus d’informations. Shiraz a proposé
de le déposer elle-même à la base et de rentrer ensuite chez ses parents,
plus au nord. Elle n’en a pas eu le temps. Sur la route, un obus du Hamas a
torpillé leur voiture. Le corps du jeune homme a été identifié au bout de
neuf jours grâce au numéro de son arme de service ; celui de Shiraz par des
prélèvements d’ADN. Le seul réconfort des familles tient dans l’amour
que se portaient les deux jeunes gens qui avaient voyagé à Paris au printemps
et prévoyaient de faire leur vie ensemble. A la fin des trente premiers jours
de deuil, la famille du garçon est venue offrir à celle de Shiraz la bague
de fiançailles qu’il venait d’acheter.

Marie-Béatrice Baudet et Annick Cojean
