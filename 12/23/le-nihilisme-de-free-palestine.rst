.. index::
   pair: Nihilisme; Free Palestine
   ! Nihilisme

.. _mansour_2023_12_23:

===============================================================================
2023-12-23 **Le nihilisme de Free Palestine** par Hussein Aboubakr Mansour
===============================================================================

- https://k-larevue.com/le-nihilisme-de-free-palestine/

.. tags:: Nihilisme

Free Palestine. Le slogan à la traduction ambigüe fait florès dans les
manifestations en soutien à la population de Gaza. Que sous-tend-il ? Dans ce
texte d’une clarté déconcertante, l’essayiste Hussein Aboubakr Mansour
revient aux sources du slogan et propose une archéologie de la volonté
politique qu’il porte.

« Free Palestine » – qu’il s’agisse du slogan, du fantasme ou de la
politique – a toujours impliqué le meurtre de masse des Juifs dans leurs
villes, leurs rues, leurs magasins et jusqu’à leurs salons. Peu sont prêts
à l’assumer publiquement, mais dans de nombreux cercles intellectuels,
professionnels et populaires au Moyen-Orient et en Occident, l’idée de la
libération nationale palestinienne a de longue date été formulée en des
termes qui cautionnent ou exigent le massacre aveugle de juifs. Pour des acteurs
plus transparents, tels que le Hamas et la République islamique d’Iran, la
libération de la Palestine signifie tout simplement, et sans aucune réserve,
l’éradication totale d’Israël. Il ne s’agit pas d’un point polémique,
mais d’une réalité fondamentale qui exige un examen approfondi.

Prenons l’exemple du climat idéologique dans lequel de nombreux Arabes et
musulmans ont été élevés, moi y compris. En tant que musulman ayant grandi
en Égypte, le concept de Palestine n’a jamais été affaire de géopolitique
; il s’agissait d’un élément profondément ancré dans notre identité
morale collective, l’élément unificateur des tendances religieuses et
séculières de notre nationalisme arabe. C’était, et c’est toujours, une
cause qui trouvait en nous une résonance politique, sociale et spirituelle,
souvent à la limite d’une ferveur défiant toute rationalité. Cette charge
émotionnelle imprègne les récits politiques et religieux d’une grande
partie du monde arabo-musulman, et invalide l’idée que l’antisionisme
dont se réclame la cause palestinienne n’aurait rien d’antisémite.

Ce climat n’est cependant en rien constitutif de ce que signifie être arabe
ou musulman. Il s’agit d’un phénomène typiquement moderne, principalement
tributaire de l’influence des idéologies révolutionnaires européennes sur
les intellectuels et les activistes politiques arabes. Parmi ces schémas de
pensée importés figure une version de l’antisémitisme révolutionnaire
faisant du juif l’ennemi éternel non seulement des Arabes, mais aussi de
tous les êtres humains. Bien entendu, tous les Arabes et tous les musulmans
ne souscrivent pas à ces conceptions, mais lorsqu’elles se conjuguent aux
préjugés religieux et culturels préexistants, elles finissent par contaminer
la quasi-totalité des institutions, des courants de pensée et des aspects de
la vie dans le monde arabo-musulman. La littérature politique et religieuse
arabe moderne est remplie d’affirmations selon lesquelles les Juifs sont
hostis humani generis, les ennemis de l’humanité – une calomnie classique
dans l’Europe moderne, et un cri révolutionnaire français.

Les écueils de ce courant de pensée pernicieux sont encore aggravés par
l’idée que la « libération de la Palestine » est une forme de résistance
contre les colonisateurs étrangers, une révolution fanonienne dans laquelle
la violence contre les civils est défendue comme étant un moyen légitime
de parvenir à la justice raciale. L’étiquetage systématique des Juifs
israéliens – dont la grande majorité sont des réfugiés ou des descendants
de réfugiés des dictatures arabo-musulmanes et du totalitarisme soviétique
– en tant que colons et impérialistes est en fait une sorte de punition
ethnique collective, absurde même dans ses propres termes tordus. Il y a
là comme un écho de la manière dont, dans la chrétienté médiévale,
les Juifs, le groupe aussi bien que les individus, étaient condamnés en tant
qu’abominations morales.

Vous avez peut-être remarqué ces derniers jours que les partisans de la
libération de la Palestine ne semblent pas pouvoir éviter la déshumanisation
abjecte des Juifs en tant que peuple, et que leur objectif n’est pas simplement
que les Palestiniens vivent dans la paix, la dignité et la liberté aux côtés
des Israéliens, mais plutôt l’établissement d’un nouvel État sur les
ruines d’Israël. Le Hamas est explicite dans son intention d’assassiner
la population juive d’Israël et de réduire en esclavage les survivants ;
ses partisans au Moyen-Orient et en Occident sont plus hypocrites sur ce point.

Les islamistes articulent le fantasme de l’éradication des Juifs dans le
langage du djihad, formulé en termes eschatologiques et imprégné d’un
sens de la justice divine et de la guerre cosmique – ce que les Occidentaux
reconnaîtraient ordinairement comme un type de fascisme religieux. Mais
il suffit apparemment d’envelopper la version islamiste de cette idée,
efficace pour mobiliser les masses appauvries et sans éducation, dans
le langage de Fanon et de Karl Marx, de l’émancipation humaine, de
l’égalité, de l’anticapitalisme et de la justice sociale, pour obtenir
une version « de gauche » ou laïque, capable de rallier l’opinion au sein
de l’intelligentsia occidentale. Le fait est qu’il s’agit des deux faces
d’une même pièce, dont la valeur est fixée dans le sang juif.

Pour ceux qui sont imprégnés d’une telle vision du monde – qu’il
s’agisse de la version « de droite » ou « de gauche », religieuse ou
athée – le meurtre de civils israéliens innocents, y compris des enfants,
des femmes et des personnes âgées, représente l’accomplissement partiel
d’une vision morale, et demande donc à être célébré. Lorsque j’étais
adolescent en Égypte, je me souviens que presque tous les adultes de mon
entourage exprimaient de tels sentiments lorsqu’ils suivaient les nouvelles
concernant les attentats-suicides visant des civils israéliens au cours de la
seconde Intifada. Les autorités religieuses les plus éminentes d’Égypte ont
déclaré que les auteurs de ces attentats étaient des martyrs et des saints.

D’une certaine manière, cela n’est pas sans rappeler la valorisation,
voire la canonisation, de ceux qui ont détruit des commerces, brûlé des
biens et pris pour cible des policiers lors des manifestations qui ont eu
lieu aux États-Unis au cours de l’été 2020. Je ne veux pas injecter la
politique intérieure américaine là où elle n’a pas sa place, ni suggérer
une équivalence morale parfaite, mais il y a une raison pour laquelle les
dirigeants du Hamas et de la République islamique d’Iran eux-mêmes insistent
sur le fait qu’ils sont engagés dans la même lutte contre le racisme.

Presque tous les musulmans arabes savent que ce que je décris n’est pas
une opinion personnelle, mais une réalité objective. Nous pouvons essayer de
minimiser ces faits, ou de les rejeter comme les rêves délirants d’ignorants
sans éducation sous l’influence de fanatiques religieux et populistes. Mais
nous ne devrions pas nier leur réalité.

Je crains que la tentation d’écarter et de minimiser cette réalité soit le
sous-produit non pas d’une croyance sincère, mais d’un profond sentiment
d’impuissance. Après de nombreuses conversations récentes avec la nouvelle
génération de jeunes professionnels et diplomates arabes, intelligents,
occidentalisés et hautement éduqués, j’ai constaté une forte tendance à
l’évitement de cette réalité. Même parmi ceux qui acceptent sincèrement la
légitimité d’Israël d’une manière dont leurs parents n’auraient jamais
été capables, j’entends presque toujours décrire la mort d’Israéliens
innocents comme étant en quelque sorte de leur propre faute, ou du moins de la
faute du gouvernement israélien qui n’a pas unilatéralement fait la paix
et mis fin au conflit. Il n’y a rien de plus déprimant que la capitulation
des jeunes face à un problème qu’ils considèrent comme trop grand pour
être résolu.

Ceux d’entre nous qui appartiennent à la classe professionnelle cosmopolite
des Arabes, qui sautent d’un pays et d’un style de vie à l’autre,
profitant des cultures étrangères qui vivent selon les valeurs morales
du libéralisme et de la tolérance, ont, dans bien des cas, secrètement
honte. Nous voyons l’antisémitisme, la soif de sang, la folie, et nous
grimaçons – mais nous espérons que cela disparaîtra. Il est plus facile
pour nous de nous projeter dans un avenir hypothétique où les choses se
passeraient autrement. Il est plus facile de s’intégrer dans le nouveau
monde social auquel nous voulons appartenir, plutôt que d’affronter les
échecs de celui que nous avons laissé derrière nous. Nous rejetons, nous
déprécions, nous expliquons, nous disons « Et Shireen Abu Akleh ? » –
et nous continuons à faire semblant.

Mais il faut reconnaître que nous ne sommes pas aussi frais et jeunes que
nous aimons à le penser. Nous marchons sur les traces des générations
précédentes d’Arabes modernisateurs, laïcs et intellectuels. Eux aussi
ne voulaient rien savoir de leur pays d’origine, qu’ils considéraient
dépourvu du pouvoir, du prestige et du respect dont ils rêvaient. Dans leur
égoïsme et leur narcissisme intellectuel, ils ne voulaient pas appartenir à
des sociétés « arriérées ». Ils ont donc cherché dans des idéologies
étrangères, principalement occidentales, un refuge et une cachette contre
l’arriération. Ils ont rejoint les mouvements laïcs progressistes et les
révolutions à la mode parce qu’ils leur offraient une échappatoire à
la pénibilité d’un changement lent, marginal et local. Ils sont devenus
révolutionnaires parce qu’ils avaient peur et n’étaient pas sûrs
d’eux. Comme Edward Said, ils se sont faits « humanistes » antisionistes
et antiaméricains parce qu’ils ne voulaient pas, ou ne pouvaient pas, être
« arabes ». Leur chauvinisme culturel évident n’était qu’une envie
de s’anéantir, de disparaître dans l’universalisme. Leur vie n’était
qu’une quête désespérée pour se débarrasser de leur propre peau.

Aux Arabes de ma génération, je dis que nous avons besoin d’une approche
totalement différente. Je ne vous demande pas d’aimer Israël ou le sionisme,
ni d’accrocher un poster de Herzl dans votre chambre. Si vous critiquez Israël
et pensez qu’il devrait y avoir une Palestine, continuez à le faire. Tout ce
que je vous demande, c’est d’être authentiquement courageux, d’admettre
que le massacre dont nous avons tous été témoins ces derniers jours est
la représentation exacte et la conséquence logique d’un système moral
catastrophique, celui que nous connaissons tous intimement. L’heure est à
l’introspection collective. Il est temps d’affronter les pans les plus
sombres de notre héritage idéologique et de remettre en question les idées
et les croyances que nous avons absorbées sans les critiquer. Ce n’est
qu’ainsi que nous pourrons espérer contribuer par nous-mêmes à un monde
plus positif et plus humain.

© Hussein Aboubakr Mansour

Hussein Aboubakr Mansour est le Directeur du programme EMET for Emerging
Democratic Voices from the Middle East.
