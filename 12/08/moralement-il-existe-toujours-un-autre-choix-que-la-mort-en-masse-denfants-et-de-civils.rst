.. index::
   pair: Pierre Zahoui ; Moralement, il existe toujours un autre choix que la mort en masse d’enfants et de civils (2023-12-08)

.. _zahoui_2023_12_08:

===============================================================================================================================
2023-12-08 **Moralement, il existe toujours un autre choix que la mort en masse d’enfants et de civils** de Pierre Zahoui
===============================================================================================================================

- https://acrobat.adobe.com/id/urn:aaid:sc:EU:ab3d34ef-c36a-4917-a253-c5d8ff68ffbd


Préambule
============

**Le philosophe invite les juives et juifs du monde entier à un sursaut moral 
et Israël à ne pas perdre son âme, en stoppant les bombardements à Gaza** 

Les massacres du 7 octobre 2023, (**crimes contre l'humanité**, NDLR)
=======================================================================

Les massacres du 7 octobre 2023 (**crimes contre l'humanité**, NDLR) commis 
en Israël par les brigades du Hamas  et du Jihad islamique et la brutalité 
inouïe de leur mode d’exécution ont  produit un e et d’horreur et de 
sidération que nos générations d’après-guerre  n’avaient jusque-là connu 
qu’en lisant des livres d’histoire. 

Pendant quelques jours, quelques semaines, nous fûmes tous hébétés, sans mots, 
sans pensées, seulement ravagés de douleur, de terreur, et parfois stupéfaits 
devant les premiers signes d’une flambée d’antisémitisme **là où l’on attendait 
empathie et solidarité**. 

La réponse de l’armée israélienne (**crimes de guerre**, NDLR)
================================================================

Mais, depuis près de deux mois, la réponse de l’armée israélienne a commencé,
et elle est épouvantable. 
Déjà au moins 15 000 morts, dont plus de 5 000 enfants, sans doute bien 
davantage, tant il est impossible de dénombrer toutes les victimes 
ensevelies sous les gravats, et sans compter les blessés, les traumatisés, 
les affamés. 
En moins de deux mois, Israël a tué au moins vingt fois plus d’enfants 
que le Hamas n’en avait tués ou kidnappés le 7 octobre 2023. 

La reprise de l’offensive laisse envisager un `obituaire <https://fr.wikipedia.org/wiki/Obituaire>`_ bien plus
important encore. 

Ne parlons ni de pogrom pour le 7 octobre 2023 ni de génocide pour
l’offensive en cours, mots épouvante qui brouillent tout. 

Tentons plutôt un simple calcul macabre : si l’on s’en tient aux seuls 
chiffres de l’armée israélienne, les Brigades Al-Qassam et leurs alliés 
étant estimés à peu près à 30 000 hommes, s’il a fallu tuer plus de 
5 000 enfants et 10 000 civils pour tuer 5 000 combattants, il faudra 
au minimum tuer 60 000 civils dont 30 000 enfants pour "éradiquer" 
les forces du Hamas, si seulement cela a un sens. 

Violences sans nom (**crimes contre l'humanité et crimes de guerre**, NDLR)
============================================================================

Mais ne parlons pas en termes de réalisme, seulement en termes de justice. 

Vouloir mettre hors d’état de nuire le Hamas et ses alliés après le massacre 
(**crimes contre l'humanité**, NDLR) du 7 octobre 2023 est-il juste ? 
Absolument. 

Etre prêt à le faire au prix d’au moins 60 000 civils dont 30 000 enfants est-il
encore juste (**crimes de guerre**, NDLR) ?
En aucune manière. 

Aucun Etat, aucune société ne peut fonder sa sécurité ou user de son légitime 
droit à se défendre à un tel prix connu d’avance. 
Sauf à mériter d’être inscrit pour longtemps au rang d’Etat voyou et de 
société de voyous.

Le choc du 7 octobre 2023 est passé.  
Pas le deuil ni la douleur, mais le choc qui empêche de penser. 

Il est grand temps que tous les juifs de gauche, que tous les juifs tout 
court qui ont gardé une certaine idée de ce que Nietzsche appelait 
"le grand style dans la morale" (Par delà le bien et le mal, 
"Ce que l'Europe doit aux juifs", 1886)  manifestent leur condamnation 
la plus vive d’un calcul aussi sanglant (ce qui est une demande essentialisante, NDLR) 

On a assez demandé (demande essentialisante, NDLR) 
aux musulmans du monde entier de s’écrier après chaque  attentat islamiste 
depuis le 11 septembre 2001. 

Il est plus que temps qu’une majorité de juifs s’écrient "Not in my name"  
face aux violences  sans nom que subit depuis près de deux mois la 
population gazaouie,  tout particulièrement les enfants. 

D’autant que si la culpabilité des massacres du 7 octobre 2023 ("crimes contre l'humanité, NDLR) 
revient exclusivement aux membres du Hamas, la responsabilité de leur 
invasion revient tout autant au gouvernement israélien actuel, qui n’a 
pas su défendre sa population. 

Et même aux gouvernements israéliens successifs qui, depuis le début des 
accords d’Oslo [en 1993], ont laissé faire puis favorisé de nouvelles 
implantations (colonies, NDLR) juives en Cisjordanie tout en maintenant 
Gaza dans un blocus sans fin. 
Jusqu’à rendre caduque la doctrine israélienne officielle depuis 1967, 
"les territoires contre a paix", ce qui revenait à acquiescer d’avance 
à l’idée de guerre perpétuelle, dont les massacres des deux côtés sont 
l’une des modalités inévitables. 


Nous ne parlons pas ici de politique ou de géopolitique, nous ne prétendons 
même pas qu’Israël fait un mauvais calcul stratégique, en rappelant avec 
Rousseau que le droit du plus fort est une absurdité politique parce que 
"le plus fort n’est jamais assez fort pour être toujours le maître, s’il 
ne transforme sa force en droit, et l’obéissance en devoir"
[Du contrat social, 1762] . 
Nous parlons seulement de morale. 
 

Moralement, il est absolument insupportable de tuer deux innocents
pour tuer un méchant, quels que soient ses crimes par ailleurs. 

Plus encore, par principe, il existe moralement toujours un autre choix 
que la mort en masse d’enfants et de civils. 

Car s’il n’en existe pas, alors il n’y a plus de morale, plus de légitimité, 
plus de justice, plus de proportion, plus rien de ce qui constitue notre 
humanité essentielle. 
Le premier devoir moral du gouvernement israélien était donc de chercher 
une alternative aussi longtemps que nécessaire au lieu de foncer tête 
baissée dans le piège tendu par le Hamas. 

Charles Enderlin, dans "Israël. L’agonie d’une démocratie (Seuil, 60 pages, 4,90
euros), rappelait les mots d’Emmanuel Levinas après les massacres de
Sabra et Chatila, en 1982 "Se réclamer de l’Holocauste pour dire que Dieu 
est avec nous en toutes circonstances est aussi odieux que le : Gott mit uns 
qui gurait sur les ceinturons des bourreaux. 
Je ne crois pas du tout que la responsabilité ait des limites (...).
Là, personne ne peut nous dire : vous êtes en Europe et en paix, vous n’êtes
pas en Israël et vous vous permettez de juger"  
La situation d’aujourd’hui est bien pire.

En 1982, l’armée israélienne n’avait fait "qu’" ouvrir les portes des 
camps sans défense de Sabra et Chatila aux phalangistes chrétiens et 
fermer les yeux sur leurs assassinats de femmes et d’enfants palestiniens. 
Aujourd’hui, certes les camps palestiniens de Gaza ne sont plus sans défense, 
mais c’est l’armée israélienne elle-même qui réalise ces morts en masse ("crimes de guerre, NDLR). 

Il est donc de notre devoir le plus pressant à toutes et tous, particulièrement
aux juives et juifs du monde entier, de retrouver la hauteur morale d’un
Levinas, quelle que soit la douleur personnelle ressentie depuis le 7 octobre 2023,
en criant que ce qui se passe en ce moment même à Gaza est une faute morale
considérable et que ces massacres doivent s’arrêter immédiatement. 

--
Pierre Zaoui est maître de conférences en philosophie à l’université Paris Cité
