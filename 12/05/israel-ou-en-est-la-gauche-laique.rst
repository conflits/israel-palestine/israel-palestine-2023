
.. _gauche_israel_2023_12_05:

=================================================================
2023-12-05 **Où en est la gauche laïque ?** par Charles Conte
=================================================================

- https://blogs.mediapart.fr/edition/laicite/article/051223/israel-ou-en-est-la-gauche-laique
- https://blogs.mediapart.fr/charles-conte


ÉDITION : LAÏCITÉ Israël. Où en est la gauche laïque ?

5 décembre 2023 | Par Charles Conte

Quasi absente du parlement, la gauche laïque subsiste notamment grâce au
quotidien « Haaretz », menacé, et aux travaux du journaliste franco-israélien
Charles Enderlin.


Menée par le premier ministre Benjamin Netanyahou, une droite de plus en
plus radicale gouverne le pays depuis près de 40 ans et mène une politique
implacable à l’égard des Palestiniens. Le « Meretz » (« Energie » en
hébreu), le plus à gauche des partis sionistes, qui défendait la laïcité,
le gel des colonies dans les territoires occupés, l’Etat social, le principe
« deux peuples, deux États »… ne compte plus aucun député. Heureusement,
beaucoup de citoyens et de citoyennes restent attachées à la démocratie. Des
manifestations rassemblant jusqu’à 200.000 personnes contre le remplacement
du ministre de la justice, opposé à une réforme favorisant une nouvelle
fois les milieux religieux, l’ont prouvé. Il savoir, par exemple, que les
citoyens se référant à l’orthodoxie religieuse, payent, de dérogation
en dérogation, six fois moins de taxes que les autres citoyens.


En 2018 la loi sur Israël « Etat-nation du peuple juif » impose une
vision ethnicisante de la nationalité au détriment des citoyens israéliens
d’origine arabe ou druze. En 2021, seuls 41 % d’enfants juifs ont entamé
leur scolarité dans une école laïque. Le 7 octobre dernier, les attaques
terroristes du Hamas sur le territoire israélien, et la sanglante répression
menée par l’armée israélienne, révèlent une montée aux extrêmes qui
semble inexorable. Gérard Noiriel analyse sur son blog les violentes polémiques
générées par cette situation dans les milieux intellectuels français :
« L’antisémitisme en question. Réflexions sur le respect de l’Autre ».

Le quotidien « Haaretz ».
============================


« Quiconque veut contrecarrer la création d’un État palestinien doit
soutenir le renforcement du Hamas et transférer de l’argent au Hamas »
En Israël, le quotidien « Haaretz » (« Le Pays »), publié en hébreu et
doté d’une version anglophone, est un des seuls à s’insurger contre la
politique menée par le gouvernement israélien. Il touche un peu moins de 5 %
du lectorat israélien. Selon « La Revue des médias » de l’INA, il est lu
par beaucoup d’intellectuels. Haaretz : lieu de vie intellectuelle, lieu de
débat | la revue des médias (ina.fr) Depuis longtemps « Haaretz » dénonce
les menées de la droite messianique, les privilèges dont bénéficient
les religieux et la stratégie du premier ministre Benjamin Netanyahou, en
particulier par rapport au Hamas : « Quiconque veut contrecarrer la création
d’un État palestinien doit soutenir le renforcement du Hamas et transférer
de l’argent au Hamas », a déclaré Netanyahu aux membres de la Knesset de
son parti, le Likoud, en mars 2019.

« Cela fait partie de notre stratégie ».Voir tweet ci-dessus. Le ministre de
la Communication, membre du parti Likoud, vient de soumettre une proposition
visant à asphyxier le journal à travers un arrêt des financements et des
abonnements publics. Ces menaces contre le « dernier bastion de l’opposition
juive en Israël » selon le journaliste Sylvain Cypel, sont de plus en
plus fortes.  

Le journaliste Charles Enderlin
==================================

Journaliste franco-israélien, Charles Enderlin a été durant 34 ans ans le
correspondant à Jérusalem de France 2. Il a publié une dizaine de livres, de
la biographie d’ Yitzhak Shamir à « Israël : l’agonie d’une démocratie
» (Editions du Seuil) qui est paru en septembre 2023. Il décrit la politique
poursuivie par Benjamin Netanyahou et ses alliés ultra-orthodoxes. Son point
d’orgue est la loi « Israël Etat-nation du peuple juif » de 2018. Elle se
traduit notamment par la création en mai 2023 de l’Agence gouvernementale
de l’identité nationale juive dirigée par un vice-ministre, le rabbin
intégriste Avi Maoz. A la suite des élections de 2022 le nouveau gouvernement a
remis les ministères clés de la colonisation et les postes décisionnaires dans
le domaine de l’éducation à des colons de la liste « Sionisme religieux ».

Charles Enderlin a analysé en détail les origines religieuses et idéologiques
du messuianisme juif, son développement et sa montée en puissance jusqu’à
l’arrivée au pouvoir des sionistes religieux dans son livre « Au nom
du Temple. Israël et l'arrivée au pouvoir des juifs messianiques ». Cet
ouvrage actualisé est réédité dans la collection de poche Points Seuil. Il
constate: « « Islamistes et intégristes messianiques sont des miroirs. Pour
les militants du sionisme religieux, il n’y a pas de place pour une Palestine
en terre d’Israël et, pour les islamistes, il n’y a pas de place pour
un État juif en terre d’islam ». Il a tiré de ce livre un film « Au
nom du Temple: Le sionisme religieux contre la paix » (Zadig production,
diffusé par France 2) visible sur Youtube Au nom du Temple: Le sionisme
religieux contre la paix - YouTube et donné de nombreuses conférences (A
l’Université Libre de Bruxelles notamment) et entretiens.

Entretien avec Jean-Paul Chagnollaud à l’Institut de recherches et d’études
Méditerranée Moyen-Orient. « Israël. L’agonie d’une démocratie » 14
octobre 2023.
