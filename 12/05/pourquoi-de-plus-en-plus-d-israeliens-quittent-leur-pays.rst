
.. _escape_israel_2023_12_05:

==============================================================================
2023-12-05 **Pourquoi de plus en plus d’Israéliens quittent leur pays ?**
==============================================================================

- https://regards.fr/pourquoi-de-plus-en-plus-disraeliens-quittent-leur-pays/

Face aux guerres et à la radicalisation, le rêve d’un peuple de trouver un
refuge, de fuir l’antisémitisme de par le monde, peut devenir un véritable
crève-cœur.

Il y a des témoignages, nombreux, rapportés par les journalistes. Ces
Israéliens qui fuient Israël pour leur sécurité. Mais difficile, voire
impossible, d’obtenir d’une quelconque autorité des données précises. Le
sujet est tabou pour Israël, pays qui se veut être un refuge pour les Juifs.

En juillet dernier, un sondage réalisé en Israël montrait que « près
d’un tiers des Israéliens envisageaient de quitter le pays ». La faute à un
gouvernement et une société qui se radicalise toujours plus à l’extrême
droite et contre la démocratie. La faute aussi à la guerre, qui inquiète
toujours la population – gageons que si l’on refaisait ce sondage depuis le
7 octobre, les chiffres seraient bien différents. Mais au-delà des intentions,
le départ d’Israël est une réalité pour de nombreux binationaux.

Dans Marianne, ce 8 novembre, ce sont des binationaux, Israéliens et
Russes/Italiens/Britanniques/Brésiliens, qui témoignent. Ils quittent
Israël par peur qu’une guerre éclate. Encore. Et ce n’est pas de
gaîté de cœur qu’ils retrouvent des pays de départ où ils subissaient
l’antisémitisme. Combien sont-ils, comme eux, à avoir pris cette décision
de fuir Israël après le 7 octobre ? Seul Israël le sait.

« Le nombre d’Israéliens qui quittent le pays a, ces dernières années,
dépassé celui des immigrants. »

Le phénomène est antérieur aux événements récents. On retrouve
même un article du quotidien suisse Le Temps daté de 2007 qui évoque la
problématique. En septembre dernier, le Courrier international, citant
le quotidien israélien Haaretz, écrivait ceci : « De plus en plus
d’Israéliens, mécontents des conditions politiques et économiques dans
leur pays, trouvent refuge à l’étranger. […] Ils sont pour la plupart
des opposants à la politique de Benyamin Nétanyahou, participent à des
manifestations de protestation et sont accablés par l’état de la démocratie
en Israël, pays auquel ils sont tous pourtant très attachés ».

En mars 2023, un long reportage du Times of Israël évoque également ce
désir de quitter le pays. La réforme judiciaire, très décriée et qui
avait fait l’objet d’une forte contestation, est régulièrement citée
comme la goutte d’eau qui a fait déborder le vase – une nouvelle fois,
l’éclatement de la guerre entre Israël et le Hamas n’a pas dû arranger la
tendance. Là encore, les témoignages affluent. Mais point de chiffres ni de
statistiques. Voici ce que l’on peut lire sur le site Equal Times : « Selon
les professionnels du secteur, toutefois, ces demandes auraient connu un pic
au lendemain des élections [qui ont reconduit Benyamin Netanyahou au pouvoir
fin 2022, ndlr], avec les États-Unis et les pays de l’Union européenne en
tête des destinations […] Les passeports allemands et autrichiens seraient
parmi les plus convoités ».

Et soudain, des chiffres, toujours sur Equal Times : « Selon le Bureau
central des statistiques, le nombre d’Israéliens qui quittent le pays a, ces
dernières années, dépassé celui des immigrants. Cette tendance s’accentue
depuis 2009, année où une guerre de trois semaines a éclaté entre Gaza et
Israël. En 2020, le nombre de départs s’élevait à un peu moins de 21 000,
contre environ 10 000 rapatriements. »

Par ailleurs, sur le site Middle East Eye, on trouve cette donnée : « Entre
1948 et 2015, selon le gouvernement israélien, 720 000 Israéliens ont émigré
et ne sont jamais revenus. » La fin du rêve israélien ?

Dans les divers témoignages, il y a plusieurs profils qui se détachent parmi
les émigrants : les Israéliens laïques, jeunes, diplômés, de gauche,
des classes plutôt aisées. Sachant que les Israéliens ne peuvent pas voter
depuis l’étranger… Cela veut-il dire que seuls les plus radicaux, les
plus extrémistes et/ou les plus pauvres vont rester ?

Selon les dernières données disponibles, la part des Israéliens laïques est
encore majoritaire : 44%, contre 35% de traditionalistes, 11% d’orthodoxes
et 10% d’ultra-orthodoxes – ces deux derniers groupes ayant la croissance
démographique la plus forte. Parallèlement, c’est l’écart entre les
Juifs et les Arabes qui se réduit en Israël : de 87,8% de Juifs et 8,5%
de musulmans dans les années 1950, on est passé à 74% de Juifs et 18%
de musulmans dans les années 2020.

Car derrière ces déplacements de population, c’est bien la question
démographique qui est au centre. Avec cette problématique majeure pour
Israël, comme un paradoxe : à force de coloniser la Cisjordanie, le pays
« accueille » de plus en plus de personnes non-juives. Lors des premières
élections législatives, en 1949, la gauche était majoritaire et il n’y
avait que deux députés arabes. Aux dernières législatives, la gauche a
quasiment disparu de la Knesset et les partis arabes comptent dix députés.

Face à cette réalité des courbes de tendance démographique et politique,
l’avenir d’Israël est difficile à anticiper. Va-t-on vers un pays qui
se proclame « État Juif », à majorité arabe ? Et quelle cohabitation avec
une population juive de plus en plus ultra-orthodoxe ?
