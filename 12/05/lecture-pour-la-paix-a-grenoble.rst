
.. _lecture_2023_12_05:

=================================================================================
2023-12-05 **Lecture d'extraits du roman Apeirogon pour la paix à Grenoble**
=================================================================================

- :ref:`parents:parentscirclefriends`
- https://kolektiva.social/@grenobleluttes/111520940866794597

Apeirogon
===========

- https://fr.wikipedia.org/wiki/Apeirogon_(roman)

Apeirogon (titre original : Apeirogon) est un roman, publié en 2020, par 
l'écrivain irlandais Colum McCann. 

La même année, le roman est traduit en français par Clément Baude pour le 
compte des éditions Belfond. 

parentscirclefriends Description
=======================================

American Friends of the Parents Circle – Families Forum shares the human side 
of the Israeli-Palestinian conflict with the American public in order to foster 
a peace and reconciliation process.

The Parents Circle – Families Forum is a joint Israeli-Palestinian organization 
made up of more than 700 bereaved families. 

Their common bond is that they have lost a close family member to the conflict. 

But instead of choosing revenge, they have chosen a path of reconciliation.


2023-11-30 Voices of Apeirogon An Intimate Conversation with Colum McCann and Bereaved Fathers 
==================================================================================================

- https://parentscirclefriends.org/voices/
- https://www.youtube.com/watch?v=J0ASOZ3XZA0&t=1s

A conversation with renowned Irish author Colum McCann. 

Colum engaged in a  heartfelt dialogue with Rami Elhanan and Bassam Aramin, 
bereaved fathers from  Israel and Palestine, the protagonists of Apeirogon.

**In the face of the violence shaking Israel and Palestine, join us to hear from 
Rami and Bassam, who, despite the pain of loss, still call each other brothers**. 

`McCann’s powerful 2020 novel Apeirogon <https://en.wikipedia.org/wiki/Apeirogon_(novel)>`_ weaves 
together tales of peace, loss,  and reconciliation. 

The book’s exploration of humanity becomes even more relevant today.

Bassam Aramin
==================

- :ref:`parents:bassam_aramin`

Rami Elhanan
==============

- :ref:`parents:rami_elhanan`


Colum McCann
==============

Colum McCann is the author of seven novels, three collections of stories 
and two works of non-fiction. 

Born and raised in Dublin, Ireland, he has been the recipient of many 
international honours, including the U.S National Book Award, the 
International Dublin Literary Prize, a Chevalier des Arts et Lettres 
from the French government, election to the Irish arts academy, several 
European awards, the 2010 Best Foreign Novel Award in China, and an 
Oscar nomination. 

In 2017 he was elected to the American Academy of Arts. 

He is the President and co-founder of the non-profit global story 
exchange organisation, Narrative 4. His most recent novel, Apeirogon, 
became an immediate New York Times best-seller and won several major 
international awards. 


A Grenoble
===========

.. figure:: images/lecture_pour_la_paix.png
