
.. _markowicz_2023_12_05:

=================================================================
2023-12-05 **Les ruines** par André Markowicz 
=================================================================


Il y a des reportages, israéliens, sur les opérations militaires en cours,
sur, censément, les grandes victoires de Tsahal à Gaza, aujourd’hui, et,
hier, – le jour change, mais pas les images, qui ne se concentrent jamais
sur ça, mais sur l’héroïsme et le dévouement des soldats israéliens,
lesquels ne subissent que très peu de pertes, parce qu’ils avancent d’une
façon professionnelle, – et ce qu’on voit, en arrière-fond, ce sont les
ruines. 

Quels que soient les angles de prise en vue, l’impression est que, soit
le Hamas a mis des tunnels sous toutes les maisons, ou sur les toits des maisons,
ou entre les étages, parce que, dans les images israéliennes même (et, très
volontairement, je ne parle pas des autres reportages, potentiellement, sans
doute, sympathisants des terroristes), il n’y a pas une immeuble intact. 

En fait, ces images-là, sans le fait qu’on y voit des soldats israéliens,
qui parlent hébreu quand on les entend parler, je me demanderais si elles
n’ont pas faites par des journalistes infiltrés, avec des montages de vues
d’Alep ou de je ne sais quelle autre ville e Syrie ou d’Irak.


Je le répète encore : on a toujours tort de ne pas prendre à la lettre ce
que disent les fascistes. Netanyahou, dont la politique porte, directement et
indirectement, une responsabilité écrasante dans l'horreur du 7 octobre, a
dit qu’Israel allait répliquer de telle sorte, — je le redis en substance
– qu’« ils » (va savoir qui, le Hamas, ou tous les Palestiniens, ou le
monde entier ?) allaient s’en souvenir pendant des décennies.

Le but de la guerre, aujourd’hui, est clair : il s’agit de détruire
tout. Et d’abord toutes les infrastructures, – tous les systèmes, déjà
fort défaillants, d’adduction d’eau, d’électricité, que sais-je,
tout ce qui permet une vie civile un tant soit peu quotidienne. Et tous les
bâtiments, quels qu’ils soient, sans aucune distinction, – que ce soit
les administrations, les établissements scolaires, les hôpitaux (parce
que le Hamas les utilisait comme caches, – ce qui est, souvent, mais pas
toujours, avéré). Mais il ne s’agit pas du Hamas. Il s’agit de faire que
les gens, après, n’aient nulle part où aller. Parce qu’il s’agit aussi
de détruire les lieux d’habitation : on ne le fait pas, toujours, exprès,
mais nous sommes en guerre, et Israel utilise des systèmes ultra-modernes
de guidage de ses bombes, avec des logiciels qui permettent de calculer,
pour un terroriste tué, combien il peut y avoir de victimes extérieures
(des gens qui ne connaissent ce terroriste ni d’Eve ni d’Adam, mais qui se
font tuer parce qu’ils sont dans les environs). Ces logiciels calculent, et
autorisent : pour un terroriste avéré écrasé sous un missile, deux cents,
trois cents personnes sont tuées, vieillards, femmes et enfants, et, encore
une fois, les enfants palestiniens n’ont, pour nous, ni nom ni visage. –
Sachant, en outre, qu’Israel est un des plus grands exportateurs d’armes
du monde, et qu’une guerre en vrai est une possibilité réelle de démontrer
l’efficacité des produits que tu proposes.

Mais il ne s’agit pas seulement des maisons. – Dans un premier temps,
après un blocus de l’eau et de l’électricité (qui, en soi, déjà,
constituaient des crimes de guerre), Israel a donné ordre à toute la population
de Gaza nord d’évacuer, en vingt-quatre heures. C’est-à-dire que, je ne
sais pas, en ving-quatre heures, un million de personnes ont été censées
prendre ce qu’elles pouvaient, et partir – personne ne disait où, dans
quelles conditions, pour combien de temps, rien. Partir, parce qu’Israel
allait commencer à bombarder. C’est-à-dire que, du jour au lendemain,
un million de personnes (qui n’avaient déjà pas grand’chose pour la
plupart – ce qui n’était certes pas le cas des chefs du Hamas) se sont
retrouvés carrément sans rien. Juste sans rien.

Et ce déplacement de population, qu’on appellerait ailleurs un « nettoyage
ethnique » ne suffisait pas, puisque, désormais, la guerre est passée
dans le secteur sud, et que Netanyahou explique que le sud sera traité
d’une façon aussi dure que l’a été le nord (entièrement, donc,
détruit). C’est-à-dire que les gens qui se sont retrouvés agglutinés
au sud, avec ceux qui étaient déjà au sud – dans un des endroits, je le
rappelle, les plus densément peuplés du monde – se retrouvent sous les
bombes, et la question de savoir où ils vont s’évacuer n’est même pas
posée : j’ai l’impression qu’à moins de les laisser là, comme Staline
avait interdit l’évacuation des habitants de Stalingrad, c’est l’Egypte
qui sera mise devant le fait accompli, – il faudra bien qu’elle ouvre ses
frontières et qu’elle ouvre le SInaï à des villes de tentes. Je ne sais pas.

Le but n’est pas seulement de détruire les conditions de vie, il n’est
pas, non, de détruire physiquement les gens (même si, très sciemment,
Israel ne regarde pas le nombre de pertes civiles qu’entraîne sa politique
militaire), il est de les détruire mentalement. De les écraser non pas
tellement sous les bombes (parce que les bombes ne vont pas durer dix ans),
mais sous leur impuissance. De les transformer en loques, à la merci d’un
ordre d’évacuation, ou d’un autre, ou de je ne sais pas quoi d’autre,
mais toujours en sorte qu’ils soient comme un troupeau, poussé ici, poussé
ailleurs, dépendant entièrement d’autrui, – de l’aide internationale,
de la Croix-rouge, que sais-je – juste pour survivre, mais qu’ils n’aient
jamais la possibilité d’envisager une vie à long terme.

Le but est là. Clair comme le jour. – J’allais dire noir comme le jour. De
faire tellement peur que cette peur se transmette à des générations et des
générations. Que les petits-enfants des enfants d’aujourd’hui tremblent
rien qu’à s’en souvenir. – Sauf qu’ils ne trembleront pas. Ils haïront,
et toujours plus. Et ils se construiront sur cette haine. Et, tout en forgeant
la haine, en l’exploitant (parce qu’il n’y a rien à forger, en fait,
elle est là, et elle est absolument générale, la haine pour l’oppression
israélienne (qui devient très vite la haine pour le Juif en général),
c’est une autre des victoires du Hamas que ce but-là – de transformer
ce qui se présente comme une démocratie en ça, en machine à détruire,
au nom d’une folie identitaire, quitte à détruire en plus ce qui reste
de la planète pour reconstruire le Temple ou va savoir. Avec cet Israel-là,
le Hamas ne peut qu’avoir un avenir radieux. Oui, dans les ruines de Gaza,
c’est, réellement, un grand, grand boulevard qui s’ouvre à lui.

