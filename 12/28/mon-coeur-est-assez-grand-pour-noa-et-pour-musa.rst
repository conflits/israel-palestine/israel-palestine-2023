.. index::
   pair: Ghadir Hani; Mon cœur est assez grand pour Noa et pour Musa (2023-12-28)

.. _hani_2023_12_28:

====================================================================================
2023-12-28 **Mon cœur est assez grand pour Noa et pour Musa** par Ghadir Hani
====================================================================================

Si nous perdons cette capacité à supporter la douleur des deux côtés,
nous perdrons l’espoir et la chance de vivre un jour côte à côte.

Depuis plus de deux mois, la réalité de nos vies est un cauchemar
permanent. L’horrible assaut du 7 octobre a sapé les fondements mêmes de
notre existence – pour nous tous.

Ce bain de sang menace de nous noyer tous.

Plus de deux mois après cette terrible journée, 129 personnes sont toujours
retenues en otage par le Hamas, sans médicaments et sans provisions de
base. J’appelle toute personne ayant de l’influence à l’utiliser. Nous
ne pouvons pas permettre aux otages de rester là-bas. Nous sommes tous
responsables les uns des autres, dans les bons comme dans les mauvais moments,
dans les difficultés comme dans l’espoir. Mon cœur est avec les nombreuses
familles qui vivent dans la tourmente depuis trop longtemps, tiraillées entre
désespoir et espoir.

Mes pensées vont également à toutes les personnes assassinées lors de cette
attaque, parmi lesquelles des amis proches. J'ai eu la chance de rencontrer
de nombreux Israéliens des communautés proches de la bande de Gaza qui ont
compris peut-être mieux que chacun d'entre nous que leur avenir est lié à
celui de Gaza.

Beaucoup d’entre eux ne sont plus parmi nous, mais leur héritage résonne
: notre mode par défaut ne doit pas être l’option militaire. Cela peut
paraître aux yeux du public comme une solution magique, mais cela ne fait
qu’accroître l’hostilité et la haine et n’offre pas d’alternative à
l’effusion de sang en cours. Sans espoir pour ceux qui vivent à Gaza, il n’y
a pas de sécurité pour les communautés israéliennes proches de la frontière.

Mon cœur va également aux habitants de Gaza qui sont victimes de ce conflit
depuis bien trop longtemps. Des générations après des générations
de Gazaouis ont vécu toute la gamme des tragédies et des horreurs de ce
conflit. La vue des enfants s’abritant sous des bâches en plastique sous
la pluie me brise le cœur.

Aucun mot ne peut réconforter les milliers de morts, les dizaines de milliers
de blessés et les centaines de milliers de personnes arrachées à leurs
foyers. Essayez d’imaginer cette vie – tant d’années de difficultés et
de tentatives pour survivre à une cruelle réalité, et maintenant ils sont
pris entre des bombardements incessants et un régime qui détruit tout espoir.

Mon cœur est grand. Mon cœur a la capacité de retenir Noa Argamani, qui a
été prise en otage et dont la mère, atteinte d'un cancer, prie pour revoir sa
fille, ainsi que Musa, dont toute la famille a été tuée alors qu'il partait
chercher de l'eau pour son petit frères. Nos cœurs doivent contenir à la
fois Noa et Musa. Si nous perdons cette capacité, nous perdrons l’espoir
et la chance de vivre un jour côte à côte.

Les tentatives visant à faire taire les voix du bon sens et de la modération
sont incessantes. Il ne se passe pas un jour sans que nous soyons attaqués
parce que nous défendons nos valeurs. Il ne se passe pas un jour sans qu’on
nous dise que notre naïveté affaiblit notre peuple, ou qu’on nous accuse
de trahison ou de déloyauté. Je connais bien ces accusations. Je ne trouve
pas toujours les mots justes pour y répondre, mais je sais que des décennies
de force et d'agression ne nous ont rien apporté de bon.

L’attaque meurtrière et odieuse du Hamas n’a fait que renforcer les
extrémistes au sein de la société israélienne. Les politiques militaires
en Cisjordanie et à Gaza n’ont fait que renforcer les extrémistes parmi
les Palestiniens. La force engendre la force. La violence mène à la haine
et à la vengeance.

Je me demande quotidiennement : comment se fait-il que nous n'ayons pas appris
cette leçon ? Comment avons-nous permis à des dirigeants extrémistes,
arrogants et corrompus de nous conduire au bord du gouffre ? Comme l’a dit
mon amie Vivian Silver, assassinée le 7 octobre : il n’y a pas de chemin
vers la paix – la paix est le chemin.

Il ne faut pas perdre espoir, il ne faut pas abandonner. Trop d’enfants
dépendent de nous pour éviter d’entraîner une nouvelle génération dans
cette guerre. Même si nous sommes peu nombreux aujourd’hui, notre lumière ne
s’éteindra pas. Notre espoir réveillera, gonflera, motivera et balayera les
Juifs et les Arabes qui refusent d’être ennemis. Les Juifs et les Arabes,
voisins, veulent vivre côte à côte dans une société juste, guidés par
le respect mutuel, bénéficiant de droits et assumant des responsabilités
sur un pied d’égalité.

Cette horrible guerre doit redéfinir les relations entre Juifs et Arabes
dans l’État et dans toute la région. Nous, citoyens arabes d’Israël,
voulons servir de pont entre le peuple juif et le peuple palestinien. De
nombreux dirigeants de la société arabo-israélienne le disent.

Le public, dans son ensemble, doit être un partenaire dans la construction
d’une société juste, respectueuse et tolérante. Même s’il existe
déjà de nombreuses initiatives locales, nous devons continuer à forger
de plus en plus de liens aux niveaux communautaire, éducatif, religieux et
politique. Dans les hôpitaux, des équipes judéo-arabes sauvent la vie des
Juifs et des Arabes. Travaillons tous ensemble pour atteindre cet objectif
de sauver des vies.  En vérité, l’extrémisme ne disparaîtra pas, mais
notre lumière brillera de plus en plus fort car aucun de nous n’a d’autre
patrie. Même s’ils tentent de nous faire taire, nous n’abandonnerons
pas. S’ils tentent de nous empêcher de nous rassembler, nous continuerons
à dire : la voie de la terreur et de la force ne doit pas gagner. La paix
viendra un jour et elle prévaudra – et plus
